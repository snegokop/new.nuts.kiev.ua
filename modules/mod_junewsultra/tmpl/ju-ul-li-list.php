<?php
/**
 * JUNewsUltra Pro
 *
 * @version 	6.x
 * @package 	UNewsUltra Pro
 * @author 		Denys D. Nosov (denys@joomla-ua.org)
 * @copyright 	(C) 2007-2016 by Denys D. Nosov (http://joomla-ua.org)
 * @license 	GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 **/

/******************* PARAMS (update 20.03.2016) ************
*
* $params->get('moduleclass_sfx') - module class suffix
*
* $item->link           - show link
* $item->title          - show title
* $item->title_alt      - for attribute title or alt
*
* $item->cattitle       - show category title
*
* $item->image          - show image
* $item->imagesource    - show raw image source
*
* $item->sourcetext		- show raw intro and fulltext
*
* $item->introtext      - show introtex
* $item->fulltext       - show fulltext
*
* $item->author         - show author or created by alias
*
* $item->sqldate		- show raw date (0000-00-00 00:00:00)
* $item->date           - show date & time with date format
* $item->df_d           - show day
* $item->df_m           - show mounth
* $item->df_y           - show year
*
* $item->hits           - show Hits
*
* $item->rating         - show rating with stars
*
* $item->comments		- show comments couner
* $item->commentslink   - show link to comments
* $item->commentstext   - show comments text
* $item->commentscount  - show comments couner (alias)
*
* $item->readmore       - show 'Read more...'
* $item->rmtext         - show 'Read more...' text
*
************************************************************/

defined('_JEXEC') or die('Restricted access');

?>
<ul class="junewsultra <?php echo $params->get('moduleclass_sfx'); ?>">
<?php foreach ($list as $item) :  ?>
	<li class="jn-list">
        <?php if($params->get('show_title')): ?>
    	<strong><a href="<?php echo $item->link; ?>" title="<?php echo $item->title_alt; ?>"><?php echo $item->title; ?></a></strong>
        <?php endif; ?>
        <div class="jn-list-info">
            <?php if($params->get('showRating')): ?>
            <div class="left">
                <?php echo $item->rating; ?>
                <div>
                <?php if($params->get('showRatingCount')): ?>
                <?php echo $item->rating_count; ?>
                <?php endif; ?>
                <?php if($params->get('showHits')): ?>
                <?php echo JText::_('JGLOBAL_HITS'); ?>: <?php echo $item->hits; ?>
                <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
            <div class="right">
            <?php if($params->get('show_date')): ?>
            <span><?php echo $item->date; ?></span>
            <?php endif; ?>
            <?php if($params->get('showcat')): ?>
            <span><?php echo $item->cattitle; ?></span>
            <?php endif; ?>
            <?php if($params->get('juauthor')): ?>
            <span><?php echo $item->author; ?></span>
            <?php endif; ?>
            <?php if($params->get('use_comments')): ?>
			<span><a class="jn-comment-link" href="<?php echo $item->link; ?><?php echo $item->commentslink; ?>"><?php echo $item->commentstext; ?></a></span>
            <?php endif; ?>
            </div>
        </div>
	</li>
<?php endforeach; ?>
</ul>