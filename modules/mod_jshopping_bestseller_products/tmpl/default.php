<?php



	addLinkToProducts($last_prod);
	
        $jshopConfig = JSFactory::getConfig();
	
	$default_count_product = 1;

        if ($jshopConfig->min_count_order_one_product>1){

            $default_count_product = $jshopConfig->min_count_order_one_product;

        }
?>
<div class="bestseller_products">
<?php foreach($last_prod as $curr){
    
    
			$pr = JTable::getInstance('product', 'jshop'); 

			$pr->load($curr->product_id);

			$attributesDatas = $pr->getAttributesDatas($back_value['attr']);

			$pr->setAttributeActive($attributesDatas['attributeActive']);

			$attributeValues = $attributesDatas['attributeValues'];        

			$attributes = $pr->getBuildSelectAttributes($attributeValues, $attributesDatas['attributeSelected'], false);//$curr->product_id);			

	
    ?>
<form class="ajax-edit" name="product_<?php print $curr->product_id?>" id="form_<?php print $curr->product_id?>" method="post" action="<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=add');?>" enctype="multipart/form-data" autocomplete="off">
	<input type="hidden" name="to" id='to' value="cart" />
	<input type="hidden" name="product_id" id="product_id_<?php print $curr->product_id?>" value="<?php print $curr->product_id?>" />
	<input type="hidden" name="category_id" id="category_id_<?php print $curr->product_id?>" value="<?php print $curr->category_id?>" />	
	<input type="hidden" name="action" value="<?php print SEFLink('index.php?option=com_jshopping&controller=product&task=ajax_attrib_select_and_price&product_id='.$curr->product_id.'&ajax=1',1,1)?>" />

    <div class="block_item">
        <?php if ($show_image) { ?>
        <div class="item_image">
            <a href="<?php print $curr->product_link?>">               
                <img src = "<?php print $jshopConfig->image_product_live_path?>/<?php if ($curr->product_thumb_image) print $curr->product_thumb_image; else print $noimage?>" alt="" />
            </a>
        </div>
        <?php } ?>
        <div class="item_name">
            <a href="<?php print $curr->product_link?>"><?php print $curr->name?></a>
        </div>
	<div class="description">
            <?php print $curr->short_description; ?>
	<?php if ($curr->manufacturer->name){?>
	<br/><br/>
            <div class="manufacturer_name"><?php print _JSHOP_MANUFACTURER;?>: <span><?php print $curr->manufacturer->name?></span></div>
        <?php }?>
        </div>
       <?php if ($curr->_display_price){?>
            <div class = "jshop_price">
                <?php if ($curr->config->product_list_show_price_description) print _JSHOP_PRICE.": ";?>
                <?php if ($curr->show_price_from) print _JSHOP_FROM." ";?>
                <span class="view_prod_price"><?=$curr->product_quantity <= 0 ? "Ожидается" : formatprice($curr->product_price)?></span>
            </div>
        <?php }?>
		<div style="clear: both"></div>
		<?php if (count($attributes)){?>
		
		<div class="jshop_prod_attributes">
			<table class="jshop">
			<?php foreach($attributes as $attribut){?>
			<tr>
				<td class="attributes_title">
					<span class="attributes_name"><?php print $attribut->attr_name?>:</span><span class="attributes_description"><?php print $attribut->attr_description;?></span>
				</td>
				<td>
					<span id='block_attr_sel_<?php print $attribut->attr_id?>'>
					<?php print preg_replace('#onchange="([^"]+)"#ixsU', '', $attribut->selects)?>
					</span>
				</td>
			</tr>
			<tr>
				<td class="prod_qty">
					<?php print _JSHOP_QUANTITY?>:&nbsp;
				</td>
				<td class="prod_qty_input">
					<input type="text" name="quantity" class="inputbox" value="<?php print $default_count_product?>" />
				</td>   
			</td>
			<?php }?>
			</table>
		</div>
		<?php }?>
    	    <div class="buttons">
            <?php if ($curr->buy_link){?>
			<a class="button_buy" href="javascript:void(0);" onClick="document.getElementById('form_<?php print $curr->product_id?>').submit();"><?php print _JSHOP_BUY?></a> &nbsp;
            <!--<a class="button_buy" href="<?php print $curr->buy_link?>"><?php print _JSHOP_BUY?></a> &nbsp;-->
            <?php }?>
            <a class="button_detail" href="<?php print $curr->product_link?>"><?php print _JSHOP_DETAIL?></a>
            <?php print $curr->_tmp_var_buttons;?>
        </div>
    </div>
    </form>
<?php } ?>
</div>
