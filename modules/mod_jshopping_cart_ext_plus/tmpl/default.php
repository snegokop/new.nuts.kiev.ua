<?php 
$view_cart = $params->get('view_cart',1);
$show_cart = $params->get('show_cart',1);
$show_attr = $params->get('show_attr',1);
$show_extra = $params->get('show_extra',1);
$show_popup = $params->get('show_popup',1);
$autoclose_show_popup = $params->get('autoclose_show_popup',1);
$set_timeout_popup = $params->get('set_timeout_popup',1);
$highlight_attr = $params->get('highlight_attr',1);
$link_prod = $params->get('link_prod',1);
$show_image = $params->get('show_image',1);
$mycart_name = $params->get('mycart_name',1);
$mycart_show_image_cart=$params->get('mycart_show_image_cart',1);
?>
<script type="text/javascript">
if (typeof(jQuery) == 'undefined') {
     document.write('<scr' + 'ipt type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></scr' + 'ipt>');
					document.write('<scr' + 'ipt type="text/javascript" src="/components/com_jshopping/js/jquery/jquery-noconflict.js"></scr' + 'ipt>');
    }
</script>

<?php if ($highlight_attr=='1') {?>
<style type="text/css">
.highlight{background: url("/modules/mod_jshopping_cart_ext_plus/img/bgattr.gif");}
</style>
<?php require (JPATH_SITE.DS.'modules'.DS.'mod_jshopping_cart_ext_plus'.DS."js".DS."highlight.js"); }?>

<?php if ($view_cart=='1') {?>
<style type="text/css">
<?php $fancy_css = $params->get('fancy_css');
	print $fancy_css;  ?>
	</style>
<div class="mycart_wrapp">
  <div><a id="autoopen" class="various fancybox.ajax" href="<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>?tmpl=component" title="Товары в корзине"><span class="mycart_headertxt"><?php print $mycart_name; ?>&darr;&uarr;</span></a>  Товаров (<?php print $cart->count_product?>)
  на сумму <span style="display:inline-block"><?php print formatprice($cart->getSum(0,1))?></span><?php if ($mycart_show_image_cart){ ?><a href = "<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>" title="Перейти в корзину для оформления покупки"><?php if (count($cart->products) < 1){?><img style="margin-bottom:-2px;margin-left: 3px;" src="/modules/mod_jshopping_cart_ext_plus/img/cart.png" /><?php } else {?><img style="margin-bottom:-2px;margin-left: 3px;" src="/modules/mod_jshopping_cart_ext_plus/img/cart.gif" /><?php }?></a><?php }?>
  </div>
</div>
<link href="/modules/mod_jshopping_cart_ext_plus/fb/jquery.fancybox.css" rel="stylesheet" />
<script src="/modules/mod_jshopping_cart_ext_plus/fb/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
 jQuery(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
</script>
<?php if ($show_popup=='1') require_once (JPATH_SITE.DS.'modules'.DS.'mod_jshopping_cart_ext_plus'.DS."js".DS."autoopen.js"); ?>

<?php	} elseif ($view_cart=='2') {?>
 
 <style type="text/css">
<?php $fancy_css = $params->get('fancy_css');
	print $fancy_css;  ?>
	</style>
 <div class="mycart_wrapp">
  <div><a id="autoopen" class="various" href="#inline" title="Товары в корзине"><span class="mycart_headertxt"><?php print $mycart_name; ?>&darr;&uarr;</span></a> 
  Товаров (<?php print $cart->count_product?>)
  на сумму <span style="display:inline-block"><?php print formatprice($cart->getSum(0,1))?></span><?php if ($mycart_show_image_cart){ ?><a href = "<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>" title="Перейти в корзину для оформления покупки"><?php if (count($cart->products) < 1){?><img style="margin-bottom:-2px;margin-left: 3px;" src="/modules/mod_jshopping_cart_ext_plus/img/cart.png" /><?php } else {?><img style="margin-bottom:-2px;margin-left: 3px;" src="/modules/mod_jshopping_cart_ext_plus/img/cart.gif" /><?php }?></a><?php }?>
  </div>
<div id="inline" style="display:none">
<div id="jshop_module_cart" class="mycart_content">
<form action="<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=refresh')?>" method="post" name="updateCartMy">
<table class = "module_cart_detail" width = "100%">
       <tr class="headertab">
         <th>Наименование</th>
         <th>Количество</th>
         <th>Цена</th>
        <?php if ($show_image) { ?>
        <th>Изображение</th>
        <?php } ?>
        <th>Краткое описание товара</th>
        <th>X</th>
       </tr>
<?php 
  $countprod = 0;
  $array_products = array();
  foreach($cart->products as $key_id=>$value){
    $array_products [$countprod] = $value;
?>      <tr class="<?php  if ( ($countprod + 2) % 2 > 0) { print 'odd'; } else { print 'even'; }  ?>">
        <?php if ($link_prod=='0') {?>
        <td class="name"><a class="fancybox-type-iframe" data-fancybox-type="iframe" href="<?php print $value['href']?>?tmpl=component"><?php print $array_products [$countprod]["product_name"]; ?></a><br/>
        <?php if ($show_attr=='1') {?>
								<?php print sprintAtributeInCart($value['attributes_value']); }?>
        <?php if ($show_extra=='1') {?>
        <?php print sprintFreeExtraFiledsInCart($value['extra_fields']); }?>
        </td>
        <?php } else {?>
        <td class="name"><?php print $value["product_name"]; ?><br/>
        <?php if ($show_attr=='1') {?>
								<?php print sprintAtributeInCart($value['attributes_value']); }?>
        <?php if ($show_extra=='1') {?>
        <?php print sprintFreeExtraFiledsInCart($value['extra_fields']); }?>
        </td>
        <?php }?>
        <td class="qtty">
        <input type = "text" name = "quantity[<?php print $key_id ?>]" value = "<?php print $value["quantity"]; ?>" class = "inputbox" style = "width: 25px" />
        <img class="imgupdate" style="cursor:pointer;margin-bottom:-5px" src="/modules/mod_jshopping_cart_ext_plus/img/reload.png" title="<?php print _JSHOP_UPDATE_CART ?>" alt = "<?php print _JSHOP_UPDATE_CART ?>" onclick="document.updateCartMy.submit();" />
        </td>
        <td class="summ"><?php print formatprice($value["price"] * $value["quantity"]); ?>        </td>
        <?php
								if ($show_image) { ?>
        <td class="mycart_img">
        <img src="<?php print $jshopConfig->image_product_live_path?>/<?php if ($value["thumb_image"]!='') print $array_products [$countprod]["thumb_image"]; else print $noimage ?>"/>
       <?php }?>
        </td>
        <td class="sd">
        <?php
								$sd=$value["product_id"];
	       $db = &JFactory::getDBO();
	       $Query = "SELECT `short_description_ru-RU` 	FROM #__jshopping_products WHERE product_id=$sd";
		      $db->setQuery($Query);
		      $sdru = $db->loadResult();
		      print $sdru;?>
        </td>
        <td>
<a style="text-decoration:none" href="<?php print $value["href_delete"];?>" onclick="return confirm('<?php print _JSHOP_REMOVE?>')" title="Удалить"><span style="background-color:#DA3924;color:#FFF;font-weight:700;border-radius:15px;-moz-border-radius:15px;-webkit-border-radius:15px;box-shadow:0 0 2px #000 inset;-moz-box-shadow:0 0 2px #000 inset;-webkit-box-shadow:0 0 2px #000 inset;padding:0px 5px;">x</span></a>
        </td>
     </tr>
      <?php $countprod++; }?>
</table>
</form>

<table width = "100%">
<tr>
    <td>
      <!-- <span id = "jshop_quantity_products"><?php print $cart->count_product?></span>&nbsp;<?php print JText::_('PRODUCTS')?> -->
      <span id = "jshop_quantity_products"><strong><?php print JText::_('SUM_TOTAL')?>:</strong>&nbsp;</span>&nbsp;
    </td>
    <td>
      <span id = "jshop_summ_product" class="mycart_summ"><strong><?php print formatprice($cart->getSum(0,1))?></strong></span>
    </td>
</tr>
<tr>
    <td colspan="2" align="right" class="goto_cart">
    <a href="<?php print SEFLink('index.php?option=com_jshopping&controller=category',1)?>" title="Продолжить покупки">Вернуться в магазин</a>&nbsp;&nbsp;&nbsp;&nbsp;
     <a href = "<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>" title="Перейти в корзину для управления товарами"><?php print JText::_('GO_TO_CART')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php if ($jshopConfig->shop_user_guest==1){
            print SEFLink('index.php?option=com_jshopping&controller=checkout&task=step2&check_login=1',1, 0, $jshopConfig->use_ssl);
        }else{
            print SEFLink('index.php?option=com_jshopping&controller=checkout&task=step2',1, 0, $jshopConfig->use_ssl);
        } ?>"><?php print _JSHOP_CHECKOUT ?></a>
    </td>
</tr>
</table>
</div>
</div>
</div>
<link href="/modules/mod_jshopping_cart_ext_plus/fb/jquery.fancybox.css" rel="stylesheet" />
<script src="/modules/mod_jshopping_cart_ext_plus/fb/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
 jQuery(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	jQuery('.fancybox-type-iframe').fancybox({
	maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		afterClose: function(){
parent.location.reload(true);}
});
});
</script>
<?php if ($show_popup=='1') require_once (JPATH_SITE.DS.'modules'.DS.'mod_jshopping_cart_ext_plus'.DS."js".DS."autoopen.js"); ?>

<?php } elseif  ($view_cart=='3') {?>
	<style type="text/css">
<?php $mycart_css = $params->get('mycart_css');
	print $mycart_css;  ?>
	.mycart_img img{width:<?php $mycart_img_width = $params->get('mycart_img_width');
	print $mycart_img_width;?>px;height:<?php $mycart_img_height = $params->get('mycart_img_height');
	print $mycart_img_height;?>px;}
 </style>
 <?php if ($show_cart=='1'){?>
 <style type="text/css">
/*.mycart_wrapp #mycart{display:none!important}
.mycart_wrapp:hover #mycart{display:block!important}*/
 </style>
<script type="text/javascript">
jQuery(document).ready(function() {
jQuery(".mycart_headertxt").mouseover(function(){
jQuery("div#mycart").show("slow");
});

jQuery("div.mycart_content").mouseleave(function(){
jQuery("div#mycart").hide("slow");
});
});
</script>
  <?php 
		require_once (JPATH_SITE.DS.'modules'.DS.'mod_jshopping_cart_ext_plus'.DS."js".DS."collaps.js");} 
		else {require_once (JPATH_SITE.DS.'modules'.DS.'mod_jshopping_cart_ext_plus'.DS."js".DS."collaps.js");} ?>
   
<div class="mycart_wrapp">
  <div><span title="Товары в козине" class="mycart_headertxt" onclick="collapsElement('mycart')"><?php $mycart_name = $params->get('mycart_name');
   print $mycart_name;  ?>&darr;&uarr;</span> 
  Товаров (<?php print $cart->count_product?>)
  на сумму <span style="display:inline-block"><?php print formatprice($cart->getSum(0,1))?></span><?php $mycart_show_image_cart=$params->get('mycart_show_image_cart',1); if ($mycart_show_image_cart){ ?><a href = "<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>" title="Перейти в корзину для оформления покупки"><?php if (count($cart->products) < 1){?><img style="margin-bottom:-2px;margin-left: 3px;" src="/modules/mod_jshopping_cart_ext_plus/img/cart.png" /><?php } else {?><img style="margin-bottom:-2px;margin-left: 3px;" src="/modules/mod_jshopping_cart_ext_plus/img/cart.gif" /><?php }?></a><?php }?>
  </div>
<div id="mycart" style="display: none">
<div id="jshop_module_cart" class="mycart_content">
<table class = "module_cart_detail" width = "100%">
<?php 
  $countprod = 0;
  $array_products = array();
  foreach($cart->products as $key_id=>$value){
    $array_products [$countprod] = $value;
				?>
      <tr class="<?php  if ( ($countprod + 2) % 2 > 0) { print 'odd'; } else { print 'even'; }  ?>">
      <?php
								$show_image = $params->get('show_image',1);
								if ($show_image) { ?>
        <td class="mycart_img">
        <img src="<?php print $jshopConfig->image_product_live_path?>/<?php if ($value["thumb_image"]!='') print $value["thumb_image"]; else print $noimage ?>" title="<?php
								$sd=$array_products [$countprod]["product_id"];
	       $db = &JFactory::getDBO();
	       $Query = "SELECT `short_description_ru-RU` 	FROM #__jshopping_products WHERE product_id=$sd";
		      $db->setQuery($Query);
		      $sdru = $db->loadResult();
		      print $sdru;?>" />
        </td>
        <?php }?>
        <?php if ($link_prod=='0') {?>
        <td class="name"><a class="fancybox-type-iframe" data-fancybox-type="iframe" href="<?php print $value['href']?>?tmpl=component"><?php print $value["product_name"]; ?></a><br/>
        <?php if ($show_attr=='1') {?>
								<?php print sprintAtributeInCart($value['attributes_value']); }?>
        <?php if ($show_extra=='1') {?>
        <?php print sprintFreeExtraFiledsInCart($value['extra_fields']); }?>
        </td>
        <?php } else {?>
        <td class="name"><?php print $value["product_name"]; ?><br/>
        <?php if ($show_attr=='1') {?>
								<?php print sprintAtributeInCart($value['attributes_value']); }?>
        <?php if ($show_extra=='1') {?>
        <?php print sprintFreeExtraFiledsInCart($value['extra_fields']); }?>
        </td>
        <?php }?>
        <td class="qtty">(<?php print $value["quantity"]; ?>)</td>
        <td class="summ"><?php print formatprice($value["price"] * $value["quantity"]); ?></td>
        <td>
<a style="text-decoration:none" href="<?php print $value["href_delete"];?>" onclick="return confirm('<?php print _JSHOP_REMOVE?>')" title="Удалить"><span style="background-color:#DA3924;color:#FFF;font-weight:700;border-radius:15px;-moz-border-radius:15px;-webkit-border-radius:15px;box-shadow:0 0 2px #000 inset;-moz-box-shadow:0 0 2px #000 inset;-webkit-box-shadow:0 0 2px #000 inset;padding:0px 5px;">x</span></a>
        </td>
        </tr>
      <?php $countprod++; }?>
</table>
<table width = "100%">
<tr>
<?php if (count($cart->products) >= 1) {?>
    <td>
     <span id = "jshop_quantity_products"><strong><?php print JText::_('SUM_TOTAL')?>:</strong>&nbsp;</span>&nbsp;
    </td>
    <td>
      <span id = "jshop_summ_product" class="mycart_summ"><strong><?php print formatprice($cart->getSum(0,1))?></strong></span>
    </td>
    <?php } else print '<td>Ваша корзина пуста</td>';?>
    
</tr>
<?php if (count($cart->products) >= 1) {?>
<tr>
    <td colspan="2" align="right" class="goto_cart">
     <a href = "<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>" title="Перейти в корзину для управления товарами"><?php print JText::_('GO_TO_CART')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php if ($jshopConfig->shop_user_guest==1){
            print SEFLink('index.php?option=com_jshopping&controller=checkout&task=step2&check_login=1',1, 0, $jshopConfig->use_ssl);
        }else{
            print SEFLink('index.php?option=com_jshopping&controller=checkout&task=step2',1, 0, $jshopConfig->use_ssl);
        } ?>"><?php print _JSHOP_CHECKOUT ?></a>
    </td>
</tr>
<?php }?>
</table>
<?php if (count($cart->products) >= 1) {?>
<link href="/modules/mod_jshopping_cart_ext_plus/fb/jquery.fancybox.css" rel="stylesheet" />
<script src="/modules/mod_jshopping_cart_ext_plus/fb/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
 jQuery(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	jQuery('.fancybox-type-iframe').fancybox({
	maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		afterClose: function(){
parent.location.reload(true);}
});
});
</script>
<?php if ($show_popup=='1') {?> 

<div><a id="autoopen" title="Товары в козине" class="various" href="#inline2" style="display:none"> + </a> </div>
<div id="inline2" style="display:none">
<div id="jshop_module_cart">
<form action="<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=refresh')?>" method="post" name="updateCartMy">
<table class = "module_cart_detail" width = "100%">
<?php 
  $countprod = 0;
  $array_products = array();
  foreach($cart->products as $key_id=>$value){
    $array_products [$countprod] = $value;
				?>
      <tr class="<?php  if ( ($countprod + 2) % 2 > 0) { print 'odd'; } else { print 'even'; }  ?>">
        <?php if ($link_prod=='0') {?>
        <td class="name"><a class="fancybox-type-iframe"  data-fancybox-type="iframe" href="<?php print $value['href']?>?tmpl=component"><?php print $value["product_name"]; ?></a><br/>
        <?php if ($show_attr=='1') {?>
								<?php print sprintAtributeInCart($value['attributes_value']); }?>
        <?php if ($show_extra=='1') {?>
        <?php print sprintFreeExtraFiledsInCart($value['extra_fields']); }?>
        </td>
        <?php } else {?>
        <td class="name"><?php print $value["product_name"]; ?><br/>
        <?php if ($show_attr=='1') {?>
								<?php print sprintAtributeInCart($value['attributes_value']); }?>
        <?php if ($show_extra=='1') {?>
        <?php print sprintFreeExtraFiledsInCart($value['extra_fields']); }?>
        </td>
        <?php }?>
       <td class="qtty"><input type = "text" name = "quantity[<?php print $key_id ?>]" value = "<?php print $value["quantity"]; ?>" class = "inputbox" style = "width: 25px" />
        <img class="imgupdate" style="cursor:pointer;margin-bottom:-5px" src="/modules/mod_jshopping_cart_ext_plus/img/reload.png" title="<?php print _JSHOP_UPDATE_CART ?>" alt = "<?php print _JSHOP_UPDATE_CART ?>" onclick="document.updateCartMy.submit();" /></td>
        <td class="summ"><?php print formatprice($value["price"] * $value["quantity"]); ?>        </td>
        <?php
								$show_image = $params->get('show_image',1);
								if ($show_image) { ?>
        <td class="mycart_img">
        <img src="<?php print $jshopConfig->image_product_live_path?>/<?php if ($value["thumb_image"]!='') print $value["thumb_image"]; else print $noimage ?>" title="<?php
								$sd=$value["product_id"];
	       $db = &JFactory::getDBO();
	       $Query = "SELECT `short_description_ru-RU` 	FROM #__jshopping_products WHERE product_id=$sd";
		      $db->setQuery($Query);
		      $sdru = $db->loadResult();
		      print $sdru;?>" />
        </td>
        <?php }?>
         <td>
<a style="text-decoration:none" href="<?php print $value["href_delete"];?>" onclick="return confirm('<?php print _JSHOP_REMOVE?>')" title="Удалить"><span style="background-color:#DA3924;color:#FFF;font-weight:700;border-radius:15px;-moz-border-radius:15px;-webkit-border-radius:15px;box-shadow:0 0 2px #000 inset;-moz-box-shadow:0 0 2px #000 inset;-webkit-box-shadow:0 0 2px #000 inset;padding:0px 5px;">x</span></a>
        </td>
     </tr>
      <?php $countprod++; }?>
</table>
</form>
<table width = "100%">
<tr>
<?php if (count($cart->products) >= 1) {?>
    <td>
      <span id = "jshop_quantity_products"><strong><?php print JText::_('SUM_TOTAL')?>:</strong>&nbsp;</span>&nbsp;
    </td>
    <td>
      <span id = "jshop_summ_product" class="mycart_summ"><strong><?php print formatprice($cart->getSum(0,1))?></strong></span>
    </td>
    <?php } else print '<td>Ваша корзина пуста</td>';?>
    
</tr>
<?php if (count($cart->products) >= 1) {?>
<tr>
    <td colspan="2" align="right" class="goto_cart">
     <a href = "<?php print SEFLink('index.php?option=com_jshopping&controller=cart&task=view', 1)?>" title="Перейти в корзину для управления товарами"><?php print JText::_('GO_TO_CART')?></a>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php if ($jshopConfig->shop_user_guest==1){
            print SEFLink('index.php?option=com_jshopping&controller=checkout&task=step2&check_login=1',1, 0, $jshopConfig->use_ssl);
        }else{
            print SEFLink('index.php?option=com_jshopping&controller=checkout&task=step2',1, 0, $jshopConfig->use_ssl);
        } ?>"><?php print _JSHOP_CHECKOUT ?></a>
    </td>
</tr>
<?php }?>
</table>
</div></div>
<?php require_once (JPATH_SITE.DS.'modules'.DS.'mod_jshopping_cart_ext_plus'.DS."js".DS."autoopen.js"); ?>

<?php }
if ($show_popup=='2') { ?>
<script type="text/javascript">
jQuery(document).ready(function() {
if (jQuery("dd").is(":contains('Товар добавлен в корзину.')")){
	jQuery("div#mycart").show("slow");
		setTimeout( "jQuery('div#mycart').hide('slow');",7000);
}
});
</script>

<?php 
}}?>
</div>
</div>
</div>
<?php }?>