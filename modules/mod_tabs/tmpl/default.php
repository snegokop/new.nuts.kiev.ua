<?php
/**
* @title		Shape Mod Tabs
* @version		1.0
* @package		Joomla
* @website		http://www.shape5.com
* @copyright	Copyright (C) 2009 Shape 5 LLC. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<style type="text/css">
  #mod_tabs_selector {
       padding-left:5px;
       margin-top: -10px;
  }
  #mod_tabs_selector li {
    list-style: none;
    display: inline-block;
    border:1px solid #EAE9E5;
    border-radius:5px 5px 0 0;
    border-bottom:none;
    line-height:30px;
    padding:0 7px;
    background:#FFF;
    font-style: italic;
    margin:0 2px 0 2px;
    text-transform:uppercase;
    cursor:pointer;  
    color: #F6AC49;
    font-family: Georgia, "Times New Roman", Times, sant-serif;
  }
  #mod_tabs_selector li.active{
    background:#F6AC49;
    cursor:text;
    color: #FFF;
  }
  #mod_tabs_end{
      margin-bottom:-35px;
  }
 
</style>

<ul id="mod_tabs_selector">
  <?php 
  $_params = json_decode($module->params);
  require_once JPATH_ROOT.'/components/com_jshopping/lib/factory.php';
  $lang = JSFactory::getLang();
  $db= JFactory::getDBo();
  $db->setQuery('SELECT `id`, `'.$lang->get('name').'` AS `name` FROM `#__jshopping_product_labels` WHERE `id` IN('.implode(",", $_params->labelids).')');
  $labels = $db->loadAssocList();
  
  if(!isset($attribs['cookie_label_id'])){
      $_cookies = JFactory::getApplication()->input->cookie;
      if(!$cookie_label = $_cookies->get("label_id", false)){
          $cookie_label = "";
          $_cookies->set("label_id", $cookie_label);
      }
  }
  $ids = new stdClass();
  $j = 0;
  foreach($labels as $label){?>
  <li<?php 
  $ids->$j = $label["id"]; $j++; 
  if($label["id"]==$cookie_label){?> class="active"<?php }?>><?php echo $label["name"];?></li>
  <?php } ?>
  <?php if($_params->bestsellers!="0"){?><li<?php if($cookie_label==""){?> class="active"<?php }?>>ПОПУЛЯРНЫЕ</li><?php }?>
</ul>
<script>
    jQuery(document).ready(function(){
       var $ = jQuery;
       var labels = JSON.parse('<?php echo json_encode($ids);?>');
       var start = true;
       $("#mod_tabs_selector").find('li').each(function(i){
           $(this).click(function(){
               if(this.classList=="active" || !start) return;
               if(undefined!==labels[i] && labels[i]!=null){
                   document.cookie="label_id="+labels[i]+"; path=/";
               }
               else document.cookie="label_id=; path=/";
               start = false;
               $("#mod_tabs_selector").find('li').removeClass('active');
               var _this = this;
               $($('#innertop').find('.bestseller_products')[0]).html('<center><img style="margin-top:50px;" src="/images/img/icons/loading2.gif"></center>')
               //return; 
               $.get('/?module=mod_tabs&ajax=1',{}, 
               function(data){
                   var new_html =  $('<div style="display:none;">'+data+'</div>');
                   $('body').append(new_html);
                   var content = $($(new_html).find('.bestseller_products')[0]).html();
                   $($('#innertop').find('.bestseller_products')[0]).html(content);
                   $(new_html).remove(); 
                   $(_this).addClass('active');
                   start = true;
                   return;
               }, 'html');
               return;
           });
       });       
    });
</script>
<div id="mod_tabs_end"></div>