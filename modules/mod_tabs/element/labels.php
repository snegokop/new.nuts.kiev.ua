<?php
class JFormFieldLabels extends JFormField {
    
  protected function getInput(){
      
        require_once JPATH_ROOT.'/components/com_jshopping/lib/factory.php';
        $lang = JSFactory::getLang();
        $db = JFactory::getDbo();
        $db->setQuery('SELECT `'.$lang->get('name').'` AS `name`,`id` FROM `#__jshopping_product_labels`');
        $result = $db->loadAssocList();
        
        $ctrl  =  $this->name ; 
        $ctrl          .= '[]'; 
        $value        = empty($this->value) ? '' : $this->value; 
        $return = JHTML::_('select.genericlist', $result, $ctrl, 'class="inputbox" id = "labels_ordering" multiple="multiple"', 'id', 'name', $value);
        return $return;
  }
}
?>
