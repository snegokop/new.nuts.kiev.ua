<?php 
$file = end(explode("/", rtrim($_SERVER['REQUEST_URI'], "/")));
//file_put_contents(__DIR__."/request.txt", $file);
if($file!="" && file_exists(__DIR__."/".$file)){
	$mime = strtolower(end(explode(".", $file)));
	if($mime=="jpg")$mime = "jpeg";
	header("Content-type:image/".$mime);
	echo file_get_contents(__DIR__."/".$file);
	die;
}
header("Content-type:image/gif");
echo file_get_contents(__DIR__."/noimage.gif");
?>