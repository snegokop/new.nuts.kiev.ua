<?php
  /**
* @version      3.3.0 20.12.2010
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

define('_JSHOP_TYPE_ADMIN_ATRIBUT', 'Art der Anzeige Attribut(unabhängig) in admin');
define('_JSHOP_TYPE_SELECT', 'Select (Default)');
define('_JSHOP_TYPE_AS_DATE', 'Display as Datum');
define('_JSHOP_TYPE_AS_TEXT', 'Display as Text'); 
?>
