<?php
/**
* @version      3.3.0 12.12.2010
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

define('_JSHOP_TYPE_ADMIN_ATRIBUT', 'Тип отображения атрибута(независимого) в админке');
define('_JSHOP_TYPE_SELECT', 'Select (По умолчанию)');
define('_JSHOP_TYPE_AS_DATE', 'Отображать как дата');
define('_JSHOP_TYPE_AS_TEXT', 'Отображать как текст'); 
?>
