<?php
/**
* @version      3.3.0 20.12.2010
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

define('_JSHOP_TYPE_ADMIN_ATRIBUT', 'Type of display attribute(Independent) in admin');
define('_JSHOP_TYPE_SELECT', 'Select (Default)');
define('_JSHOP_TYPE_AS_DATE', 'Display as date');
define('_JSHOP_TYPE_AS_TEXT', 'Display as text'); 
?>
