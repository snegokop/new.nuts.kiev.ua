<?php
/**
* @version      1.3.3
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2012 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

define('_JSHOP_EDIT_STATE','Edit state');
define('_JSHOP_NEW_STATE','New state');
define('_JSHOP_LIST_STATE','State list');
define('_JSHOP_STATE_DELETED','State was deleted');
define('_JSHOP_STATES','States');


?>