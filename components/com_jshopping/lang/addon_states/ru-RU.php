<?php
/**
* @version      1.3.3
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2012 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

define('_JSHOP_EDIT_STATE','Редактировать штат/область');
define('_JSHOP_NEW_STATE','Новый штат/область');
define('_JSHOP_LIST_STATE','Список штатов/областей');
define('_JSHOP_STATE_DELETED','Штат/область удалён');
define('_JSHOP_STATES','Штаты/области');

?>