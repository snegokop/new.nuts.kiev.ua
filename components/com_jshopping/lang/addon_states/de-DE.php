<?php
  /**
* @version      1.3.3
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2012 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

define('_JSHOP_EDIT_STATE','Bundesland bearbeiten');
define('_JSHOP_NEW_STATE','Neues Bundesland');
define('_JSHOP_LIST_STATE','Bundesländerliste');
define('_JSHOP_STATE_DELETED','Bundesland wurde gelöscht');
define('_JSHOP_STATES','Bundesländer');


?>
