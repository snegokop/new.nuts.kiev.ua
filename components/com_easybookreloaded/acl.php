<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

/**
 * This code segment sets the correct permission rights of the user
 */
$canAdd = false;
$canEdit = false;

// Get all groups which are associated with the user
$user = JFactory::getUser();
$userGroup = JAccess::getGroupsByUser($user->id);

// Get the specified groups from the parameters
$params = JComponentHelper::getParams('com_easybookreloaded');
$addAclArray = $params->get('add_acl', array(1));
$adminAclArray = $params->get('admin_acl', array(8));

foreach ($userGroup as $value) {
    foreach ($addAclArray as $addAclValue) {
        if ($value == $addAclValue) {
            $canAdd = true;
            break;
        }
    }

    foreach ($adminAclArray as $adminAclValue) {
        if ($value == $adminAclValue) {
            $canEdit = true;
            break;
        }
    }
}

// Defines constants for adding and editing permission rights
define('EASYBOOK_CANADD', $canAdd);
define('EASYBOOK_CANEDIT', $canEdit);
