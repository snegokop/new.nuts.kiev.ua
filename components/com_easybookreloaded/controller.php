<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedController extends JControllerLegacy
{
    protected $input;

    public function display($cachable = false, $urlparams = false)
    {
        parent::display();

        $this->input = JFactory::getApplication()->input;
    }

    /**
     * This function is triggered when the user clicks on the publish link the notification mail
     */
    public function publishMail()
    {
        $hashRequest = $this->input->getString('hash');
        $checkHash = $this->performMail($hashRequest);
        $gbId = $this->input->getInt('gbid');

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_CHANGE_PUBLISH_STATUS');
        $type = 'error';

        if ($checkHash == true) {
            $model = $this->getModel('entry');

            switch ($model->publish()) {
                case -1:
                    $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_CHANGE_PUBLISH_STATUS');
                    $type = 'error';
                    break;
                case 0:
                    $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_UNPUBLISHED');
                    $type = 'success';
                    break;
                case 1:
                    $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_PUBLISHED');
                    $type = 'success';
                    break;
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false), $message, $type);
    }

    private function performMail($hashRequest)
    {
        // No empty hash value allowed
        if (empty($hashRequest)) {
            return false;
        }

        // Prepare request string
        $hashData = explode('-', $hashRequest);

        // The hash_data array must have 2 entries - ID of entry and the hash itself
        if (count($hashData) != 2) {
            return false;
        }

        if (empty($hashData[0]) OR !is_numeric($hashData[0])) {
            return false;
        }

        // Check whether the ID is available and numeric - load the entry for the checks
        $model = $this->getModel('entry');
        $gbRow = $model->getRow($hashData[0]);

        // Check whether the hash link is still valid
        $params = JComponentHelper::getParams('com_easybookreloaded');

        $app = JFactory::getApplication();
        $offset = $app->get('offset');

        $dateEntry = JFactory::getDate($gbRow->get('gbdate'), $offset);
        $dateNow = JFactory::getDate('now', $offset);

        $validTimeEmailnot = $params->get('valid_time_emailnot') * 60 * 60 * 24;

        if ($dateEntry->toUnix() + $validTimeEmailnot <= $dateNow->toUnix()) {
            return false;
        }

        // Create a second hash link from the same data and compare it with the transmitted hash value
        $hash = array();
        $hash['id'] = (int) $gbRow->get('id');
        $hash['gbmail'] = md5($gbRow->get('gbmail'));
        $hash['username'] = $gbRow->get('gbname');

        // Get config object for the secret word and sitename
        $config = JFactory::getConfig();
        $hash['custom_secret'] = $config->get('secret');

        $secretWord = $params->get('secret_word');

        if (!empty($secretWord)) {
            $hash['custom_secret'] = $params->get('secret_word');
        }

        $hash = substr(base64_encode(md5(serialize($hash))), 0, 16);

        if ($hash != $hashData[1]) {
            return false;
        }

        return true;
    }

    /**
     * This function is triggered when the user clicks on the remove link the notification mail
     */
    public function removeMail()
    {
        $hashRequest = $this->input->getString('hash');
        $checkHash = $this->performMail($hashRequest);
        $gbId = $this->input->getInt('gbid');

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_ENTRY_COULD_NOT_BE_DELETED');
        $type = 'error';

        if ($checkHash == true) {
            $model = $this->getModel('entry');

            if ($model->delete()) {
                $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_DELETED');
                $type = 'success';
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false), $message, $type);
    }

    /**
     * This function is triggered when the user clicks on the comment link the notification mail
     */
    public function commentMail()
    {
        $hashRequest = $this->input->getString('hash');
        $checkHash = $this->performMail($hashRequest);
        $gbId = $this->input->getInt('gbid');

        if ($checkHash == true) {
            $this->input->set('view', 'entry');
            $this->input->set('layout', 'commentform_mail');
            $this->input->set('hidemainmenu', 1);
            parent::display();

            return;
        }

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_SAVE_COMMENT');
        $type = 'error';
        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false), $message, $type);
    }

    /**
     * This function is triggered when the user saves the comment form which was called from the notification mail
     */
    public function savecommentMail()
    {
        $hashRequest = $this->input->getString('hash');
        $checkHash = $this->performMail($hashRequest);
        $gbId = $this->input->getInt('gbid');

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_SAVE_COMMENT');
        $type = 'error';

        if ($checkHash == true) {
            $model = $this->getModel('entry');

            if ($row = $model->saveComment()) {
                // Change state of the guestbook entry
                if (isset($row['toggle_state']) AND $row['toggle_state'] == 1) {
                    $model->publish();
                }

                $message = JText::_('COM_EASYBOOKRELOADED_COMMENT_SAVED');

                if (isset($row['inform']) AND $row['inform'] == 1) {
                    $data = $model->getRow($row['id']);
                    $uri = JUri::getInstance();
                    $mail = JFactory::getMailer();
                    $params = JComponentHelper::getParams('com_easybookreloaded');
                    require_once(JPATH_COMPONENT . '/helpers/route.php');

                    $href = $uri->base() . EasybookReloadedHelperRoute::getEasybookReloadedRoute($data->get('id'), $gbId);
                    $mail->setSubject(JText::_('COM_EASYBOOKRELOADED_ADMIN_COMMENT_SUBJECT'));
                    $mail->setBody(JText::sprintf('COM_EASYBOOKRELOADED_ADMIN_COMMENT_BODY', $data->get('gbname'), $uri->base(), $href));

                    if ($params->get('send_mail_html')) {
                        $mail->isHtml(true);
                        $mail->setBody(JText::sprintf('COM_EASYBOOKRELOADED_ADMIN_COMMENT_BODY_HTML', $data->get('gbname'), $uri->base(), $href));
                    }

                    $mail->addRecipient($data->get('gbmail'));
                    $mail->Send();

                    $message = JText::_('COM_EASYBOOKRELOADED_COMMENT_SAVED_INFORM');
                }

                $type = 'success';
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false), $message, $type);
    }

    /**
     * This function is triggered when the user clicks on the edit link the notification mail
     */
    public function editMail()
    {
        $hashRequest = $this->input->getString('hash');
        $checkHash = $this->performMail($hashRequest);
        $gbId = $this->input->getInt('gbid');

        if ($checkHash == true) {
            $this->input->set('view', 'entry');
            $this->input->set('layout', 'form_mail');
            parent::display();

            return;
        }

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_PLEASE_VALIDATE_YOUR_INPUTS');
        $type = 'error';
        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false), $message, $type);
    }

    /**
     * This function is triggered when the user saves the edit form which was called from the notification mail
     */
    public function saveMail()
    {
        $hashRequest = $this->input->getString('hash');
        $checkHash = $this->performMail($hashRequest);
        $gbId = $this->input->getInt('gbid');

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_SAVE_COMMENT');
        $type = 'error';

        if ($checkHash == true) {
            $params = JComponentHelper::getParams('com_easybookreloaded');

            // Reset the time to avoid error in the spam check
            $session = JFactory::getSession();
            $time = $session->get('time', null, 'easybookreloaded');
            $session->set('time', $time - $params->get('type_time_sec'), 'easybookreloaded');

            $model = $this->getModel('entry');

            if ($model->store()) {
                $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_SAVED_BUT_HAS_TO_BE_APPROVED');
                $type = 'message';

                if ($params->get('default_published', true)) {
                    $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_SAVED');
                    $type = 'success';
                }
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false), $message, $type);
    }
}
