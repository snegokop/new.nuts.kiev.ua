<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedViewEasybookReloaded extends JViewLegacy
{
    protected $params;
    protected $entries;
    protected $gbData;
    protected $count;
    protected $pagination;
    protected $heading;
    protected $entryId;
    protected $gbId;

    function display($tpl = null)
    {
        require_once(JPATH_COMPONENT . '/helpers/content.php');
        $this->params = JComponentHelper::getParams('com_easybookreloaded');

        // Check whether a guestbook ID is set in the request
        $gbId = $this->get('GbId');

        if (is_null($gbId) OR !is_int($gbId)) {
            return parent::display('error');
        }

        // Get the required data
        $this->entries = $this->get('Data');
        $this->gbData = $this->get('GbData');
        $this->count = $this->get('Total');
        $this->pagination = $this->get('Pagination');
        $this->entryId = JFactory::getApplication()->input->getInt('entryid', null);
        $this->gbId = $gbId;

        // Set the head data
        $this->addHeadData();

        // Remove cache from Page Cache plugin if required
        EasybookReloadedHelperContent::cleanCache($gbId);

        if ($gbId == 0) {
            parent::setLayout('all');

            return parent::display(null);
        }

        parent::display($tpl);
    }

    private function addHeadData()
    {
        $document = JFactory::getDocument();

        // Set CSS File
        $cssFile = 'easybookreloaded';
        $template = $this->params->get('template', 0);

        if ($template == 1) {
            $cssFile .= 'dark';
        } elseif ($template == 2) {
            $cssFile .= 'transparent';
        }

        $document->addStyleSheet(JUri::root() . 'components/com_easybookreloaded/css/' . $cssFile . '.css');

        // JavaScript
        JHtml::_('jquery.framework');
        $jsCode = 'jQuery(document).ready(function() {
                    jQuery(".easy_top", this).each(function() {
                        jQuery(this).css("cursor", "pointer");
                        jQuery(this).click(function() {
                            window.location.href = "?entryid="+jQuery(this).attr("data-id");
                        });
                    });
                });';

        $document->addScriptDeclaration($jsCode);

        // Show RSS Feed
        $link = '&format=feed&limitstart=';
        $attribs = array('type' => 'application/rss+xml', 'title' => 'RSS 2.0');
        $document->addHeadLink(JRoute::_($link . '&type=rss'), 'alternate', 'rel', $attribs);
        $attribs = array('type' => 'application/atom+xml', 'title' => 'Atom 1.0');
        $document->addHeadLink(JRoute::_($link . '&type=atom'), 'alternate', 'rel', $attribs);

        // Add meta data from menu link
        $menus = JMenu::getInstance('site');
        $menu = $menus->getActive();

        if (!empty($menu)) {
            if ($menu->params->get('menu-meta_description')) {
                $document->setDescription($menu->params->get('menu-meta_description'));
            }

            if ($menu->params->get('menu-meta_keywords')) {
                $document->setMetaData('keywords', $menu->params->get('menu-meta_keywords'));
            }

            if ($menu->params->get('robots')) {
                $document->setMetaData('robots', $menu->params->get('robots'));
            }
        }

        $this->heading = $document->getTitle();

        // Add HTML Head Link
        if (method_exists($document, 'addHeadLink')) {
            $paginationData = $this->pagination->getData();

            if ($paginationData->start->link) {
                $document->addHeadLink($paginationData->start->link, 'first');
            }

            if ($paginationData->previous->link) {
                $document->addHeadLink($paginationData->previous->link, 'prev');
            }

            if ($paginationData->next->link) {
                $document->addHeadLink($paginationData->next->link, 'next');
            }

            if ($paginationData->end->link) {
                $document->addHeadLink($paginationData->end->link, 'last');
            }
        }
    }
}
