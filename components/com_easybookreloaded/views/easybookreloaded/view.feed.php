<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedViewEasybookReloaded extends JViewLegacy
{
	function display($tpl = null)
	{
		// Get the data from the model
		$items = $this->get('Data');
		$gbData = $this->get('GbData');

		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$document->link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid='.$gbData->id);
		JFactory::getApplication()->input->set('limit', $app->get('feed_limit'));

		// Get the Item ID of the menu entry
		require_once(JPATH_SITE.'/components/com_easybookreloaded/helpers/route.php');
		$itemId = EasybookReloadedHelperRoute::getItemId($gbData->id);

		foreach($items as $item)
		{
			if(!empty($item->gbtitle))
			{
				$title = html_entity_decode($this->escape($item->gbtitle.' - '.$item->gbname));
			}
			else
			{
				$title = html_entity_decode($this->escape($item->gbname));
			}

			// Create correct link to the entry
			$limit = EasybookReloadedHelperRoute::getLimitstart($item->id);

			$linkRaw = 'index.php?option=com_easybookreloaded&view=easybookreloaded';
			$linkRaw .= '&gbid='.$gbData->id;
			$linkRaw .= '&Itemid='.$itemId;

			if(!empty($limit))
			{
				$linkRaw .= '&limitstart='.$limit;
			}

			$linkRaw .= '#gbentry_'.$item->id;
			$link = JRoute::_($linkRaw);
			$description = $item->gbtext;
			$date = ($item->gbdate ? date('r', strtotime($item->gbdate)) : '');

			// Add prepared entry to the feed
			$feedItem = new JFeedItem();
			$feedItem->title = $title;
			$feedItem->link = $link;
			$feedItem->description = $description;
			$feedItem->date = $date;
			$feedItem->category = 'Guestbook';

			// Add entry to RSS array
			$document->addItem($feedItem);
		}
	}
}
