<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedViewEntry extends JViewLegacy
{
    protected $params;
    protected $session;
    protected $user;
    protected $entry;
    protected $heading;
    protected $gbId;

    function display($tpl = null)
    {
        require_once(JPATH_COMPONENT . '/helpers/content.php');
        require_once(JPATH_COMPONENT . '/helpers/smilie.php');

        $this->params = JComponentHelper::getParams('com_easybookreloaded');
        $this->session = JFactory::getSession();
        $this->user = JFactory::getUser();
        $this->entry = $this->get('Data');
        $this->entry->ip = EasybookReloadedHelperContent::getIpAddress();

        // Get the correct guestbook ID
        $this->gbId = (int) JFactory::getSession()->get('gbid', false, 'easybookreloaded');

        if (empty($this->gbId)) {
            return parent::display('error');
        }

        // Load antispam checks (to the session)
        $this->get('CalcCheck');
        $this->addHeadData();

        // Remove cache from Page Cache plugin if required
        EasybookReloadedHelperContent::cleanCache($this->gbId);

        parent::display($tpl);
    }

    private function addHeadData()
    {
        $document = JFactory::getDocument();

        // Set CSS File
        $cssFile = 'easybookreloaded';
        $template = $this->params->get('template', 0);

        if ($template == 1) {
            $cssFile .= 'dark';
        } elseif ($template == 2) {
            $cssFile .= 'transparent';
        }

        $document->addStyleSheet(JUri::root() . 'components/com_easybookreloaded/css/' . $cssFile . '.css');

        $task = JFactory::getApplication()->input->getWord('task');

        switch ($task) {
            case 'add':
                $this->heading = $document->getTitle() . " - " . JText::_('COM_EASYBOOKRELOADED_SIGN_GUESTBOOK');
                break;
            case 'edit' || 'editMail':
                $this->heading = $document->getTitle() . " - " . JText::_('COM_EASYBOOKRELOADED_EDIT_ENTRY');
                break;
            case 'comment' || 'commentMail':
                $this->heading = $document->getTitle() . " - " . JText::_('COM_EASYBOOKRELOADED_EDIT_COMMENT');
                break;
        }

        $document->addScriptDeclaration($this->addBbcodeJs($this->params, $task), 'text/javascript');

        if ($this->params->get('show_rating', 1)) {
            JHtml::_('behavior.framework');
            $document->addScript('components/com_easybookreloaded/scripts/moostarrating.js', 'text/javascript');

            $showRatingType = $this->params->get('show_rating_type', 1);

            if ($showRatingType == 0) {
                $document->addCustomTag('<script type="text/javascript">
                        //<![CDATA[
                        window.addEvent("load", function() {
                                MooStarRatingImages.defaultImageFolder = "' . JUri::base() . 'components/com_easybookreloaded/images";
                                var Rating = new MooStarRating({ form: "gbookForm", radios: "gbvote", imageEmpty: "sun_empty.png", imageFull:  "sun_full.png", imageHover: "sun_hover.png", tip: "<em>[VALUE] / [COUNT]</em>", tipTarget: $("easybookvotetip"), tipTargetType: "html"  });
                        });
                        //]]>
                </script>');
            } elseif ($showRatingType == 1) {
                $document->addCustomTag('<script type="text/javascript">
                        //<![CDATA[
                        window.addEvent("load", function() {
                                MooStarRatingImages.defaultImageFolder = "' . JUri::base() . 'components/com_easybookreloaded/images";
                                var Rating = new MooStarRating({ form: "gbookForm", radios: "gbvote", imageEmpty: "star_empty.png", imageFull:  "star_full.png", imageHover: "star_hover.png", tip: "<em>[VALUE] / [COUNT]</em>", tipTarget: $("easybookvotetip"), tipTargetType: "html"  });
                        });
                        //]]>
                </script>');
            } elseif ($showRatingType == 2) {
                $document->addCustomTag('<script type="text/javascript">
                        //<![CDATA[
                        window.addEvent("load", function() {
                                MooStarRatingImages.defaultImageFolder = "' . JUri::base() . 'components/com_easybookreloaded/images";
                                var Rating = new MooStarRating({ form: "gbookForm", radios: "gbvote", imageEmpty: "star_boxed_empty.png", imageFull:  "star_boxed_full.png", imageHover: "star_boxed_hover.png", width: 17, tip: "<em>[VALUE] / [COUNT]</em>", tipTarget: $("easybookvotetip"), tipTargetType: "html" });
                        });
                        //]]>
                </script>');
            }
        }
    }

    private function addBbcodeJs($params, $task = 'add')
    {
        if ($task == 'add' || $task == 'edit' || $task == 'editMail') {
            $textareaName = 'gbtext';
        } elseif ($task == 'comment' || $task == 'commentMail') {
            $textareaName = 'gbcomment';
        }

        $js = 'function x()
            {
                return;
            }

            function insertprompt(insert, input, start, end, revisedMessage, currentMessage)
            {
                // Internet Explorer
                if (typeof document.selection != \'undefined\')
                {
                    var range = document.selection.createRange();
                    range.text = insert;
                    var range = document.selection.createRange();
                    range.move(\'character\', 0);
                    range.select();
                }
                // Gecko Software
                else if (typeof input.selectionStart != \'undefined\')
                {
                    revisedMessage = currentMessage.substr(0, start) + insert + currentMessage.substr(end);
                    document.gbookForm.' . $textareaName . '.value=revisedMessage;
                    document.gbookForm.' . $textareaName . '.focus();
                    var pos;
                    pos = start + insert.length;
                    input.selectionStart = pos;
                    input.selectionEnd = pos;
                }
            }

            function insert(aTag, eTag)
            {
                var input = document.forms[\'gbookForm\'].elements[\'' . $textareaName . '\'];
                input.focus();
                // Internet Explorer
                if(typeof document.selection != \'undefined\')
                {
                    var range = document.selection.createRange();
                    var insText = range.text;
                    range.text = aTag + insText + eTag;
                    range = document.selection.createRange();
                    if (insText.length == 0)
                    {
                        range.move(\'character\', -eTag.length);
                    }
                    else
                    {
                        range.moveStart(\'character\', aTag.length + insText.length + eTag.length);
                    }
                    range.select();
                }
                // Gecko Software
                else if (typeof input.selectionStart != \'undefined\')
                {
                    var start = input.selectionStart;
                    var end = input.selectionEnd;
                    var insText = input.value.substring(start, end);
                    input.value = input.value.substr(0, start) + aTag + insText + eTag + input.value.substr(end);
                    var pos;
                    if (insText.length == 0)
                    {
                        pos = start + aTag.length;
                    }
                    else
                    {
                        pos = start + aTag.length + insText.length + eTag.length;
                    }
                    input.selectionStart = pos;
                    input.selectionEnd = pos;
                }
                else
                {
                    var pos;
                    var re = new RegExp(\'^[0-9]{0,3}$\');
                    while (!re.test(pos))
                    {
                        pos = prompt("Einfügen an Position (0.." + input.value.length + "):", "0");
                    }
                    if (pos > input.value.length)
                    {
                        pos = input.value.length;
                    }
                    var insText = prompt("Bitte geben Sie den zu formatierenden Text ein:");
                    input.value = input.value.substr(0, pos) + aTag + insText + eTag + input.value.substr(pos);
                }
            }

            function insertsmilie(thesmile)
            {
                var input = document.forms[\'gbookForm\'].elements[\'' . $textareaName . '\'];
                input.focus();
                // Internet Explorer
                if(typeof document.selection != \'undefined\')
                {
                    var range = document.selection.createRange();
                    var insText = range.text;
                    range.text = " "+thesmile+" ";
                    range = document.selection.createRange();
                    range.move(\'character\', 0);
                    range.select();
                }
                // Gecko Software
                else if (typeof input.selectionStart != \'undefined\')
                {
                    var start = input.selectionStart;
                    var end = input.selectionEnd;
                    var insText = input.value.substring(start, end);
                    input.value = input.value.substr(0, start) + " "+thesmile+" " + input.value.substr(end);
                    var pos;
                    pos = start + (thesmile.length + 2);
                    input.selectionStart = pos;
                    input.selectionEnd = pos;
                }
                else
                {
                    var pos;
                    var re = new RegExp(\'^[0-9]{0,3}$\');
                    while (!re.test(pos))
                    {
                        pos = prompt("Einfügen an Position (0.." + input.value.length + "):", "0");
                    }
                    if (pos > input.value.length)
                    {
                        pos = input.value.length;
                    }
                    var insText = prompt("Bitte geben Sie den zu formatierenden Text ein:");
                    input.value = input.value.substr(0, pos) + aTag + insText + eTag + input.value.substr(pos);
                }
            }';

        if ($params->get('support_bbcode', true)) {
            $js .= 'function DoPrompt(action)
            {
                var input = document.forms[\'gbookForm\'].elements[\'' . $textareaName . '\'];
                input.focus();

                var start = input.selectionStart;
                var end = input.selectionEnd;
                var revisedMessage;
                var currentMessage = document.gbookForm.' . $textareaName . '.value;';

            if ($params->get('support_link', true)) {
                $js .= 'if (action == "url")
                    {
                        var thisURL = prompt("' . JText::_("COM_EASYBOOKRELOADED_ENTER_THE_URL_HERE") . '", "http://");
                        var thisTitle = prompt("' . JText::_("COM_EASYBOOKRELOADED_ENTER_THE_WEB_PAGE_TITLE") . '", "' . JText::_("COM_EASYBOOKRELOADED_WEB_PAGE_TITLE") . '");
                        if (thisURL != undefined && thisTitle != undefined)
                        {
                            if  (thisURL != "" && thisTitle != "")
                            {
                                var urlBBCode = "[URL="+thisURL+"]"+thisTitle+"[/URL]";
                                insertprompt(urlBBCode, input, start, end, revisedMessage, currentMessage);
                            }
                        }
                        return;
                    }';
            }

            if ($params->get('support_mail', true)) {
                $js .= 'if (action == "email")
                {
                    var thisEmail = prompt("' . JText::_("COM_EASYBOOKRELOADED_ENTER_THE_EMAIL_ADDRESS") . '", "");
                    if (thisEmail != undefined)
                    {
                        if  (thisEmail != "")
                        {
                            var emailBBCode = "[EMAIL]"+thisEmail+"[/EMAIL]";
                            insertprompt(emailBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }';
            }

            $js .= 'if (action == "code")
                {
                    var thisLanguage = prompt("' . JText::_("COM_EASYBOOKRELOADED_WHICH_LANGUAGE") . '", "");
                    if (thisLanguage != undefined)
                    {
                        if  (thisLanguage != "")
                        {
                            var codeBBCode = "[CODE="+thisLanguage+"]\n\n[/CODE]";
                            insertprompt(codeBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }
                if (action == "youtube")
                {
                    var thisYoutube = prompt("' . JText::_("COM_EASYBOOKRELOADED_YOUTUBE_VIDEO_ID") . '", "");
                    if (thisYoutube != undefined)
                    {
                        if  (thisYoutube != "")
                        {
                            var codeBBCode = "[YOUTUBE]"+thisYoutube+"[/YOUTUBE]";
                            insertprompt(codeBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }';

            if ($params->get('support_pic', true)) {

                $js .= 'if (action == "image")
                {
                    var thisImage = prompt("' . JText::_("COM_EASYBOOKRELOADED_ENTER_THE_URL_OF_THE_PICTURE_YOU_WANT_TO_SHOW") . '", "http://");
                    if (thisImage != undefined)
                    {
                        if  (thisImage != "")
                        {
                            var imageBBCode = "[IMG]"+thisImage+"[/IMG]";
                            insertprompt(imageBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }
                if (action == "image_link")
                {
                    var thisImage = prompt("' . JText::_("COM_EASYBOOKRELOADED_ENTER_THE_URL_OF_THE_PICTURE_YOU_WANT_TO_SHOW") . '", "http://");
                    var thisURL = prompt("' . JText::_("COM_EASYBOOKRELOADED_ENTER_THE_URL_HERE") . '", "http://");
                    if (thisImage != undefined && thisURL != undefined)
                    {
                        if  (thisImage != "" && thisURL != "")
                        {
                            var imageBBCode = "[IMGLINK="+thisURL+"]"+thisImage+"[/IMGLINK]";
                            insertprompt(imageBBCode, input, start, end, revisedMessage, currentMessage);
                        }
                    }
                    return;
                }';
            }

            $js .= '}';
        }

        return $js;
    }
}
