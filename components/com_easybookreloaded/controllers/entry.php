<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedControllerEntry extends JControllerLegacy
{
    protected $input;

    public function __construct()
    {
        parent::__construct();

        $this->input = JFactory::getApplication()->input;
    }

    public function add()
    {
        $this->addEdit();
    }

    private function addEdit()
    {
        $params = JComponentHelper::getParams('com_easybookreloaded');
        $id = $this->input->getInt('cid', 0);

        if ((($id == 0 && EASYBOOK_CANADD) || ($id != 0 && EASYBOOK_CANEDIT)) && !$params->get('offline')) {
            $this->input->set('view', 'entry');
            $this->input->set('layout', 'form');
            parent::display();

            return;
        }

        $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . JFactory::getSession()->get('gbid', false, 'easybookreloaded'), false);
        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_RIGHTS');
        $type = 'message';
        $this->setRedirect($link, $message, $type);
    }

    public function edit()
    {
        $this->addEdit();
    }

    /**
     * Saves the entry and inform the administrator(s) or show an error message to entry creator
     */
    public function save()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $this->cleanCache();

        $params = JComponentHelper::getParams('com_easybookreloaded');
        $id = $this->input->getInt('id', 0);
        $gbId = JFactory::getSession()->get('gbid', false, 'easybookreloaded');

        if ((($id == 0 && EASYBOOK_CANADD) || ($id != 0 && EASYBOOK_CANEDIT)) && !$params->get('offline')) {
            $model = $this->getModel('entry');

            // Store the entered data, create an output message and send the notification mail
            if ($row = $model->store()) {
                $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_SAVED_BUT_HAS_TO_BE_APPROVED');
                $type = 'notice';

                if ($params->get('default_published', true)) {
                    $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_SAVED');
                    $type = 'success';
                }

                $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false);

                // Send mail if it is a new entry and the send mail option is activated
                if ($id == 0 && $params->get('send_mail', true)) {
                    // Reference needed objects and prepare the requested variables for the mail
                    require_once(JPATH_COMPONENT . '/helpers/route.php');
                    $mail = JFactory::getMailer();
                    $uri = JUri::getInstance();
                    $db = JFactory::getDbo();

                    // Load all request variables - because JInput doesn't allow to load the whole data at once, a workaround
                    //is used. This was easily possible with the deprecated JRequest (e.g. JRequest::get('post');)
                    $dataTemp = $_REQUEST;
                    array_walk($dataTemp, function(&$dataTemp) {
                        $dataTemp = htmlspecialchars(strip_tags(trim($dataTemp)));
                    });

                    // Get unfiltered request variable is only with a trick with JInput possible, so direct access is used instead
                    // Possible solution: list($gbtext) = ($this->_input->get('gbtext', array(0), 'array') - use the filter array
                    // With JRequest one could use - JRequest::getVar('gbtext', NULL, 'post', 'none', JREQUEST_ALLOWRAW)
                    // Update: Now possible with RAW filter parameter in newer Joomla! versions, due to B/C no changes here
                    $dataTemp['gbtext'] = htmlspecialchars($_REQUEST['gbtext'], ENT_QUOTES);

                    $name = $dataTemp['gbname'];
                    $text = $dataTemp['gbtext'];
                    $ip = '0.0.0.0';

                    if ($params->get('enable_log', true)) {
                        require_once(JPATH_COMPONENT . '/helpers/content.php');
                        $ip = EasybookReloadedHelperContent::getIpAddress();
                    }

                    $title = '';

                    if (!empty($dataTemp['gbtitle'])) {
                        $title = $dataTemp['gbtitle'];
                    }

                    // Get config object for the secret word, sitename and email settings
                    $config = JFactory::getConfig();

                    $hash = array();
                    $hash['id'] = (int) $row->get('id');
                    $hash['gbmail'] = md5($row->get('gbmail'));
                    $hash['username'] = $row->get('gbname');

                    // Get the custom secret word. If no word was set, take the Joomla! secret word from the configuration
                    $hash['custom_secret'] = $config->get('secret');
                    $secretWord = $params->get('secret_word');

                    if (!empty($secretWord)) {
                        $hash['custom_secret'] = $params->get('secret_word');
                    }

                    $hash = substr(base64_encode(md5(serialize($hash))), 0, 16);
                    $hashId = $row->get('id') . '-' . $hash;

                    $href = $uri::base() . EasybookReloadedHelperRoute::getEasybookReloadedRoute($row->get('id'), $gbId);
                    $hashmailPublish = $uri::base() . EasybookReloadedHelperRoute::getEasybookReloadedRouteHash('publishMail', $gbId) . $hashId;
                    $hashmailComment = $uri::base() . EasybookReloadedHelperRoute::getEasybookReloadedRouteHash('commentMail', $gbId) . $hashId;
                    $hashmailEdit = $uri::base() . EasybookReloadedHelperRoute::getEasybookReloadedRouteHash('editMail', $gbId) . $hashId;
                    $hashmailDelete = $uri::base() . EasybookReloadedHelperRoute::getEasybookReloadedRouteHash('removeMail', $gbId) . $hashId;

                    // Mail subject - get the name of the website and add it to the subject
                    $siteName = $config->get('sitename');
                    $mail->setSubject(JText::sprintf('COM_EASYBOOKRELOADED_NEW_GUESTBOOKENTRY', $siteName));
                    $mail->setBody(JText::sprintf('COM_EASYBOOKRELOADED_A_NEW_GUESTBOOKENTRY_HAS_BEEN_WRITTEN', $uri::base(), $name, $title, $text, $href, $hashmailPublish, $hashmailComment, $hashmailEdit, $hashmailDelete, $ip));

                    if ($params->get('send_mail_html')) {
                        $mail->isHtml(true);
                        $mail->setBody(JText::sprintf('COM_EASYBOOKRELOADED_A_NEW_GUESTBOOKENTRY_HAS_BEEN_WRITTEN_HTML', $uri::base(), $name, $title, $text, $href, $hashmailPublish, $hashmailComment, $hashmailEdit, $hashmailDelete, $ip));
                    }

                    // Get mail addresses for the notification mail
                    $admins = array();
                    $emailfornotificationUsergroupArray = $params->get('emailfornotification_usergroup', array(8));

                    foreach ($emailfornotificationUsergroupArray as $emailfornotificationUsergroup) {
                        $query = "SELECT " . $db->quoteName('email') . " FROM " . $db->quoteName('#__users') . " AS A, " . $db->quoteName('#__user_usergroup_map') . " AS B WHERE " . $db->quoteName('B.group_id') . " = " . $db->quote($emailfornotificationUsergroup) . " AND " . $db->quoteName('B.user_id') . " = " . $db->quoteName('A.id') . " AND " . $db->quoteName('A.sendEmail') . " = 1";
                        $db->setQuery($query);
                        $result = $db->loadRowList();

                        if (!empty($result)) {
                            foreach ($result as $value) {
                                $admins[] = $value[0];
                            }
                        }
                    }

                    if ($params->get('emailfornotification')) {
                        $emailfornotification = array_map('trim', explode(',', $params->get('emailfornotification')));

                        foreach ($emailfornotification as $email) {
                            $admins[] = $email;
                        }
                    }

                    // Set recipient and reply to addresses
                    $replyTo = $row->get('gbmail');

                    if (empty($replyTo)) {
                        $replyTo = $config->get('mailfrom');
                    }

                    $mail->addRecipient($admins);
                    $mail->addReplyTo($replyTo, $row->get('gbname'));
                    $mail->setSender(array($config->get('mailfrom'), $config->get('fromname')));

                    // Which mail type should be used? Default is PHP mail
                    if ($config->get('mailer') == 'sendmail') {
                        $mail->useSendmail($config->get('sendmail'));
                    } elseif ($config->get('mailer') == 'smtp') {
                        $mail->useSmtp($config->get('smtpauth'), $config->get('smtphost'), $config->get('smtpuser'), $config->get('smtppass'), $config->get('smtpsecure'), $config->get('smtpport'));
                    }

                    // Send the mail
                    $mail->Send();
                }

                $this->setRedirect($link, $message, $type);

                return;
            }

            $errorsOutput = array();
            $errorsArray = array_keys(JFactory::getSession()->get('errors', null, 'easybookreloaded'));

            if ((in_array('easycalccheck', $errorsArray)) || (in_array('easycalccheck_time', $errorsArray))) {
                if (in_array('easycalccheck_time', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_EASYCALCCHECK_TIME');
                } else {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_EASYCALCCHECK');
                }
            } elseif (in_array('akismet', $errorsArray)) {
                $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_AKISMET');
            } elseif (in_array('gbid', $errorsArray)) {
                $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_GBID');
            } elseif (in_array('easycalccheck_question', $errorsArray)) {
                $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_SPAMCHECKQUESTION');
            } else {
                if (in_array('name', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_NAME');
                }

                if (in_array('mail', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_MAIL');
                }

                if (in_array('title', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_TITLE');
                }

                if (in_array('text', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_TEXT');
                }

                if (in_array('aim', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_AIM');
                }

                if (in_array('icq', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_ICQ');
                }

                if (in_array('yah', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_YAH');
                }

                if (in_array('skype', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_SKYPE');
                }

                if (in_array('msn', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_MSN');
                }

                if (in_array('toomanylinks', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_TOOMANYLINKS');
                }

                if (in_array('iptimelock', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_TIMELOCK');
                }

                if (in_array('eugdpr', $errorsArray)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_ERROR_EUGDPR');
                }

                if (empty($errorsOutput)) {
                    $errorsOutput[] = JText::_('COM_EASYBOOKRELOADED_UNKNOWNERROR');
                }
            }

            $errors = implode(', ', $errorsOutput);

            $message = JText::sprintf('COM_EASYBOOKRELOADED_PLEASE_VALIDATE_YOUR_INPUTS', $errors);
            $link = JRoute::_('index.php?option=com_easybookreloaded&controller=entry&task=add&retry=true', false);
            $type = 'error';

            JFactory::getSession()->clear('errors', 'easybookreloaded');
            $this->setRedirect($link, $message, $type);

            return;
        }

        $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false);
        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_RIGHTS');
        $type = 'message';
        $this->setRedirect($link, $message, $type);
    }

    /**
     * Cleans the cached pages of the component by the system cache plugin
     *
     * @deprecated Used due to B/C reasons for older Joomla! versions
     */
    private function cleanCache()
    {
        // Clean page cache if System Cache plugin is enabled
        if (JPluginHelper::isEnabled('system', 'cache')) {
            $gbId = JFactory::getSession()->get('gbid', false, 'easybookreloaded');
            $cacheUrls = array(
                'index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId,
                'index.php?option=com_easybookreloaded&controller=entry&task=add',
                'index.php?option=com_easybookreloaded&controller=entry&task=add&retry=true'
            );

            foreach ($cacheUrls as $cacheUrl) {
                $this->cleanCacheProcess($cacheUrl);
            }
        }

        return;
    }

    private function cleanCacheProcess($cacheUrl)
    {
        $jUri = JUri::getInstance();
        $jUri->setPath(JRoute::_($cacheUrl, false));

        $cacheId = $jUri->toString(array('host', 'port', 'scheme', 'path', 'query', 'fragment'));

        if (version_compare(JVersion::RELEASE, '3.8', 'ge')) {
            $cacheId = md5(serialize(array($cacheId)));
        }

        return JCache::getInstance('page')->remove($cacheId, 'page');
    }

    /**
     * Calls the comment form if user has the correct permission rights
     */
    public function comment()
    {
        if (EASYBOOK_CANEDIT) {
            $this->input->set('view', 'entry');
            $this->input->set('layout', 'commentform');
            $this->input->set('hidemainmenu', 1);
            parent::display();

            return;
        }

        $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . JFactory::getSession()->get('gbid', false, 'easybookreloaded'), false);
        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_RIGHTS');
        $type = 'message';
        $this->setRedirect($link, $message, $type);
    }

    /**
     * Removes an entry from the database
     */
    public function remove()
    {
        $this->cleanCache();

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_RIGHTS');
        $type = 'message';

        if (EASYBOOK_CANEDIT) {
            $model = $this->getModel('entry');

            $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_DELETED');
            $type = 'success';

            if (!$model->delete()) {
                $message = JText::_('COM_EASYBOOKRELOADED_ERROR_ENTRY_COULD_NOT_BE_DELETED');
                $type = 'error';
            }
        }

        $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . JFactory::getSession()->get('gbid', false, 'easybookreloaded'), false);
        $this->setRedirect($link, $message, $type);
    }

    /**
     * Changes the status of the entry - online / offline
     */
    public function publish()
    {
        $this->cleanCache();

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_RIGHTS');
        $type = 'message';

        if (EASYBOOK_CANEDIT) {
            $model = $this->getModel('entry');

            switch ($model->publish()) {
                case -1:
                    $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_CHANGE_PUBLISH_STATUS');
                    $type = 'error';
                    break;
                case 0:
                    $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_UNPUBLISHED');
                    $type = 'success';
                    break;
                case 1:
                    $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_PUBLISHED');
                    $type = 'success';
                    break;
            }
        }

        $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . JFactory::getSession()->get('gbid', false, 'easybookreloaded'), false);
        $this->setRedirect($link, $message, $type);
    }

    /**
     * Saves the comment of the administrator and inform the entry creator
     */
    public function saveComment()
    {
        $this->cleanCache();

        $gbId = JFactory::getSession()->get('gbid', false, 'easybookreloaded');
        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_RIGHTS');
        $type = 'message';

        if (EASYBOOK_CANEDIT) {
            JSession::checkToken() || jexit('Invalid Token');
            $model = $this->getModel('entry');

            if (!$row = $model->saveComment()) {
                $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_SAVE_COMMENT');
                $type = 'error';
            } else {
                $message = JText::_('COM_EASYBOOKRELOADED_COMMENT_SAVED');

                if (isset($row['inform']) && $row['inform'] == 1) {
                    $data = $model->getRow($row['id']);
                    $uri = JUri::getInstance();
                    $mail = JFactory::getMailer();
                    $params = JComponentHelper::getParams('com_easybookreloaded');
                    require_once(JPATH_COMPONENT . '/helpers/route.php');

                    $href = $uri::base() . EasybookReloadedHelperRoute::getEasybookReloadedRoute($data->get('id'), $gbId);

                    $mail->setSubject(JText::_('COM_EASYBOOKRELOADED_ADMIN_COMMENT_SUBJECT'));
                    $mail->setBody(JText::sprintf('COM_EASYBOOKRELOADED_ADMIN_COMMENT_BODY', $data->get('gbname'), $uri::base(), $href));

                    if ($params->get('send_mail_html')) {
                        $mail->isHtml(true);
                        $mail->setBody(JText::sprintf('COM_EASYBOOKRELOADED_ADMIN_COMMENT_BODY_HTML', $data->get('gbname'), $uri::base(), $href));
                    }

                    $config = JFactory::getConfig();

                    $mail->addRecipient($data->get('gbmail'));
                    $mail->setSender(array($config->get('mailfrom'), $config->get('fromname')));

                    // Which mail type should be used? Default is PHP mail
                    if ($config->get('mailer') == 'sendmail') {
                        $mail->useSendmail($config->get('sendmail'));
                    } elseif ($config->get('mailer') == 'smtp') {
                        $mail->useSmtp($config->get('smtpauth'), $config->get('smtphost'), $config->get('smtpuser'), $config->get('smtppass'), $config->get('smtpsecure'), $config->get('smtpport'));
                    }

                    $mail->Send();

                    $message = JText::_('COM_EASYBOOKRELOADED_COMMENT_SAVED_INFORM');
                }

                $type = 'success';
            }
        }

        $link = JRoute::_('index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=' . $gbId, false);
        $this->setRedirect($link, $message, $type);
    }
}
