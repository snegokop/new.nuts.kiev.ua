<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedHelperRoute
{
    /**
     * Creates correct URL to the entry
     *
     * @param $id
     * @param $gbId
     *
     * @return string
     * @throws Exception
     */
    public static function getEasybookReloadedRoute($id, $gbId)
    {
        $itemId = EasybookReloadedHelperRoute::getItemId($gbId);
        $limit = EasybookReloadedHelperRoute::getLimitstart($id);

        $link = 'index.php?option=com_easybookreloaded&view=easybookreloaded';
        $link .= '&gbid=' . $gbId;
        $link .= '&Itemid=' . $itemId;

        if (!empty($limit)) {
            $link .= '&limitstart=' . $limit;
        }

        $link .= '#gbentry_' . $id;

        return $link;
    }

    /**
     * Gets the Item ID of the component - the Item ID is the ID from the menu entry
     *
     * @param int $gbId
     *
     * @return bool|int
     * @throws Exception
     */
    public static function getItemId($gbId = 1)
    {
        // First get the ItemID from the request variable
        $itemIdRequest = JFactory::getApplication()->input->getInt('Itemid');

        // Now also load the ID from the db to get sure that we have a correct ItemID
        $db = JFactory::getDbo();
        $query = "SELECT " . $db->quoteName('id') . " FROM " . $db->quoteName('#__menu') . " WHERE " . $db->quoteName('link') . " = 'index.php?option=com_easybookreloaded&view=easybookreloaded&gbid=" . $gbId . "' AND " . $db->quoteName('published') . " = 1";
        $db->setQuery($query);
        $itemId = (int) $db->loadResult();

        if (!empty($itemId)) {
            return $itemId;
        }

        if ((!empty($itemIdRequest) && empty($itemId)) || ($itemIdRequest == $itemId)) {
            return $itemIdRequest;
        }

        return false;
    }

    /**
     * Gets limitstart to set the correct page with the entry
     *
     * @param int $id
     *
     * @return int
     */
    public static function getLimitstart($id)
    {
        $params = JComponentHelper::getParams('com_easybookreloaded');
        $entriesPerPage = (int) $params->get('entries_perpage', 5);
        $order = $params->get('entries_order', 'DESC');

        $db = JFactory::getDbo();
        $query = "SELECT * FROM " . $db->quoteName('#__easybook') . " WHERE " . $db->quoteName('published') . " = 1 ORDER BY " . $db->quoteName('id') . " " . $order;
        $db->setQuery($query);
        $result = $db->loadRowList();

        foreach ($result as $key => $value) {
            if ($value[0] == $id) {
                break;
            }
        }

        $limit = $entriesPerPage * intval($key / $entriesPerPage);

        return (int) $limit;
    }

    /**
     * Creates correct URL with the task for the hash link in the notification mail
     *
     * @param $task
     * @param $gbId
     *
     * @return string
     * @throws Exception
     */
    public static function getEasybookReloadedRouteHash($task, $gbId)
    {
        $link = 'index.php?option=com_easybookreloaded&task=';

        // Add the task to the URL
        $link .= $task;

        // Add the GB ID and Item ID to the URL
        $link .= '&gbid=' . $gbId;
        $link .= '&Itemid=' . EasybookReloadedHelperRoute::getItemId($gbId);
        $link .= '&hash=';

        return $link;
    }
}
