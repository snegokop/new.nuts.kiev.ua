<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedHelperSmilie
{
    /**
     * Creates an array with the associated image replacements
     *
     * @return array
     */
    public static function getSmilies()
    {
        return array(
            ':zzz'   => 'sm_sleep.gif',
            ';)'     => 'sm_wink.gif',
            '8)'     => 'sm_cool.gif',
            ':p'     => 'sm_razz.gif',
            ':roll'  => 'sm_rolleyes.gif',
            ':eek'   => 'sm_bigeek.gif',
            ':grin'  => 'sm_biggrin.gif',
            ':)'     => 'sm_smile.gif',
            ':sigh'  => 'sm_sigh.gif',
            ':?'     => 'sm_confused.gif',
            ':cry'   => 'sm_cry.gif',
            ':('     => 'sm_mad.gif',
            ':x'     => 'sm_dead.gif',
            ':upset' => 'sm_upset.gif',
        );
    }
}
