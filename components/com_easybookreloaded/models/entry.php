<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedModelEntry extends JModelLegacy
{
    protected $data;
    protected $id;
    protected $badWords;
    protected $input;
    protected $params;
    protected $user;
    protected $session;
    protected $app;

    public function __construct()
    {
        parent::__construct();

        require_once(JPATH_COMPONENT . '/helpers/content.php');
        $this->app = JFactory::getApplication();
        $this->params = JComponentHelper::getParams('com_easybookreloaded');
        $this->user = JFactory::getUser();
        $this->session = JFactory::getSession();
        $this->input = $this->app->input;

        $this->id = $this->input->getInt('cid', 0);

        // Requests from the notification mail do send a valid hash value
        if ($hashRequest = $this->input->getString('hash')) {
            $hashData = explode('-', $hashRequest);

            if (count($hashData) != 2) {
                return false;
            }

            if (!empty($hashData[0]) && is_numeric($hashData[0])) {
                $this->id = $hashData[0];
            }
        }
    }

    /**
     * Stores the guestbook entries into the database
     *
     * @return bool|JTable
     * @throws Exception
     */
    public function store()
    {
        $row = $this->getTable('entry', 'EasybookReloadedTable');

        // Load all request variables - because JInput doesn't allow to load the whole data at once, a workaround
        // is used. This was easily possible with the deprecated JRequest (e.g. JRequest::get('post');)
        $data = $_REQUEST;
        array_walk($data, function(&$data) {
            $data = htmlspecialchars(strip_tags(trim($data)));
        });

        // Get unfiltered request variable is only with a trick with JInput possible, so direct access is used instead
        // Possible solution: list($gbtext) = ($this->_input->get('gbtext', array(0), 'array') - use the filter array
        // With JRequest one could use - JRequest::getVar('gbtext', NULL, 'post', 'none', JREQUEST_ALLOWRAW)
        $data['gbtext'] = htmlspecialchars($_REQUEST['gbtext'], ENT_QUOTES);

        $date = JFactory::getDate();

        if ($this->user->guest == 0 && !EASYBOOK_CANEDIT) {
            $data['gbname'] = $this->user->get('username');

            if ($this->params->get('registered_username')) {
                $data['gbname'] = $this->user->get('name');
            }

            $data['gbmail'] = $this->user->get('email');
        }

        if (!isset($data['id'])) {
            $data['gbdate'] = $date->toSql();
            $data['published'] = $this->params->get('default_published', 1);
            $data['gbip'] = '0.0.0.0';

            if ($this->params->get('enable_log', true)) {
                $data['gbip'] = EasybookReloadedHelperContent::getIpAddress();
            }

            $data['gbcomment'] = null;
        }

        // Validate the entered data
        if (!$this->validate($data)) {
            return false;
        }

        if (!$row->save($data)) {
            throw new Exception(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 404);
        }

        $this->clearSession();

        return $row;
    }

    /**
     * Validates all entered data that was submitted by the user
     *
     * @param $data
     *
     * @return bool
     * @throws Exception
     */
    private function validate(&$data)
    {
        // Load mail helper class - needed for the email address checks
        jimport('joomla.mail.helper');

        $errors = array();
        $error = false;

        // gbid session variable has to be set and the ID in the request has to be the same
        $gbidSession = (int) $this->session->get('gbid', false, 'easybookreloaded');

        // Clear gbid session variable
        $this->session->clear('gbid', 'easybookreloaded');

        if (!empty($gbidSession)) {
            // Ooops, wrong ID submitted - prevent saving of such requests
            if ($data['gbid'] != $gbidSession) {
                unset($data['gbid']);
                $error = true;
                $errors['gbid'] = true;
            }
        } else {
            // No chance, my little friend :-)
            unset($data['gbid']);
            $error = true;
            $errors['gbid'] = true;
        }

        if ($this->params->get('enable_spam_reg') || $this->user->guest) {
            $time = $this->session->get('time', null, 'easybookreloaded');

            if ($time == '') {
                $error = true;
                $errors['sessionvariable'] = true;
            } else {
                if ((time() - $this->params->get('type_time_sec')) <= $time) {
                    $error = true;
                    $errors['easycalccheck_time'] = true;
                }
            }

            if ($this->params->get('enable_spam', true)) {
                $spamcheck1 = $this->session->get('spamcheck1', false, 'easybookreloaded');
                $spamcheck2 = $this->session->get('spamcheck2', false, 'easybookreloaded');
                $spamcheckResult = $this->session->get('spamcheckresult', null, 'easybookreloaded');
                $spamcheckFieldName = $this->session->get('spamcheck_field_name', false, 'easybookreloaded');

                if ((empty($spamcheck1)) || (empty($spamcheck2)) || ($spamcheckResult === null) || (empty($spamcheckFieldName))) {
                    $error = true;
                    $errors['sessionvariable'] = true;
                } else {
                    if ($data[$spamcheckFieldName] === '' || (int) $data[$spamcheckFieldName] != $spamcheckResult) {
                        $error = true;
                        $errors['easycalccheck'] = true;
                    }
                }
            }

            if ($this->params->get('spamcheck_question') && ($this->params->get('spamcheck_question_question') && $this->params->get('spamcheck_question_answer'))) {
                $spamcheckQuestionFieldName = $this->session->get('spamcheck_question_field_name', null, 'easybookreloaded');

                if ($spamcheckQuestionFieldName == '') {
                    $error = true;
                    $errors['sessionvariable'] = true;
                } else {
                    $spamcheckQuestionAnswer = JText::_($this->params->get('spamcheck_question_answer'));

                    if (strtolower($data[$spamcheckQuestionFieldName]) != strtolower($spamcheckQuestionAnswer)) {
                        $error = true;
                        $errors['easycalccheck_question'] = true;
                    }
                }
            }

            // Akismet - Further information: http://akismet.com/
            if ($this->params->get('akismet')) {
                $akismetKey = $this->params->get('akismet_key');

                if ($akismetKey) {
                    require_once(JPATH_COMPONENT . '/helpers/akismet.php');
                    $akismetUrl = JUri::getInstance()->toString();

                    $name = $data['gbname'];
                    $email = $data['gbmail'];
                    $comment = $data['gbtext'];

                    // Add title if provided
                    if (!empty($data['gbtitle'])) {
                        $comment = $data['gbtitle'] . ' ' . $comment;
                    }

                    // Check homepage if provided
                    $url = '';

                    if (!empty($data['gbpage'])) {
                        $url = $data['gbpage'];
                    }

                    $akismet = new Akismet($akismetUrl, $akismetKey);
                    $akismet->setCommentAuthor($name);
                    $akismet->setCommentAuthorEmail($email);
                    $akismet->setCommentAuthorURL($url);
                    $akismet->setCommentContent($comment);

                    if ($akismet->isCommentSpam()) {
                        $error = true;
                        $errors['akismet'] = true;
                    }
                }
            }
        }

        if ($this->params->get('block_ip')) {
            $gbIp = EasybookReloadedHelperContent::getIpAddress();
            $ips = array_map('trim', explode(',', $this->params->get('block_ip')));

            foreach ($ips as $ip) {
                $ipRegexp = str_replace('x', '..?.?', preg_quote($ip));

                if (preg_match('@' . $ipRegexp . '@', $gbIp)) {
                    $error = true;
                    $errors['easycalccheck'] = true;
                }
            }
        }

        if ($this->params->get('timelock_ip') && $this->params->get('enable_log')) {
            $gbIp = EasybookReloadedHelperContent::getIpAddress();
            $dateLastEntry = $this->lastEntryDate($gbIp);

            if (!empty($dateLastEntry)) {
                date_default_timezone_set('UTC');
                $dateBack = strftime("%Y-%m-%d %H:%M:%S", time() - $this->params->get('timelock_ip'));

                if ($dateLastEntry > $dateBack) {
                    $error = true;
                    $errors['iptimelock'] = true;
                }
            }
        }

        if (empty($data['gbname'])) {
            $error = true;
            $errors['name'] = true;
        }

        if (empty($data['gbtext'])) {
            $error = true;
            $errors['text'] = true;
        } else {
            if (preg_match_all('@\[img\].+\[/img\]@isU', $data['gbtext'], $matches)) {
                $text = $data['gbtext'];

                foreach ($matches[0] as $value) {
                    $img = str_replace(array('\'', "\""), '', $value);

                    if (strpos($img, ' ') == true) {
                        $imgNew = substr($img, 0, strpos($img, ' ')) . '[/img]';
                        $text = str_replace($value, $imgNew, $text);
                    }
                }

                $data['gbtext'] = $text;
            }

            if (preg_match_all('@https?://(www\.)?([a-zA-Z0-9-]+\.)?([a-zA-Z0-9-]{3,65})(\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))@is', $data['gbtext'], $matches)) {
                if (count($matches[0]) > $this->params->get('maxnumberlinks')) {
                    $error = true;
                    $errors['toomanylinks'] = true;
                }
            }

            if (preg_match('@\[link.*\].*\[/link\]@isU', $data['gbtext'])) {
                $error = true;
                $errors['easycalccheck'] = true;
            }
        }

        if (!empty($data['gbaim'])) {
            $allowed = '@^[A-Za-z0-9_\.]+$@';

            if (!preg_match($allowed, $data['gbaim'])) {
                $error = true;
                $errors['aim'] = true;
            }
        }

        if (!empty($data['gbicq'])) {
            $allowed = '@^[0-9]+$@';

            if (!preg_match($allowed, $data['gbicq'])) {
                $error = true;
                $errors['icq'] = true;
            }
        }

        if (!empty($data['gbyah'])) {
            $allowed = '@^[A-Za-z0-9_\.]+$@';

            if (!preg_match($allowed, $data['gbyah'])) {
                $error = true;
                $errors['yah'] = true;
            }
        }

        if (!empty($data['gbskype'])) {
            $allowed = '@^[A-Za-z0-9_\.-]+$@';

            if (!preg_match($allowed, $data['gbskype'])) {
                $error = true;
                $errors['skype'] = true;
            }
        }

        if (!empty($data['gbpage'])) {
            $data['gbpage'] = str_replace(array('\'', "\""), '', $data['gbpage']);

            if (strpos($data['gbpage'], ' ') == true) {
                $data['gbpage'] = substr($data['gbpage'], 0, strpos($data['gbpage'], ' '));
            }

            // Add scheme if not provided
            if (!preg_match('@^https?://@i', $data['gbpage'])) {
                $data['gbpage'] = 'http://' . $data['gbpage'];
            }

            $data['gbpage'] = htmlspecialchars($data['gbpage'], ENT_QUOTES);
        }

        if ((!empty($data['gbmail']) || $this->params->get('require_mail', true)) && !JMailHelper::isEmailAddress($data['gbmail'])) {
            $error = true;
            $errors['mail'] = true;
        }

        if (($this->params->get('show_title', true)) && (empty($data['gbtitle']) && $this->params->get('require_title', true))) {
            $error = true;
            $errors['title'] = true;
        } elseif (!empty($data['gbtitle'])) {
            $data['gbtitle'] = htmlspecialchars($data['gbtitle'], ENT_QUOTES);
        }

        if (!empty($data['gbmsn']) && !JMailHelper::isEmailAddress($data['gbmsn'])) {
            $error = true;
            $errors['msn'] = true;
        }

        if ($this->params->get('badwordfilter', true)) {
            $badWords = $this->getBadWordList();
            $badWordFilterRegexp = $this->params->get('badwordfilter_regexp', false);

            if (!empty($badWordFilterRegexp)) {
                foreach ($badWords as $badWord) {
                    $data['gbtext'] = preg_replace('@' . $badWord . '@iU', '***', $data['gbtext']);

                    if (!empty($data['gbtitle'])) {
                        $data['gbtitle'] = preg_replace('@' . $badWord . '@iU', '***', $data['gbtitle']);
                    }
                }
            } else {
                $data['gbtext'] = str_replace($badWords, '***', $data['gbtext']);

                if (!empty($data['gbtitle'])) {
                    $data['gbtitle'] = str_replace($badWords, '***', $data['gbtitle']);
                }
            }
        }

        if ($this->params->get('eugdpr', true) && empty($data['eugdpr'])) {
            $error = true;
            $errors['eugdpr'] = true;
        }

        if ($error == true) {
            $this->session->set('errors', $errors, 'easybookreloaded');
            $this->app->setUserState('eb_validation_errors', $errors);
            $this->app->setUserState('eb_validation_data', $data);

            return false;
        }

        return true;
    }

    /**
     * Checks the latest date of an entry from a specific IP address
     *
     * @param $ip
     *
     * @return mixed
     */
    private function lastEntryDate($ip)
    {
        $query = "SELECT " . $this->_db->quoteName('gbdate') . " FROM " . $this->_db->quoteName('#__easybook') . " WHERE " . $this->_db->quoteName('gbip') . " = " . $this->_db->quote($ip) . " ORDER BY gbdate DESC";
        $this->_db->setQuery($query);

        return $this->_db->loadResult();
    }

    /**
     * Loads all language bad words from the database for the validation check
     *
     * @return mixed
     */
    private function getBadWordList()
    {
        if (empty($this->badWords)) {
            $query = "SELECT " . $this->_db->quoteName('word') . " FROM " . $this->_db->quoteName('#__easybook_badwords') . " ORDER BY length(word) DESC";
            $this->_db->setQuery($query);
            $this->badWords = $this->_db->loadColumn();
        }

        return $this->badWords;
    }

    /**
     * Clears saved session data if entry was stored successfully in the database
     */
    private function clearSession()
    {
        $this->session->clear('spamcheck1', 'easybookreloaded');
        $this->session->clear('spamcheck2', 'easybookreloaded');
        $this->session->clear('spamcheckresult', 'easybookreloaded');
        $this->session->clear('spamcheck_field_name', 'easybookreloaded');
        $this->session->clear('operator', 'easybookreloaded');
        $this->session->clear('time', 'easybookreloaded');
        $this->session->clear('spamcheck_question_field_name', 'easybookreloaded');
    }

    /**
     * Deletes an entry using JTable
     *
     * @return bool
     * @throws Exception
     */
    public function delete()
    {
        $row = $this->getTable('entry', 'EasybookReloadedTable');

        if (!$row->delete($this->id)) {
            throw new Exception(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 404);
        }

        return true;
    }

    /**
     * Changes the status of an entry - online / offline
     *
     * @return int
     * @throws Exception
     */
    public function publish()
    {
        $data = $this->getData();
        $status = (int) !$data->published;

        $query = "UPDATE " . $this->_db->quoteName('#__easybook') . " SET " . $this->_db->quoteName('published') . " = " . $this->_db->quote($status) . " WHERE " . $this->_db->quoteName('id') . " = " . $this->_db->quote($this->id) . " LIMIT 1;";
        $this->_db->setQuery($query);

        if (!$this->_db->execute()) {
            return -1;
        }

        return $status;
    }

    /**
     * Loads the entry data when a form is loaded
     *
     * @return JTable|mixed
     * @throws Exception
     */
    public function getData()
    {
        // Error occurred - load the form with entered data again
        if ($this->input->get('retry') == 'true') {
            $this->data = $this->getTable('entry', 'EasybookReloadedTable');
            $this->data->bind($this->app->getUserState('eb_validation_data'));
        }

        // Modification process of an existing entry
        if (empty($this->data) && !empty($this->id)) {
            $query = "SELECT * FROM " . $this->_db->quoteName('#__easybook') . " WHERE " . $this->_db->quoteName('id') . " = " . $this->_db->quote($this->id);
            $this->_db->setQuery($query);
            $this->data = $this->_db->loadObject();
        }

        // First loading of the form - new entry
        if (empty($this->data)) {
            $this->data = $this->getTable('entry', 'EasybookReloadedTable');
            $this->data->id = 0;

            if ($this->user->get('id')) {
                if ($this->params->get('registered_username')) {
                    $this->data->gbname = $this->user->get('name');
                } else {
                    $this->data->gbname = $this->user->get('username');
                }

                $this->data->gbmail = $this->user->get('email');
            }

            // Okay, if we get here, then the gbid must be set. If not, then we have a direct call - bad idea!
            $gbIdSession = $this->session->get('gbid', false, 'easybookreloaded');

            if (empty($gbIdSession)) {
                // Do not interrupt, let the fools play their game ;-)
                $this->session->set('gbid', false, 'easybookreloaded');
            }
        } else {
            $this->session->set('gbid', $this->data->gbid, 'easybookreloaded');
        }

        return $this->data;
    }

    /**
     * Creates math exercise and saves values to the session for the validation process
     */
    public function getCalcCheck()
    {
        if ($this->params->get('enable_spam_reg') || $this->user->guest) {
            $this->session->set('time', time(), 'easybookreloaded');

            if ($this->params->get('enable_spam', true)) {
                $spamcheck1 = mt_rand(1, $this->params->get('max_value', 20));
                $spamcheck2 = mt_rand(1, $this->params->get('max_value', 20));
                $spamcheckResult = $spamcheck1 + $spamcheck2;
                $operatorOutput = '+';
                $operator = mt_rand(0, 1);

                if ($this->params->get('operator') == 1 || ($this->params->get('operator') == 2 && $operator == 1)) {
                    $spamcheckResult = $spamcheck1 - $spamcheck2;
                    $operatorOutput = '-';
                }

                $spamcheckFieldName = $this->getRandomValue();

                $this->session->set('spamcheck1', $spamcheck1, 'easybookreloaded');
                $this->session->set('spamcheck2', $spamcheck2, 'easybookreloaded');
                $this->session->set('spamcheckresult', $spamcheckResult, 'easybookreloaded');
                $this->session->set('spamcheck_field_name', $spamcheckFieldName, 'easybookreloaded');
                $this->session->set('operator', $operatorOutput, 'easybookreloaded');
            }

            if ($this->params->get('spamcheck_question') && ($this->params->get('spamcheck_question_question') && $this->params->get('spamcheck_question_answer'))) {
                $spamcheckQuestionFieldName = $this->getRandomValue();
                $this->session->set('spamcheck_question_field_name', $spamcheckQuestionFieldName, 'easybookreloaded');
            }
        }
    }

    /**
     * Creates a random string for the calc check field ID
     *
     * @return string
     */
    private function getRandomValue()
    {
        $randomString = '';

        // first character has to be a letter
        $characters = range('a', 'z');
        $randomString .= $characters[mt_rand(0, 25)];

        // other characters arbitrarily
        $numbers = range(0, 9);
        $characters = array_merge($characters, $numbers);

        $stringLength = mt_rand(4, 12);

        for ($i = 0; $i < $stringLength; $i++) {
            $randomString .= $characters[mt_rand(0, 35)];
        }

        return $randomString;
    }

    /**
     * Saves the comment from authorized users with admin rights for the component
     *
     * @return bool
     * @throws Exception
     */
    public function saveComment()
    {
        $row = $this->getTable('entry', 'EasybookReloadedTable');

        // Load all request variables - because JInput doesn't allow to load the whole data at once, a workaround
        // is used. This was easily possible with the deprecated JRequest (e.g. JRequest::get('post');)
        $data = $_REQUEST;
        array_walk($data, function(&$data) {
            $data = htmlspecialchars(strip_tags(trim($data)));
        });

        // Get unfiltered request variable is only with a trick with JInput possible, so direct access is used instead
        // Possible solution: list($gbtext) = ($this->_input->get('gbtext', array(0), 'array') - use the filter array
        // With JRequest one could use - JRequest::getVar('gbtext', NULL, 'post', 'none', JREQUEST_ALLOWRAW)
        $data['gbcomment'] = htmlspecialchars($_REQUEST['gbcomment'], ENT_QUOTES);

        // gbid sessian variable has to be set and the ID in the request has to be the same
        $gbIdSession = (int) $this->session->get('gbid', false, 'easybookreloaded');

        // Clear gbid session variable
        $this->session->clear('gbid', 'easybookreloaded');

        if (empty($gbIdSession)) {
            return false;
        }

        // Ooops, wrong ID - prevent saving of such requests
        if ($data['gbid'] != $gbIdSession) {
            return false;
        }

        if (!$row->save($data)) {
            throw new Exception(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 404);
        }

        return $data;
    }

    /**
     * Loads entry data from the database using JTable
     *
     * @param $id
     *
     * @return JTable
     * @throws Exception
     */
    public function getRow($id)
    {
        $id = (int) $id;
        $table = $this->getTable('entry', 'EasybookReloadedTable');
        $table->load($id);

        return $table;
    }
}
