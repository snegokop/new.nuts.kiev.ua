<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_call
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Call Model
 *
 * @package     Joomla.Administrator
 * @subpackage  com_call
 * @since       1.6
 */
require_once dirname(__FILE__).'/parentmodelclass.php';

class CallModelCall extends JModelList
{
	protected $total;
        protected $comp_name;
        protected $items;
        protected $db;
	public function getTotal()
	{  
		if (!isset($this->total))
		{
			$this->getItems();
		}
		return $this->total;
	}
	
	public function getItems()
	{
		if (!isset($this->items))
		{
                    if(!$this->comp_name) $this->getCompName();
                    if(!$this->db) $this->_getDBo();

                    $query = "SELECT `params` FROM `#__extensions` WHERE `element`='".$this->comp_name."'";
                    $this->db->setQuery($query);
                    $result = $this->db->loadAssoc();
                    $results = (object)unserialize($result["params"]);
                    $this->total = true; 
                    $this->items = $results;
                    $this->items->com_name = $this->comp_name;
		}
		return $this->items;
	}
        
        private function getCompName(){
            $app = JFactory::getApplication();
            $this->comp_name = $app->scope;
            return $this->comp_name;
        }
        
        private function _getDBo(){
           $this->db = JFactory::getDbo();
        }
        
        public function edit(){
            if(!$this->comp_name) $this->getCompName();
            $data = JRequest::get('post');
            $query = "UPDATE `#__extensions` SET `params`=";
            $tmp = array(); 
            
            foreach($data as $key=>$value){
                if($value=="on") $value = "1";
                if(strstr($key, '_call')) $tmp[$key] = $value;
            }
            $tmp["phonereq_call"] = "1";
            $query .= "'".serialize($tmp)."'";
            $query .= " WHERE `element`='".$this->comp_name."'";
            if(!$this->db) $this->_getDBo();
            $this->db->setQuery($query);
            $this->db->execute();
        }
}
