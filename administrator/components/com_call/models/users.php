<?php

defined('_JEXEC') or die;

require_once dirname(__FILE__).'/parentmodelclass.php';

class CallModelUsers extends JModelList{
    private $db = null;
    private $table = null;
    
    public function __construct($config = array()) {
        parent::__construct($config);
        $this->db = $this->_db;
    }
    public function getUsers(){ 
        $this->db->setQuery("SELECT * FROM `#__call_users`");
        return $this->db->loadAssocList();
    }
    public function deleteUser($id){
        if(!$id || $id == 0) return false;
        $res = $this->db->setQuery("DELETE FROM `#__call_users` WHERE `id`=".(int)$id."");
        $res = $this->db->execute();
    }
        
    public function someDelete($ids){
        if(is_array($ids) && !empty($ids)){
            $_in = array();
            foreach($ids as $id){
                $tmp = (int)$id;
                if(!$tmp || $tmp==0) continue;
                $_in[] = $tmp;
            }
            $res = $this->db->setQuery("DELETE FROM `#__call_users` WHERE `id` IN(".implode(",", $_in).")");
            $res = $this->db->execute();
        }
    }
}
