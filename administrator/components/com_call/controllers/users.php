<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $_version2_5;
if($_version2_5){
    jimport('joomla.application.component.controlleradmin');
}

class CallControllerUsers extends JControllerAdmin{
    
    public $users;
    
    private $layout=null;
    
    public function __construct($config = array(), $layout) {
        parent::__construct($config);
        if(!is_null($layout)) $this->$layout();
    }
    
    public function display($cachable = false, $urlparams = array())
    {
        $this->getUsers();
        parent::display($cachable, $urlparams);
        return $this;
    }
    
    private function getUsers() {
        $this->users = $this->getModel("Users")->getUsers();
        $view = $this->createView("Users","CallView","html");
        $view->assign("users", $this->users);
        $view->display();
    }
    
    public function delete() {
        $model = $this->getModel("Users");
        $user_id = JRequest::getVar("user_id");
        if(is_null($user_id) || $user_id=="") return false;
        if(is_array($user_id)) $model->someDelete($user_id);
        else $model->deleteUser($user_id);
    }
}
