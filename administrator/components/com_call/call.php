<?php

defined('_JEXEC') or die;

global $_version3, $_version2_5;

$_version3 = strstr($GLOBALS["jversion"]->RELEASE, "3.") ? true:false;
$_version2_5  = strstr($GLOBALS["jversion"]->RELEASE, "2.5") ? true:false;

if (!JFactory::getUser()->authorise('core.admin'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

$controller	= JControllerLegacy::getInstance('Call');
if($_version2_5){
    $controller->execute(JRequest::getVar('task'));
}
elseif($_version3) $controller->execute(JFactory::getApplication()->input->get('task'));
else die("error version");
$controller->redirect();

?>