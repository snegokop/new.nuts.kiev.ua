<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_all
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Call Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_call
 * @since       1.6
 */
class CallController extends JControllerLegacy
{
	public function display($cachable = false, $urlparams = array())
	{
		parent::display($cachable, $urlparams);
		return $this;
	}
        public function edit(){
            global $_version3;
            JSession::checkToken() or jexit(JText::_('JInvalid_Token'));
            if($_version3) $dispatcher = JEventDispatcher::getInstance();
            $model = $this->getModel('Call');
            if($_version3) $dispatcher->trigger('onBeforeEditCallData', array(&$model));
            $model->edit(); 
            if($_version3) $dispatcher->trigger('onAfterEditCallData', array());
            $this->setRedirect('index.php?option=com_call');
        }
        public function users(){
            include_once dirname(__FILE__)."/controllers/users.php";
            $users = new CallControllerUsers(array(), JFactory::getApplication()->input->get('layout'));
            $users->display();
}
        public function apply(){
            $this->edit();
        }
}
