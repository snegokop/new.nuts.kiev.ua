<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_call
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JHtml::_('bootstrap.tooltip');
//var_dump($this->users);
?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var $ = jQuery;
        $("#checked_all").click(function(){
            var _checked = false;
            if(this.checked===true){
                _checked = true;
                $("#contr_row").css("visibility","visible");
            }
            else {_checked = false;$("#contr_row").css("visibility","hidden")}
            $("#call_users_table").find("input").each(function(){
                if($(this).attr("type")==="checkbox") this.checked=_checked;
            });
        });
        $("#call_users_table").find("input").each(function(){
                if($(this).attr("type")==="checkbox"){
                    $(this).click(function(){
                        if(this.checked===true)$("#contr_row").css("visibility","visible");
                        else {
                            var checkeds = false;
                            $("#call_users_table").find("input").each(function(){
                                if(this.checked===true && !checkeds){
                                    checkeds = true;
                                }
                            });
                            if(!checkeds) $("#contr_row").css("visibility","hidden");
                        }
                    });
                }
        });
    });
    function someDelete(link){
        var $ = jQuery;
        var q_string = '';
        //&user_id[]
        $("#call_users_table").find("input").each(function(){
            if($(this).attr("type")==="checkbox" && this.checked===true && this.value!="on"){
                q_string += "&user_id[]="+this.value;
            }
        });
        document.location = link+q_string;
    }
    
</script>
<table id="call_users_table" cellpadding="5" width="100%" style="margin-left:20px;margin-top:-10px;">
    <tr id="contr_row" style="visibility:hidden;"><td colspan="7"><a href="javascript:void(0)" onclick="someDelete('/administrator/?option=com_call&task=users&layout=delete')"><b><?php echo JText::_("COM_CALL_ALL_DELETE");?></b></a></td></tr>
    <tr style="background:#ddf;">
        <td><input type="checkbox" id="checked_all"></td>
        <td><b>ID</b></td><td><b><?php echo JText::_("COM_CALL_HEAD_PHONE");?></b></td><td><b>E-mail</b></td><td><b><?php echo JText::_("COM_CALL_HEAD_NAME");?></b></td><td><b><?php echo JText::_("COM_CALL_HEAD_TIME");?></b></td><td><b><?php echo JText::_("COM_CALL_HEAD_TIMESTAMP");?></b></td><td></td>
    </tr>
<?php
foreach($this->users as $user){?>
    <tr>
        <td><input type="checkbox" name="user_id[]" value="<?php echo $user['id'];?>" /></td>
        <td><?php echo $user['id'];?></td>
        <td><?php echo $user['phone'];?></td>
        <td><?php echo $user['email'];?></td>
        <td><?php echo $user['name'];?></td>
        <td><?php echo $user['time'];?></td>
        <td><?php if($user['timestamp'] && $user['timestamp']!="0")echo date("Y-m-d h:i:s", (int)$user['timestamp']); else echo "";?></td>
        <td><a href="/administrator/?option=com_call&task=users&layout=delete&user_id=<?php echo $user['id'];?>"><?php echo JText::_("COM_CALL_ROW_DELETE");?></a></td>
    </tr>
<?php }?>
</table>


