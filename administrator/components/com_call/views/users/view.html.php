<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_call
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Call component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_call
 * @since       1.0
 */
class CallViewUsers extends JViewLegacy{
	public function display($tpl = null)
	{
	    $this->addToolbar();
            parent::display($tpl); 
	}   
        protected function addToolbar(){
            JToolbarHelper::title(JText::_('COM_CALL_USERS_TITLE'), null);
        }
}

