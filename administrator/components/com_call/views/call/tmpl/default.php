<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_call
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
global $_version2_5, $_version3;

if($_version3) JHtml::_('bootstrap.tooltip');
?>
<style type="text/css">
    #global-call tr td.center{
        width:60%;
    }
    #global-call tr.sms td{background:#EEF;border-top:none;}
    .headpanel td {
        border-bottom:1px solid green; 
        cursor:pointer;
        border-left:1px solid green;
        border-right:1px solid green;
        border-top:none;
        background:#FFF !important;
    }
    .headpanel td:hover {
        background:#EEF !important;
    }
    .panel {display:none;}
    #controll_option {display:block;}
    #controll_call{border-bottom:none;border-top:1px solid green;}
    
</style>
<script type="text/javascript">
    window.onload = function(){
        var check_is_imail;
        var email_req_row;
        var istime_call;
        var timereq_row;
        var ifredir_call;
        var redirect_call;
        var isname_call;
        var namereq_call;
        var save_call;
        var ifsms_call;
        var iscaptcha_call;
        var captchareq_call;
        var _form = document.getElementById("adminForm");
        var inputs = _form.getElementsByTagName("input");
        for(var i=0; i<inputs.length; i++){
            if(inputs[i].name==="isemail_call") check_is_imail = inputs[i];
            if(inputs[i].name==="iscaptcha_call") iscaptcha_call = inputs[i];
            if(inputs[i].name==="captchareq_call") captchareq_call = inputs[i];
            if(inputs[i].name==="emailreq_call") email_req_row = inputs[i].parentNode.parentNode;
            if(inputs[i].name==="istime_call") istime_call = inputs[i];
            if(inputs[i].name==="timereq_call") timereq_row = inputs[i].parentNode.parentNode;
            if(inputs[i].name==="ifredir_call") ifredir_call = inputs[i];
            if(inputs[i].name==="redirect_call")redirect_call = inputs[i].parentNode.parentNode;
            if(inputs[i].name==="isname_call") isname_call = inputs[i];
            if(inputs[i].name==="namereq_call") namereq_call = inputs[i].parentNode.parentNode;
            if(inputs[i].name==="save") save_call = inputs[i];
            if(inputs[i].name==="ifsms_call"){
                ifsms_call = inputs[i];
                if(ifsms_call.checked===true) show_sms_option("table-row");
                else show_sms_option("none");
            }
        }
        
        iscaptcha_call.onclick = function(){
            if(this.checked===false) captchareq_call.checked = false;
            else captchareq_call.checked = true;
        }
        
        ifsms_call.onclick = function(){
            if(this.checked===false){
                var display = "none";
            }
            else var display = "table-row";
            show_sms_option(display);
        }
        
        isname_call.onclick = function(){
            if(this.checked===false){
                namereq_call.getElementsByTagName('input')[0].checked = false;
                namereq_call.style.display = "none";
            }
            else namereq_call.style.display = "table-row";            
        }
        
        ifredir_call.onclick = function(){
            if(this.checked===false){
                redirect_call.style.display = "none";
            }
            else redirect_call.style.display = 'table-row';            
        }
        
        check_is_imail.onclick = function(){
            if(this.checked===false){
                email_req_row.getElementsByTagName('input')[0].checked = false;
            }
            else email_req_row.style.display = "table-row";
        }
        
        istime_call.onclick = function(){
            if(this.checked===false){
                timereq_row.getElementsByTagName('input')[0].checked = false;
            }
            else timereq_row.style.display = "table-row";
        }
        
        var head_panel = document.getElementsByClassName("headpanel");
        var panels = document.getElementsByClassName("panel");
        for(var i = 0; i<head_panel.length; i++){
            var _tds = head_panel[i].getElementsByTagName("td");
            for(var j = 0; j<_tds.length; j++){
                _tds[j].onclick = function(){
                    setStyleToColection(_tds, 'borderBottom', "1px solid green");
                    setStyleToColection(_tds, 'borderTop', "none");
                    this.style.borderBottom = "none";
                    this.style.borderTop = "1px solid green";
                    setStyleToColection(panels, "display", "none");
                    var _id = this.id.split("_")[0]+"_option";
                    document.getElementById(_id).style.display = "block";
                }
            }
        }
    }
    function setStyleToColection(tags, prop, value){
          for(var i = 0; i<tags.length; i++){
              tags[i].style[prop] = value;
          }
    }
    function show_sms_option(display){
        var sms = document.getElementById("global-call").getElementsByClassName("sms");
        for(var i=0; i<sms.length; i++){
            sms[i].style.display = display;
        }
    }
</script>
<form action="<?php echo JRoute::_('index.php?option='.$this->items->com_name);?>" method="post" name="adminForm" id="adminForm">
  
    <div id="j-main-container" class="span10">

  	<div class="clearfix"> </div>
        <div>
            <table class="table table-striped">
                <tbody>
                    <tr class="row headpanel">
                        <td id="controll_call" class="center"><?php echo JText::_('COM_CALL_CONTROL'); ?></td>
                        <td id="design_call"class="center"><?php echo JText::_('COM_CALL_DESIGN'); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="panel" id="controll_option">
  	<table id="global-call" class="table table-striped">
  		<tbody>
  		
  			<tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_EMAILS'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_EMAILS_DESCR'); ?></span></td>
  				<td><input type="text" name="emails_call" value="<?php if(isset($this->items->emails_call)) echo $this->items->emails_call; ?>"></td>
  			</tr>
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_ISSEND_EMAIL'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_ISSEND_EMAIL_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="ifSendemail_call" <?php if(isset($this->items->ifSendemail_call) && $this->items->ifSendemail_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_ISSEND_SMS'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_ISSEND_SMS_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="ifsms_call" <?php if(isset($this->items->ifsms_call) && $this->items->ifsms_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        
                        <tr class="row sms">
                            <td class="center"><b><?php echo JText::_('COM_CALL_PHONES'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_PHONES_DESCR'); ?></span></td>
  			    <td><input type="text" name="smsDestination_call" value="<?php if(isset($this->items->smsDestination_call)) echo $this->items->smsDestination_call; ?>"></td>
  			</tr>
                        <tr class="row sms">
                            <td class="center"><b><?php echo JText::_('COM_CALL_SMS_SENDER'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_SMS_SENDER_DESCR'); ?></span></td>
  			    <td><input type="text" name="smsSender_call" value="<?php if(isset($this->items->smsSender_call)) echo $this->items->smsSender_call; ?>"></td>
  			</tr>
                        <tr class="row sms">
                            <td class="center"><b><?php echo JText::_('COM_CALL_SMS_AUTH'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_SMS_AUTH_DESCR'); ?></span></td>
  			    <td>
                                <div style="width: 70px;float:left;">Login:</div><input type="text" style="width:142px;" name="smsLogin_call" value="<?php if(isset($this->items->smsLogin_call)) echo $this->items->smsLogin_call; ?>" ><br>
                                <div style="width: 70px;float:left;">Password:</div><input type="text" style="width:142px;" name="smsPassw_call" value="<?php if(isset($this->items->smsPassw_call)) echo $this->items->smsPassw_call; ?>">               
                            </td>
  			</tr>
                        <tr class="row sms">
                            <td class="center"><b><?php echo JText::_('COM_CALL_SMS_LINK'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_SMS_LINK_DESCR'); ?></span></td>
  			    <td><input type="text" name="smsLink_call" value="<?php if(isset($this->items->smsLink_call)) echo $this->items->smsLink_call; ?>"></td>
  			</tr>
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_IS_EMAIL'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_IS_EMAIL_DESCR'); ?></span></td>
                                <td><input type="checkbox" name="isemail_call" <?php if(isset($this->items->isemail_call) && $this->items->isemail_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <?php if(isset($this->items->isemail_call) && $this->items->isemail_call=="1"){ $display="table-row"; }else $display="none";?>
                        <tr style="display:<?php echo $display;?>" class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_REQ_EMAIL'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_REQ_EMAIL_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="emailreq_call" <?php if(isset($this->items->emailreq_call) && $this->items->emailreq_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_IS_TIME'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_IS_TIME_DESCR'); ?></span></td>
                                <td><input type="checkbox" name="istime_call" <?php if(isset($this->items->istime_call) && $this->items->istime_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <?php 
                        if(isset($this->items->istime_call) && $this->items->istime_call=="1"){ $display="table-row";}else $display="none";?>
                        <tr style="display:<?php echo $display;?>" class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_REQ_TIME'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_REQ_TIME_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="timereq_call" <?php if(isset($this->items->timereq_call) && $this->items->timereq_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_IS_REDIR'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_IS_REDIR_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="ifredir_call" <?php if(isset($this->items->ifredir_call) && $this->items->ifredir_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <?php if(isset($this->items->ifredir_call) && $this->items->ifredir_call=="1"){ $display="table-row";}else $display="none";?>
                        <tr style="display:<?php echo $display;?>" class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_REDIRECT'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_REDIRECT_DESCR'); ?></span></td>
  				<td><input type="text" name="redirect_call" value="<?php if(isset($this->items->redirect_call)) echo $this->items->redirect_call; ?>"></td>
  			</tr>  
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_IS_NAME'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_IS_NAME_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="isname_call" <?php if(isset($this->items->isname_call) && $this->items->isname_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <?php if(isset($this->items->isname_call) && $this->items->isname_call=="1"){ $display="table-row";}else $display="none";?>
                        <tr style="display:<?php echo $display;?>" class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_REQNAME'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_REQNAME_DESCR'); ?></span></td>
  				<td><input type="checkbox" name="namereq_call" <?php if(isset($this->items->namereq_call) && $this->items->namereq_call=="1"){?>checked <?php }?>></td>
  			</tr>
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_REQCAPTCHA'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_REQCAPTCHA_DESCR'); ?></span></td>
  				<td><input type="checkbox" name="iscaptcha_call" <?php if(isset($this->items->iscaptcha_call) && $this->items->iscaptcha_call=="1"){?>checked <?php }?>></td>
                        </tr>
                        <tr style="display:none;">
                            <td>
                                <input type="checkbox" name="captchareq_call" <?php if(isset($this->items->captchareq_call) && $this->items->captchareq_call=="1"){?>checked <?php }?>>
                            </td>
                            <td></td>
                        </tr> 
                        <tr class="row">
                            <td class="center"><b><?php echo JText::_('COM_CALL_SAVE_USER'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_SAVE_USER_DESCR'); ?></span></td>
                            <td><input type="checkbox" name="saveuser_call" <?php if(isset($this->items->saveuser_call) && $this->items->saveuser_call=="1"){?>checked <?php }?>></td>
  			</tr>
                </tbody>
  	</table>
        </div>
        <div class="panel" id="design_option">
        <table id="design-call" class="table table-striped">
            <tbody> 
                    <tr class="row">
                        <td class="center">
                            <b><?php echo JText::_('COM_CALL_EMAIL_MESSAGE'); ?></b><br>
                            <span style="font-size:11px;"><?php echo JText::_('COM_CALL_EMAIL_MESSAGE_DESCR'); ?></span>
                        </td>
                        <td>
                            <textarea rows="4" style="width:90%;" name="message_call"><?php if(isset($this->items->message_call)) echo trim($this->items->message_call); ?>
                            </textarea>
                        </td>
  		    </tr>
                    <tr class="row">
                        <td class="center">
                            <b><?php echo JText::_('COM_CALL_SMS_MESSAGE'); ?></b><br>
                            <span style="font-size:11px;"><?php echo JText::_('COM_CALL_EMAIL_MESSAGE_DESCR'); ?></span>
                        </td>
                        <td>
                            <textarea rows="4" style="width:90%;" name="sms_message_call"><?php if(isset($this->items->sms_message_call)) echo trim($this->items->sms_message_call); ?>
                            </textarea>
                        </td>
  		    </tr>
                    <tr class="row">
                        <td class="center">
                            <b><?php echo JText::_('COM_CALL_TEMPLATE'); ?></b><br>
                            <span style="font-size:11px;"><?php echo JText::_('COM_CALL_TEMPLATE_DESCR'); ?></span>
                        </td>
                        <td>
                            <select style="width:90%;" name="template_call" value="<?php if(isset($this->items->template_call)) { echo $this->items->template_call;} ?>">
                                <?php     
                                foreach(scandir(JPATH_COMPONENT_SITE."/views/call/tmpl/") as $file){
                                    if(!strstr($file,".php")) continue;
                                    $val = str_replace(".php", "", $file);
                                ?>
                                <option value="<?php echo $val;?>"<?php if($val==$this->items->template_call){?> selected<?php }?>><?php echo $val;?></option>
                                <?php }?>
                            </select>
                        </td>
  		    </tr> 
                    <tr class="row">
                    <td class="center"><b><?php echo JText::_('COM_CALL_TIMEOUT'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_TIMEOUT_DESCR'); ?></span></td>
                            <td><input type="text" name="timeout_call" value="<?php if(isset($this->items->timeout_call)) echo $this->items->timeout_call; ?>"></td>
                    </tr>
                    <tr class="row">
                        <td class="center"><b><?php echo JText::_('COM_CALL_COUNT'); ?></b><br><span style="font-size:11px;"><?php echo JText::_('COM_CALL_COUNT_DESCR'); ?></span></td>
                            <td><input type="text" name="countRepeat_call" value="<?php if(isset($this->items->countRepeat_call)) echo $this->items->countRepeat_call; ?>"></td>
                    </tr>
            </tbody>
  	</table>
        </div>
        <div>
        <table class="table table-striped">
            <tbody>
                <tr class="row">
  		    <td class="center" colspan="2">
                        <input type="submit" name="save" value="<?php echo JText::_('COM_CALL_SAVE_PARAM'); ?>">
                    </td>
  		</tr>
            </tbody>
  	</table>                   
  	<input type="hidden" name="task" value="edit" />
  	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<div class="clearfix"> </div>
<div style="text-align:center;">
    <div style="padding:5px;font-weight:bold;"><?php echo JText::_('COM_CALL_INSTRUCTION_CODE'); ?></div>
    <textarea rows="3" style="width:85%;resize:none;overflow:hidden;">
<div id="call_button"> </div>
<script type="text/javascript">jQuery(document).ready(function(){var c=jQuery;c.ajax({type:"GET",url:"/?option=com_call",success:function(a){document.getElementById("call_button").innerHTML=a;a=c("#call_button").find("script");for(var b=0;b<a.length;b++)eval.call(window,a[b].innerHTML)}})});</script>
</textarea>
</div>
