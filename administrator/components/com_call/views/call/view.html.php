<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_call
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Call component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_call
 * @since       1.0
 */
class CallViewCall extends JViewLegacy
{
	
	public function display($tpl = null)
	{
                global $_version3;
                $this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		if($_version3) $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl); 
	}
}
