<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedControllerBadwords extends JControllerLegacy
{
    protected $input;

    public function __construct()
    {
        parent::__construct();
        $this->registerTask('add', 'edit');
        $this->registerTask('apply', 'save');
        $this->input = JFactory::getApplication()->input;
    }

    public function display($cachable = false, $urlparams = false)
    {
        $this->input->set('view', 'badwords');

        require_once JPATH_COMPONENT . '/helpers/easybookreloaded.php';
        EasybookReloadedHelper::addSubmenu($this->input->getCmd('view', 'easybookreloaded'));

        parent::display();
    }

    public function edit()
    {
        $this->input->set('view', 'badword');
        $this->input->set('layout', 'form');
        $this->input->set('hidemainmenu', 1);

        parent::display();
    }

    public function save()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $message = JText::_('COM_EASYBOOKRELOADED_BADWORDSAVEDFAIL');
        $type = 'error';

        $model = $this->getModel('badword');

        if ($model->store()) {
            $message = JText::_('COM_EASYBOOKRELOADED_BADWORDSAVEDSUCCESS');
            $type = 'message';
        }

        if ($this->task == 'apply' && $this->input->getInt('id', 0) !== 0) {
            $this->setRedirect('index.php?' . $this->input->getString('url_current'), $message, $type);

            return;
        }

        $this->setRedirect('index.php?option=com_easybookreloaded&controller=badwords', $message, $type);
    }

    public function remove()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $message = JText::_('COM_EASYBOOKRELOADED_BADWORDDELETEFAIL');
        $type = 'error';

        $model = $this->getModel('badword');

        if ($model->delete()) {
            $message = JText::_('COM_EASYBOOKRELOADED_BADWORDDELETESUCCESS');
            $type = 'message';
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded&controller=badwords', false), $message, $type);
    }

    public function cancel()
    {
        $message = JText::_('COM_EASYBOOKRELOADED_OPERATION_CANCELLED');
        $this->setRedirect('index.php?option=com_easybookreloaded&controller=badwords', $message, 'notice');
    }
}
