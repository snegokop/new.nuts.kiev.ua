<?php
/**
 * @package    EBR - Easybook Reloaded for Joomla! 3.x
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3.3.2 - 2018-05-09
 * @link       https://joomla-extensions.kubik-rubik.de/ebr-easybook-reloaded
 *
 * @license    GNU/GPL
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') || die('Restricted access');

class EasybookReloadedControllerEntry extends JControllerLegacy
{
    protected $input;

    public function __construct()
    {
        parent::__construct();
        $this->registerTask('add', 'edit');
        $this->registerTask('apply', 'save');
        $this->input = JFactory::getApplication()->input;

        if ($this->input->getCmd('task', '') == '') {
            $view = $this->getView('easybookreloaded', 'html');

            if ($modelGb = $this->getModel('gb')) {
                $view->setModel($modelGb, false);
            }

            require_once JPATH_COMPONENT . '/helpers/easybookreloaded.php';
            EasybookReloadedHelper::addSubmenu($this->input->getCmd('view', 'easybookreloaded'));
        }
    }

    public function edit()
    {
        $this->input->set('view', 'entry');
        $this->input->set('layout', 'form');
        $this->input->set('hidemainmenu', 1);

        $view = $this->getView('entry', 'html');
        $view->setLayout('form');

        if ($modelGb = $this->getModel('gb')) {
            $view->setModel($modelGb, false);
        }

        parent::display();
    }

    public function save()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_SAVING_ENTRY');
        $type = 'error';

        $model = $this->getModel('entry');

        if ($model->store()) {
            $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_SAVED');
            $type = 'message';
        }

        if ($this->task == 'apply') {
            $this->setRedirect('index.php?' . $this->input->getString('url_current'), $message, $type);

            return;
        }

        $this->setRedirect('index.php?option=com_easybookreloaded', $message, $type);
    }

    public function remove()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $message = JText::_('COM_EASYBOOKRELOADED_ERROR_ENTRY_COULD_NOT_BE_DELETED');
        $type = 'error';

        $model = $this->getModel('entry');

        if ($model->delete()) {
            $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_DELETED');
            $type = 'message';
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded', false), $message, $type);
    }

    public function cancel()
    {
        $message = JText::_('COM_EASYBOOKRELOADED_OPERATION_CANCELLED');
        $this->setRedirect('index.php?option=com_easybookreloaded', $message, 'notice');
    }

    public function publish()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_PUBLISHED');
        $type = 'message';

        $model = $this->getModel('entry');

        if (!$model->publish(1)) {
            $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_CHANGE_PUBLISH_STATUS') . " - " . $model->getError();
            $type = 'error';
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded', false), $message, $type);
    }

    public function unpublish()
    {
        JSession::checkToken() || jexit('Invalid Token');

        $message = JText::_('COM_EASYBOOKRELOADED_ENTRY_UNPUBLISHED');
        $type = 'message';

        $model = $this->getModel('entry');

        if (!$model->publish(0)) {
            $message = JText::_('COM_EASYBOOKRELOADED_ERROR_COULD_NOT_CHANGE_PUBLISH_STATUS') . " - " . $model->getError();
            $type = 'error';
        }

        $this->setRedirect(JRoute::_('index.php?option=com_easybookreloaded', false), $message, $type);
    }
}
