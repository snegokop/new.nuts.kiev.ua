<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

class JshoppingViewAddon_menu_builder_list extends JViewLegacy
{
    function display($tpl = null){
        JToolBarHelper::title( _JSHOP_MENU_BUILDER, 'menumgr.png' ); 
        JToolBarHelper::addNew();
		JToolBarHelper::divider();
		JToolBarHelper::publishList();
        JToolBarHelper::unpublishList();
        JToolBarHelper::divider();
		JToolBarHelper::trash('trash');
		JToolBarHelper::deleteList();
		JToolBarHelper::divider();
		JToolBarHelper::makeDefault('setDefault', 'COM_MENUS_TOOLBAR_SET_HOME');
        parent::display($tpl);
	}
}
?>