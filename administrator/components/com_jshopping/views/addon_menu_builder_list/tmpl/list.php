<?php
defined('_JEXEC') or die;
$app = JFactory::getApplication();
$show_associations = $app->get('menu_associations', 0);
JHtml::_('behavior.tooltip');
?>
<form action="<?php echo JRoute::_('index.php?option=com_jshopping&controller=addon_menu_builder');?>" method="post" name="adminForm" id="adminForm">
	<table width="100%" style="padding-bottom:5px;">
		<tr>
			<td align="right">
				<?php echo JText::_('COM_MENUS_ITEM_FIELD_ASSIGNED_LABEL').": ".$this->filter['menutype'];?>&nbsp;&nbsp;&nbsp;
				<?php echo JText::_('JSTATUS').": ".$this->filter['published'];?>&nbsp;&nbsp;&nbsp;
				<?php echo _JSHOP_LANGUAGE_NAME.": ".$this->filter['language'];?>
			</td>
		</tr>
	</table>
	
	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					#
				</th>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th>
					<?php echo JText::_('JGLOBAL_TITLE'); ?>
				</th>
				<th width="5%">
					<?php echo JText::_('JSTATUS'); ?>
				</th>
				<th width="10%">
					<?php echo JText::_('COM_MENUS_ITEM_FIELD_ASSIGNED_LABEL'); ?>
				</th>
				<th width="10%">
					<?php echo JText::_('COM_MENUS_ITEM_FIELD_HOME_LABEL'); ?>
				</th>
				<?php if ($show_associations) { ?>
				<th width="10%">
					<?php echo JText::_('COM_MENUS_HEADING_ASSOCIATION'); ?>
				</th>
				<?php } ?>
				<th width="5%">
					<?php echo _JSHOP_LANGUAGE_NAME; ?>
				</th>
				<th width="1%" class="nowrap">
					<?php echo JText::_('JGRID_HEADING_ID'); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="<?php echo 8 + $show_associations;?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php
			foreach ($this->rows as $i=>$row) { ?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo $i + $this->pagination->limitstart + 1; ?>
				</td>
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $row->id); ?>
				</td>
				<td>
					<?php echo str_repeat('<span class="gi">|&mdash;</span>', $row->level); ?>
					<a href="<?php echo JRoute::_('index.php?option=com_jshopping&controller=addon_menu_builder&task=edit&id='.$row->id) ?>" title="<?php echo $row->description;?>"><?php echo $row->title; ?></a>
					<p class="smallsub">
						<?php echo str_repeat('<span class="gtr">|&mdash;</span>', $row->level); ?>
						(<span><?php echo JText::_('COM_MENUS_TYPE_ALIAS');?></span>: <?php echo $row->alias;?>)
					</p>
				</td>
				<td class="center">
					<?php echo ($row->published == 1) ? ('<a href="javascript:void(0)" onclick="return listItemTask(\'cb'.$i. '\', \'unpublish\')"><img title="' . _JSHOP_PUBLISH . '" alt="" src="components/com_jshopping/images/tick.png"></a>') : ('<a href="javascript:void(0)" onclick="return listItemTask(\'cb' . $i . '\', \'publish\')"><img title="'._JSHOP_UNPUBLISH.'" alt="" src="components/com_jshopping/images/publish_x.png"></a>');?>
				</td>
				<td class="center">
					<a href="<?php echo JRoute::_('index.php?option=com_jshopping&controller=addon_menu_builder&task=edit&id='.$row->id) ?>" title="<?php echo $row->description;?>"><?php echo $row->menutype; ?></a>
				</td>
				<td class="center">
				<?php if ($row->language=='*' || $row->home=='0') { ?>
					<?php echo JHtml::_('jgrid.isdefault', $row->home, $i, '', ($row->language != '*' || !$row->home));?>
				<?php } else { ?>
					<a href="<?php echo JRoute::_('index.php?option=com_jshopping&controller=addon_menu_builder&task=unsetDefault&cid[]='.$row->id.'&'.JSession::getFormToken().'=1');?>">
						<?php echo JHtml::_('image', 'mod_languages/'.substr($row->language, 0, 2).'.gif', $row->language, array('title'=>JText::sprintf('COM_MENUS_GRID_UNSET_LANGUAGE', $row->language)), true);?>
					</a>
				<?php } ?>
				</td>
				<?php if ($show_associations) { ?>
				<td class="center">
					<?php if ($row->associations) {
						$text = array();
						foreach ($row->associations as $tag=>$associated) {
							if ($associated != $row->id) {
								$text[] = JText::sprintf('COM_MENUS_TIP_ASSOCIATED_LANGUAGE', JHtml::_('image', 'mod_languages/'.$row->association_items[$associated]->image.'.gif', $row->association_items[$associated]->language, array('title'=>$row->association_items[$associated]->language), true), $row->association_items[$associated]->title, $row->association_items[$associated]->menu_title);
							}
						}
						echo JHtml::_('tooltip', implode('<br />', $text), JText::_('COM_MENUS_TIP_ASSOCIATION'), 'menu/icon-16-links.png');
					} ?>
				</th>
				<?php } ?>
				<td class="center">
					<?php echo ($row->language == '*') ? JText::_('JALL') : $row->language_title; ?>
				</td>
				<td class="center">
					<?php echo $row->id; ?>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	
	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
