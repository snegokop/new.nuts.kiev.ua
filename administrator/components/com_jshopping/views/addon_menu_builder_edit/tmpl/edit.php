<?php
defined('_JEXEC') or die;
jimport('joomla.html.pane');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal');

$standart = $this->item->standart;
$jshopping = $this->item->jshopping;
$hidden = $this->item->hidden;
$linktype = $this->item->linktype;
$pagedisplay = $this->item->pagedisplay;
$metadata = $this->item->metadata;

$uri =& JURI::getInstance();
$liveurlhost = $uri->toString( array("scheme",'host', 'port'));
$url = htmlspecialchars_decode($liveurlhost.SEFLink("index.php?option=com_jshopping&controller=addon_menu_builder&task=getAjaxParams&ajax=1"), ENT_NOQUOTES);
?>
<script type="text/javascript">
	var jshop_prevAjaxQuery = null;
	
	function updateParamsFields(value) {
		jQuery('#jshop_menu_params').html('');
		var data = {};
		data['jshop_id'] = value;
		if (jshop_prevAjaxQuery){
			jshop_prevAjaxQuery.abort();
		}
		jshop_prevAjaxQuery = jQuery.ajax({
			url: '<?php echo $url;?>',
			dataType: 'json',
			data: data,
			type: 'post',    
			success: function (json) {
				jQuery('#jshop_menu_params').html(json.html);
			}
		});
	}
	
	Joomla.submitbutton = function(task, type) {
		if (task == 'cancel' || document.formvalidator.isValid(document.id('item-form'))) {
			Joomla.submitform(task, document.id('item-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jshopping&controller=addon_menu_builder'); ?>" method="post" name="adminForm" id="item-form" class="form-validate" autocomplete="off">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_MENUS_ITEM_DETAILS');?></legend>
			<ul class="adminformlist">
				<?php foreach ($standart as $k=>$v) { ?>
				<li><?php echo $v->name.$v->value; ?></li>
				<?php } ?>
			</ul>
		</fieldset>
	</div>

	<div class="width-40 fltrt">
		<?php
		//$pane = JPane::getInstance('Sliders');
		//echo $pane->startPane('settings');
		
		echo JText::_('COM_MENUS_REQUEST_FIELDSET_LABEL');
		?>
		<fieldset class="panelform">
			<ul class="adminformlist">
				<li><?php echo $jshopping->jshop_menutype->name.$jshopping->jshop_menutype->value; ?></li>
			</ul>
			<div id="jshop_menu_params">
				<ul class="adminformlist">
				<?php foreach ($jshopping as $k=>$v) {
					if ($k != 'jshop_menutype') { ?>
					<li><?php echo $v->name.$v->value;?></li>
				<?php } 
				} ?>
				</ul>
			</div>
		</fieldset>
		<?php
		//echo $pane->endPanel();
		
		echo JText::_('COM_MENUS_LINKTYPE_OPTIONS_LABEL');
		?>
		<fieldset class="panelform">
			<ul class="adminformlist">
			<?php foreach ($linktype as $k=>$v) { ?>
				<li><?php echo $v->name.$v->value;?></li>
			<?php } ?>
			</ul>
		</fieldset>
		<?php
		//echo $pane->endPanel();
		
		echo JText::_('COM_MENUS_PAGE_OPTIONS_LABEL');
		?>
		<fieldset class="panelform">
			<ul class="adminformlist">
			<?php foreach ($pagedisplay as $k=>$v) { ?>
				<li><?php echo $v->name.$v->value;?></li>
			<?php } ?>
			</ul>
		</fieldset>
		<?php
		//echo $pane->endPanel();
		
		echo JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS');
		?>
		<fieldset class="panelform">
			<ul class="adminformlist">
			<?php foreach ($metadata as $k=>$v) { ?>
				<li><?php echo $v->name.$v->value;?></li>
			<?php } ?>
			</ul>
		</fieldset>
		<?php
		//echo $pane->endPanel();
		
		if (JFactory::getApplication()->get('menu_associations', 0)) {
			$association = $this->item->association;
			//echo $pane->startPanel(JText::_('COM_MENUS_ITEM_ASSOCIATIONS_FIELDSET_LABEL'), 'associations');
			?>
			<p class="tip"><?php echo JText::_('COM_MENUS_ITEM_ASSOCIATIONS_FIELDSET_DESC');?></p><div class="clr"></div>
			
			<fieldset class="panelform">
				<ul class="adminformlist">
				<?php foreach ($association as $k=>$v) { ?>
					<li><?php echo $v->name.$v->value;?></li>
				<?php } ?>
				</ul>
			</fieldset>
			<?php
			//echo $pane->endPanel();
		}
		
		//echo $pane->endPane();
		?>
		<div class="clr"></div>
		
		<?php foreach ($hidden as $k=>$v) { ?>
			<input type="hidden" name="<?php echo $k;?>" value="<?php echo $v;?>" />
		<?php } ?>
	</div>
	
	<input type = "hidden" name = "task" value = "<?php echo JRequest::getVar('task')?>" />
	<input type = "hidden" name = "edit" value = "<?php echo $this->edit;?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>
