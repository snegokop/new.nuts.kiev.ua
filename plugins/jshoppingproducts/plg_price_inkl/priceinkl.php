<?php
defined('_JEXEC') or die('Restricted access');

class plgJshoppingProductsPriceInkl extends JPlugin
{

function plgJshoppingProductsPriceInkl(&$subject, $config){
        parent::__construct($subject, $config);
    }

function onBeforeDisplayProduct(&$product, &$view)
    {
        $document =& JFactory::getDocument();
        $jshopConfig = &JSFactory::getConfig();
        JSFactory::loadExtLanguageFile('plg_price_inkl');
        $taxes = &JSFactory::getAllTaxes();        
        $tax = $taxes[$product->product_tax_id];        

        if ($tax == 0){
        $html .= '<div id="PlgPriceInkl" style="font-size:11px;font-weight:100">'._PLG_PRICE_INKL.'</div>';
        }
        $product->_tmp_var_price_ext = $html;
    }
          
function onBeforeDisplayProductList(&$rows)
    {
        $document =& JFactory::getDocument();
        $jshopConfig = &JSFactory::getConfig();
        $product = &JTable::getInstance('product', 'jshop');
        JSFactory::loadExtLanguageFile('plg_price_inkl');
        $taxes = &JSFactory::getAllTaxes();        
        
        foreach ($rows as $key=>$value)
        if ($taxes[$value->tax_id] == 0){
        $rows[$key]->_tmp_var_bottom_price .= '<div id="PlgPriceInkl" style="font-size:11px;font-weight:100">'._PLG_PRICE_INKL.'</div>';
        }
    }
}
?>