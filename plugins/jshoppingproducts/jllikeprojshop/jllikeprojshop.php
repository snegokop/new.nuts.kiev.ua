<?php
/**
 * jllikepro
 *
 * @version 2.4.4
 * @author Zhukov Artem (artem@joomline.ru)
 * @copyright (C) 2012 by Zhukov Artem(http://www.joomline.ru)
 * @license GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 **/

// no direct access
defined('_JEXEC') or die;
error_reporting(E_ERROR);
jimport('joomla.plugin.plugin');
jimport('joomla.html.parameter');
require_once JPATH_ROOT . '/plugins/content/jllikepro/helper.php';

class plgJshoppingProductsJlLikeProJShop extends JPlugin
{


    public function onBeforeDisplayProductView(&$content)
    {
        JPlugin::loadLanguage('plg_content_jllikepro');
        $plugin = & JPluginHelper::getPlugin('content', 'jllikepro');
        $plgParams = new JRegistry;
        $plgParams->loadString($plugin->params);
        $view = JRequest::getCmd('controller');
        $prod_id = JRequest::getCmd('product_id');
        $JShopShow = $plgParams->get('jshopcontent');

        if (!$JShopShow || $view != 'product') {
            return '';
        }
		$parent_contayner = $this->params->get('parent_contayner', '');
        if(!empty($parent_contayner))
        {
            $plgParams->set('parent_contayner', $parent_contayner);
        }
        $helper = PlgJLLikeProHelper::getInstance($plgParams);
        $helper->loadScriptAndStyle(0);

        $url = 'http://' . $plgParams->get('pathbase', '') . str_replace('www.', '', $_SERVER['HTTP_HOST']);
        if ($plgParams->get('punycode_convert', 0)) {
            $file = JPATH_ROOT . '/libraries/idna_convert/idna_convert.class.php';
            if (!JFile::exists($file)) {
                return JText::_('PLG_JLLIKEPRO_PUNYCODDE_CONVERTOR_NOT_INSTALLED');
            }

            include_once $file;

            if ($url) {
                if (class_exists('idna_convert')) {
                    $idn = new idna_convert;
                    $url = $idn->encode($url);
                }
            }
        }
        $uri = JString::str_ireplace(JURI::root(), '', JURI::current());
        $link = $url . '/' . $uri;

        $image = $content->product->product_name_image;

		if(empty($image))
		{
			$image = $content->product->image;
		}
		
        if (!empty($image))
        {
            $jshopConfig = JSFactory::getConfig();
            $image = $jshopConfig->image_product_live_path . '/' . $image;
        }

        $lang = JFactory::getLanguage()->getTag();
        $name = 'name_'.$lang;
        $desc = ($plgParams->get('desc_source_jshop', 'intro') == 'intro') ? 'short_description_'.$lang : 'description_'.$lang;
		
        PlgJLLikeProHelper::addOpenGraphTags($content->product->$name, $content->product->$desc, $image);

        $shares = $helper->ShowIN($prod_id, $link, $content->product->$name, $image);

        switch ($plgParams->get('jshopposition', 2)) {
            case 1 :
                $content->_tmp_product_html_start = $shares;
                break;
            case 3 :
                $content->_tmp_product_html_end = $shares;
                break;
            default:
                $content->_tmp_product_html_after_buttons = $shares;
                break;
        }
    } //end function


}//end class
