<?php
defined('_JEXEC') or die('Restricted access');
class plgJshoppingShowattrprlist extends JPlugin{
    
    function onBeforeQueryGetProductList($name="category", &$adv_result, &$adv_from, &$adv_query, &$order_query, &$filters){
        if($name=="category"){
            $adv_result .= ", GROUP_CONCAT(pr_a2.addprice) AS addprice, GROUP_CONCAT(av.`name_ru-RU`) AS attrvalue, 
            GROUP_CONCAT(av.`value_id`) AS attr_values_id, a.`name_ru-RU` as attrname, a.attr_id as attr_id ";
            $adv_from .= " LEFT JOIN `#__jshopping_products_attr2` AS pr_a2 ON (pr_a2.product_id = prod.product_id) 
            LEFT JOIN `#__jshopping_attr_values` AS av ON (pr_a2.attr_id = av.attr_id AND pr_a2.attr_value_id = av.value_id) 
            LEFT JOIN `#__jshopping_attr` AS a ON (pr_a2.attr_id = a.attr_id)";
            $adv_query .= " GROUP BY prod.product_id ";
        }
    }
    
    function onBeforeDisplayProductListView(&$view){
        //return;
        $view->attributes = array();
        foreach($view->rows as &$row){
            $view->attributes[$row->product_id] = array();
            $addprice = trim($row->addprice);
            if($addprice!=""){
                $attrvalue = trim($row->attrvalue);
                $attr_values_id = trim($row->attr_values_id);
                if($attrvalue!="" && $attr_values_id!=""){
                    $addprice = explode(",", $addprice);
                    $attrvalue = explode(",", $attrvalue);
                    $attr_values_id = explode(",", $attr_values_id);
                    $selects = '<select id="jshop_attr_id'.$row->attr_id.'" name="jshop_attr_id['.$row->attr_id.']" ';
                    $selects .= 'class = "inputbox" size = \'1\' onchange=\'setAttrValue("'.$row->attr_id.'", this.value);\'>';
                    $firstval = $attr_values_id[0];
                    for($i=0; $i<count($attrvalue); $i++){
                        $selects .='<option value="'.$attr_values_id[$i].'"';
                        if($i==0) { $selects .=' selected="selected"'; }
                        $selects .='>'.$attrvalue[$i].'</option>';
                    }
                    $selects .= '</select><span class="prod_attr_img"><img id="prod_attr_img_1" src="/components/com_jshopping/images/blank.gif" alt="" /></span>';
                    $view->attributes[$row->product_id][0] = new stdClass;
                    $view->attributes[$row->product_id][0]->{"attr_name"} = $row->attrname;
                    $view->attributes[$row->product_id][0]->{"attr_id"} = $row->attr_id;
                    $view->attributes[$row->product_id][0]->{"attr_description"} = "";
                    $view->attributes[$row->product_id][0]->{"firstval"} = $firstval;
                    $view->attributes[$row->product_id][0]->{"selects"} = $selects;
                }
            }
        }
    }
    
    function onBeforeDisplayProduct(&$product, &$view, &$product_images, &$product_videos, &$product_demofiles){
        $view->attr_values = $product->product_attribute_datas["attributeValues"];
    }

}