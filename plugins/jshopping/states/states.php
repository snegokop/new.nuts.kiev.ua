<?php
defined('_JEXEC') or die('Restricted access');
class plgJshoppingStates extends JPlugin{
    
    function onLoadMultiLangTableField(&$obj){
        $f=array();
        $f[] = array("name","varchar(255) NOT NULL");
        $obj->tableFields["states"] = $f;
    }

}