<?php
defined('_JEXEC') or die('Restricted access');
class plgJshoppingLabelUnset extends JPlugin{
    
    protected static $_params = array();

    public function __construct(&$subject, $config = array()) {
        if(isset($config["params"]) && $config["params"]!=""){ //$config["params"]=" ";
            if(!self::$_params = (array)json_decode($config["params"])){
                self::$_params = array("deleteLabels"=>"0","default_timer"=>"172800");
            }
            else {
                $default_timer = isset(self::$_params["default_timer"])?(int)self::$_params["default_timer"]:2;
                self::$_params["default_timer"] = $default_timer*86400;
            }
        }
        parent::__construct($subject, $config);
    }
    
    function onAfterLoadShopParamsAdmin($item=array()){
        
         if(!$ajax_edit = JRequest::getCmd('ajax_edit')) return;
         if($ajax_edit!="1") return;
         if(!$product_id = JRequest::getCmd('product_id')) return;

         $task = JRequest::getCmd('task');
         $db = JFactory::getDBO();
         if($task=='edit_label_product'){
             $label = JRequest::getCmd('label', '0');
             if($label!='0' && $label!='' && $label){
                 $label_deadline = JRequest::getCmd('label_deadline');
                 if($label_deadline==''){
                     $label_deadline = date("Y-m-d", (time()+self::$_params["default_timer"]));
                 }
                 $label_start = JRequest::getCmd('label_start');
                 if($label_start=='') $label_start = date("Y-m-d");
             }
             else {
                 $label = '0';
                 $label_deadline = $label_start = "0000-00-00";
             }
             //if(!$label || !$label_deadline) return;
             $query = 'UPDATE `#__jshopping_products` SET `label_id`="'.$label.'", `label_start`="'.$label_start.'", `label_deadline`="'.$label_deadline.'" WHERE `product_id`='.(int)$product_id;
             $db->setQuery($query);
             $db->execute();
             echo json_encode(array("success"=>true)); 
             die;
         }
         
         $query = "SELECT p.`label_id`, p.`label_start`, p.`label_deadline` FROM `#__jshopping_products` AS p WHERE p.`product_id`=".$product_id;
         $db->setQuery($query);
         $product_label = $db->loadAssoc();
         
         $db->setQuery('SHOW COLUMNS FROM `#__jshopping_product_labels` WHERE `Field` LIKE "name%"');
         foreach($db->loadAssocList() as $field){
             if(strstr($field["Field"], 'name')) $field_name = $field["Field"];
         }
         
         $query = "SELECT `id`, `".$field_name."` AS `name_ru-RU` FROM `#__jshopping_product_labels`"; //die;
         $db->setQuery($query);
         $labels = $db->loadAssocList(); 
         
         $return = array("product_label"=>$product_label, "labels"=>$labels);
         echo json_encode($return);
         die();
    }
    
    function onBeforeQueryGetProductList($name, &$adv_result, &$adv_from, &$adv_query, &$order_query, &$filters){
        if($name==="category"){
            $adv_result .=", prod.label_start AS label_start, prod.label_deadline AS label_deadline ";
        }
        if($name=="label_products"){
             $adv_query .=" AND prod.`label_deadline` > '".date("Y-m-d")."' AND prod.`label_start` <= '".date("Y-m-d")."'";
        }
    }
    
    function onBeforeDisplayProductView(&$view){
        if($view->product->label_id=="0") return;
        $product = &$view->product;
        $to_delete = array();
        if($product->label_deadline<date('Y-m-d')){
            $product->label_id = "0"; 
            $to_delete[] = $product->product_id;
            unset($product->_label_image, $product->_label_name);
        }
        elseif($product->label_start>date('Y-m-d')){
            $product->label_id = "0";
            unset($product->_label_image, $product->_label_name);
        }
        self::deleteLabels($to_delete);
    }
    
    function onBeforeDisplayProductListView(&$view){
        $date = date('Y-m-d');
        $to_delete = array();
        foreach($view->rows as &$row){
            if($row->label_id=="0") continue;
            else{
                if($row->label_deadline<$date) {
                    $row->label_id = "0"; 
                    unset($row->_label_image, $row->_label_name);
                    $to_delete[] = (int)$row->product_id;
                }        
                elseif($row->label_start>date('Y-m-d')){
                    $row->label_id = "0";
                    unset($row->_label_image, $row->_label_name);
                }
            }
        }
        self::deleteLabels($to_delete);
    }
    
    function onBeforeDisplayListProductsView(&$view){
         if(empty($view->rows)) return;
         $doc = JFactory::getDocument();
         $doc->addScript("/plugins/jshopping/labelunset/js/myscript.js");
         $doc->addStyleSheet("/plugins/jshopping/labelunset/css/mystyle.css");
    }
    
    private static function deleteLabels($ids = array()){
        if(self::$_params["deleteLabels"]=="0" || empty($ids)) return;
        $query = 'UPDATE `#__jshopping_products` SET `label_id`="0", `label_start`="0000-00-00", `label_deadline`="0000-00-00" WHERE `product_id` IN ('.implode(",", $ids).')';
        $db = JFactory::getDBO();
        $db->setQuery($query);
        $db->execute();       
    }
}