    
    function closeWindowEdit(){
        var window_edit_label = getEditWindow();
        if(!window_edit_label) return false;   
        window_edit_label.style.display = "none";
    }
    
    function createWindowEdit(){
        var window_edit_label = jQuery('<div id="window_edit_label"></div>');
        jQuery(window_edit_label).css("left", ((document.documentElement.clientWidth-400)/2)+"px");
        jQuery("body").append(window_edit_label);
        var close_window_edit = jQuery('<span id="close_window_edit" onclick="closeWindowEdit();">Закрыть</span>');
        jQuery(window_edit_label).append(close_window_edit);
        return window_edit_label;
    }
    
    function getEditWindow(){
        var window_edit_label = document.getElementById("window_edit_label");
        if(undefined===window_edit_label || window_edit_label=="" || window_edit_label==null)
            window_edit_label = false;
        return window_edit_label;
    }
    
    function submitLabel(){
        var product_label_form = document.getElementById("product_label_form");
        if(undefined===product_label_form || product_label_form=="" || product_label_form==null)
            product_label_form = false;
        if(!product_label_form) return;
        
        jQuery.ajax({
             type: "GET",
             url: "/administrator/index.php",
             data: jQuery(product_label_form).serialize(),
             dataType: "json",
             success: function(data){
                 if(!data.success) alert("Error saved !");
				 jQuery(product_label_form).html('<div class="success_message"><H2>Успешно!</H2></div>');
				 setTimeout(function(){ closeWindowEdit(); jQuery(product_label_form).html(''); }, 800);
                 return;
             }
        });
        return;
    }
    
    function labelEdit(product_id){
        if(undefined===product_id || product_id=="" || product_id==null)product_id = false;
        if(!product_id) return;
        var productId = product_id;
        var window_edit_label = getEditWindow();
        if(!window_edit_label){
            window_edit_label = createWindowEdit();
        } 
        else {
            window_edit_label.style.display = "block";
            var form = jQuery(window_edit_label).find('form')[0];
            jQuery(window_edit_label).css("left", ((document.documentElement.clientWidth-400)/2)+"px");
            jQuery(form).remove();
		}
        jQuery.get("/administrator/index.php", {
              "option":"com_jshopping", 
              "controller":"products", 
              "task":"", 
              "product_id":product_id,
              "ajax_edit":"1"
            },
            function(data){
                var product_label = data.product_label;
                var labels = data.labels;
                var select = '<select name="label"><option></option>';
                var selected;
                for(var i=0; i<labels.length; i++){
                    var tmp = labels[i];
                    selected = "";
                    if(tmp.id==product_label["label_id"]) selected="selected ";
                    select+='<option '+selected+'value="'+tmp.id+'">'+tmp["name_ru-RU"]+'</option>';
                }
                select+= '</select>';
                
                var html = '<form id="product_label_form"><ul><li><div>Выбрать метку :</div>'+select+'</li>';
                html+='<li><div>Конечная дата :</div><input type="date" name="label_deadline" value="'+product_label["label_deadline"]+'"></li>';
                html+='<li><div>Начальная дата :</div><input type="date" name="label_start" value="'+product_label["label_start"]+'"></li>';
				html+='<li><div>&nbsp;</div><input type="button" onclick="submitLabel();" value="Отправить" /></li>';
                html+='</ul><input type="hidden" name="product_id" value="'+productId+'" />\n\
                <input type="hidden" name="option" value="com_jshopping" />\n\
                <input type="hidden" name="ajax_edit" value="1" />\n\
                <input type="hidden" name="controller" value="products" />\n\
                <input type="hidden" name="task" value="edit_label_product" /></form>';
                jQuery(window_edit_label).append(html);
            },
            "json"
        );
    }

