<?php
/**
* @version      3.9.0 03.08.2012
* @author       MAXXmarketing GmbH
* @package      Jshopping Auction
* @copyright    Copyright (C) 2012 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class plgJshoppingMenuJshopping_menu_builder extends JPlugin {
	function __construct(& $subject, $config){
		parent::__construct($subject, $config);
		JSFactory::loadExtAdminLanguageFile('addon_jshopping_menu_builder');
	}
	
	function onBeforeAdminMenuDisplay(&$menu, &$vName) {
		$menu['menu_builder'] = array(_JSHOP_MENU_BUILDER, 'index.php?option=com_jshopping&controller=addon_menu_builder', $vName == 'menu_builder', 1);
	}
	
	function onBeforeAdminMainPanelIcoDisplay(&$menu) {
		$menu['menu_builder'] = array(_JSHOP_MENU_BUILDER, 'index.php?option=com_jshopping&controller=addon_menu_builder', 'jshop_country_list_b.png', 1);
	}
}
?>