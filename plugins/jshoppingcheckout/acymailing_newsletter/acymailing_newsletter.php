<?php
/**
* @version      1.0.1 24.07.2012
* @author       MAXXmarketing GmbH
* @package      Jshopping Auction
* @copyright    Copyright (C) 2012 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/

defined('_JEXEC') or die('Restricted access');

class plgJshoppingCheckoutAcymailing_newsletter extends JPlugin
{
	function plgJshoppingCheckoutAcymailing_newsletter(&$subject, $config){        
		parent::__construct($subject, $config);
		JSFactory::loadExtLanguageFile('addon_acymailing_newsletter');
		define('NO_SUBSCRIPTION', 0);
		define('UNSUBSCRIBED', -1);
		define('WAIT_CONFIRMATION', 2);
		define('SUBSCRIBED', 1);
	}
	
	function _getSubscriberWithMail($user_email) {
		$db = JFactory::getDBO();
		$query = "SELECT *
					FROM `#__acymailing_subscriber`
					WHERE `email`='".$db->getEscaped($user_email)."' LIMIT 1";
		$db->setQuery($query);
		return $db->LoadObject();
	}
	
	function _getAcyUser($adv_user, $user) {
		$acy_user = new stdclass();
		if ($user->id) {
			$acy_user->email = $user->email;
			$acy_user->name = $user->username;
		} else {
			$acy_user->email = $adv_user->email;
			$acy_user->name = $adv_user->f_name;
		}
		return $acy_user;
	}
	
	function _getListStatusForSubscriber($subid) {
		$db = &JFactory::getDBO();
		$listid = $this->params->get('listid', 1);
		$query = 'SELECT `status`
					FROM `#__acymailing_listsub`
					WHERE `subid`='.$subid.' AND `listid`='.$listid.' LIMIT 1';
		$db->setQuery($query);
		return $db->LoadResult();
	}
	
	function _checkUserInAcymailing($subid) {
		$status = self::_getListStatusForSubscriber($subid);
		if (($status == SUBSCRIBED) || ($status == WAIT_CONFIRMATION)) return true;
		return false;
	}
	
	function _addSubscriber($user) {
		$db = &JFactory::getDBO();
		$query = "INSERT INTO `#__acymailing_subscriber` (`email`, `userid`, `name`, `created`, `confirmed`, `enabled`, `accept`, `html`) VALUES ( 
					'".$db->getEscaped($user->email)."', 0, '".$db->getEscaped($user->name)."', ".time().", 1, 1, 1, 1);";
		$db->setQuery($query);
		$db->query();
		return $db->insertid();
	}
	
	function _addStatusForSubscriber($subid, $status) {
		$db = &JFactory::getDBO();
		$listid = (int)$this->params->get('listid', 1);
		$query = 'INSERT INTO `#__acymailing_listsub` (`listid`, `subid`, `subdate`, `status`) VALUES ( 
					'.$listid.', '.$subid.', '.time().', '.$status.');';
		$db->setQuery($query);
		return $db->query();
	}
	
	function _updateStatusForSubscriber($subid, $status) {
		$db = &JFactory::getDBO();
		$listid = (int)$this->params->get('listid', 1);
		$unsubdate = '';
		if ($status == UNSUBSCRIBED) {
			$unsubdate = ', `unsubdate`='.time();
		}
		$query = 'UPDATE `#__acymailing_listsub`
				SET `status`='.$status.$unsubdate.'
				WHERE `listid`='.$listid.' AND `subid`='.$subid;
		$db->setQuery($query);
		return $db->query();
	}
	
	function _setStatusInAcyMailing($acymailing_subscribe, $acy_user) {
		$subscriber = self::_getSubscriberWithMail($acy_user->email);
		if (is_object($subscriber) && ($subscriber->subid)) {
			$status = self::_getListStatusForSubscriber($subscriber->subid);
			if ($acymailing_subscribe) {
				if ($status == NO_SUBSCRIPTION) {
					self::_addStatusForSubscriber($subscriber->subid, SUBSCRIBED);
				} elseif ($status == UNSUBSCRIBED) {
					self::_updateStatusForSubscriber($subscriber->subid, SUBSCRIBED);
				}
			} else {
				if (($status == WAIT_CONFIRMATION) || ($status == SUBSCRIBED)) {
					self::_updateStatusForSubscriber($subscriber->subid, UNSUBSCRIBED);
				}
			}
		} else {
			if ($acymailing_subscribe) {
				$subid = self::_addSubscriber($acy_user);
				self::_addStatusForSubscriber($subid, SUBSCRIBED);
			}
		}
	}
	
	function _showCheckboxWithCheckingStatus($tmp_var_name, &$view) {
		if (!isset($view->$tmp_var_name)) $view->$tmp_var_name = '';
		$subscriber = self::_getSubscriberWithMail($view->user->email);
		if (is_object($subscriber) && ($subscriber->subid)) {
			$checked = (self::_checkUserInAcymailing($subscriber->subid)) ? ' checked="checked"' : '';
		} else {
			$checked = '';
		}
		$view->$tmp_var_name .= '<tr><td class="name">'._JSHOP_ACYMAILING_SUBSCRIBE.'</td><td><input type="checkbox" name="acymailing_subscribe" value="1"'.$checked.'/></td></tr>';
	}
	
	function onBeforeDisplayCheckoutStep2View(&$view) {
		self::_showCheckboxWithCheckingStatus('_tmpl_address_html_4', $view);
	}
	
	function onAfterSaveCheckoutStep2(&$adv_user, &$user, &$cart) {
		$post = JRequest::get('post');
		$acymailing_subscribe = (isset($post['acymailing_subscribe']) && ($post['acymailing_subscribe'] == 1)) ? 1 : 0;
		$acy_user = self::_getAcyUser($adv_user, $user);
		self::_setStatusInAcyMailing($acymailing_subscribe, $acy_user);
	}
	
	function onBeforeDisplayRegisterView(&$view) {
		$registration_default = (int)$this->params->get('registration_default', 0);
		$checked = $registration_default ? ' checked="checked"' : '';
		if (!isset($view->_tmpl_register_html_5)) $view->_tmpl_register_html_5 = '';
		$view->_tmpl_register_html_5 .= '<div class = "jshop_register"><table><tr><td class="name">'._JSHOP_ACYMAILING_SUBSCRIBE.'</td><td><input type="checkbox" name="acymailing_subscribe" value="1"'.$checked.'/></td></tr></table></div>';
	}
	/*
	function onAfterRegister(&$user, &$row, &$_POST, &$useractivation) {
		$acymailing_subscribe = (isset($_POST['acymailing_subscribe']) && ($_POST['acymailing_subscribe'] == 1)) ? 1 : 0;
		$acy_user = new stdclass();
		$acy_user->email = $user->email;
		$acy_user->name = $user->username;
		self::_setStatusInAcyMailing($acymailing_subscribe, $acy_user);
	}
	*/
	function onBeforeDisplayEditAccountView(&$view) {
		self::_showCheckboxWithCheckingStatus('_tmpl_editaccount_html_4', $view);
	}
	
	function onAfterAccountSave() {
		$user = JFactory::getUser();
		$post = JRequest::get('post');
		$acymailing_subscribe = (isset($post['acymailing_subscribe']) && ($post['acymailing_subscribe'] == 1)) ? 1 : 0;
		$acy_user = new stdclass();
		$acy_user->email = $user->email;
		$acy_user->name = $user->f_name;
		self::_setStatusInAcyMailing($acymailing_subscribe, $acy_user);
	}
}