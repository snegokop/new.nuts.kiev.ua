<?php
defined('_JEXEC') or die('Restricted access');
class plgJshoppingCheckoutStates extends JPlugin{
    
    function _loadingStatesScripts(){
        $document =& JFactory::getDocument();
        $document->addCustomTag('<script type="text/javascript" src="'.JURI::root().'components/com_jshopping/js/states.js"></script>');
    ?>
    <script type="text/javascript">
        var urlstates = '<?php echo JRoute::_('index.php?option=com_jshopping&controller=states&task=statesAjax&ajax=1',0);?>';
    </script>
    <?php
    }
    
    function onBeforeDisplayCheckoutStep2View(&$view){
        $this->_loadingStatesScripts();
    }
    
    function onBeforeDisplayEditAccountView(&$view){
        $this->_loadingStatesScripts();
    }
    
    function onBeforeDisplayRegisterView(&$view){
        $this->_loadingStatesScripts();
    }
}