<?php
/**
 * @version		1.0.6 from Arkadiy Sedelnikov
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later;
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgContentLVSpoiler extends JPlugin {

    public function onContentPrepare($context, &$article, &$params, $limitstart) {
        $regex = "#{spoiler(?: title=(([_0-9A-Za-zА-яа-яЁё](.*?)))?)?[\s|&nbsp;]opened=([0-9](.*?))}(.*?){/spoiler}#s";

        $article->text = preg_replace_callback($regex, array($this, 'LVSpoiler_replacer'), $article->text);
        return true;
    }

    private function LVSpoiler_css() {

        $lang = JFactory::getLanguage();
        $lang->load('plg_content_LVSpoiler', JPATH_ADMINISTRATOR);
        $plugin = & JPluginHelper::getPlugin('content', 'LVSpoiler');
        $pluginParams = new JRegistry($plugin->params);
        $jsjquery = $pluginParams->get('jsjquery', 1);
        $jsshow = $pluginParams->get('jsshow', 1);
        $jstype = $pluginParams->get('jstype', 1);
        $cssload = $pluginParams->get('cssload', 1);

 	$titlesize = $pluginParams->get('titlesize', 18);
    $titlecolor = $pluginParams->get('titlecolor', '008aeb');
    $titlebordersize = $pluginParams->get('titlebordersize', 0);
    $titlebordercolor = $pluginParams->get('titlebordercolor', 'ccc');
    $titlestyle = $pluginParams->get('titlestyle', 'normal');
    $titleborderstyle = $pluginParams->get('titleborderstyle', 'solid');
   
    $titleborderradius = $pluginParams->get('titleborderradius', '4');
    $titlewidth = $pluginParams->get('titlewidth', '95%');
    $titlebgcolor = $pluginParams->get('titlebgcolor', 'eeeeee');
    
    $spoilerfontsize = $pluginParams->get('spoilerfontsize', 11);
    $spoilerfontweight = $pluginParams->get('spoilerfontweight', 'normal');
    $spoilerbg = $pluginParams->get('spoilerbg', 'FFFDDD');
    $spoilerbordersize = $pluginParams->get('spoilerbordersize', 1);
    $spoilerbordercolor = $pluginParams->get('spoilerbordercolor', 'ccc');
    $spoilerborderstyle = $pluginParams->get('spoilerborderstyle', 'solid');
    $spoilerborderradius = $pluginParams->get('spoilerborderradius', 7);
    $spoilerwidth = $pluginParams->get('spoilerwidth', '95%');
    $spoilerstyle = $pluginParams->get('spoilerstyle', 'italic');
    $spoilerpadding = $pluginParams->get('spoilerpadding', 10);
        $revealtype = $pluginParams->get('revealtype', 'click');
        $mouseoverdelay = $pluginParams->get('mouseoverdelay', 200);
        $collapseprev = $pluginParams->get('collapseprev', 0);
        $onemustopen = $pluginParams->get('onemustopen', 0);
        $animatedefault = $pluginParams->get('animatedefault', 0);
        $animatespeed = $pluginParams->get('animatespeed', 'fast');
        $togglehtml = $pluginParams->get('togglehtml', 'none');
        if ($togglehtml == 'none') {
            $togglehtml1 = '';
            $togglehtml2 = '';
        } else {
            $togglehtml1 = $pluginParams->get('togglehtml1', '');
            $togglehtml2 = $pluginParams->get('togglehtml2', '');
        }

        if (($jstype == 1) && $this->includeOnce('Spoiler_Mootools')) {
            $revealtype = ($revealtype == 'clickgo') ? 'click' : $revealtype;
            $document = & JFactory::getDocument();
            JHTML::_('behavior.mootools');
            $document->addScript(JURI::base() . 'plugins/content/LVSpoiler/LVSpoiler/mootools/spoiler.js');
            if ($cssload == 1) {
                $document->addStyleSheet(JURI::base() . 'plugins/content/LVSpoiler/LVSpoiler/mootools/spoiler.css');
            }
        $css = '
        .sp-head-click a{font-size: ' . $titlesize . 'px; font-style: ' . $titlestyle . '; color: #' . $titlecolor . ' !important; }
	.sp-head{border: ' . $titlebordersize . 'px #' . $titlebordercolor . ' ' . $titleborderstyle . '; font-weight: bold;
            -webkit-border-radius: ' . $titleborderradius . 'px;
            -moz-border-radius: ' . $titleborderradius . 'px;
            -khtml-border-radius: ' . $titleborderradius . 'px;
            border-radius: ' . $titleborderradius . 'px;
            width: ' . $titlewidth . ';
            background-color: #' . $titlebgcolor . '; 
        }        
	.sp-body{font-size: ' . $spoilerfontsize . 'px; font-weight: ' . $spoilerfontweight . '; background: #' . $spoilerbg . '; border: ' . $spoilerbordersize . 'px #' . $spoilerbordercolor . ' ' . $spoilerborderstyle . '; 
	-webkit-border-radius: ' . $spoilerborderradius . 'px;
	-moz-border-radius: ' . $spoilerborderradius . 'px;
	-khtml-border-radius: ' . $spoilerborderradius . 'px;
	border-radius: ' . $spoilerborderradius . 'px;
	width: ' . $spoilerwidth . ';
	font-style: ' . $spoilerstyle . ';
	padding: ' . $spoilerpadding . 'px}	
	';
        $document->addStyleDeclaration($css);

            $jscode = '
                        var pb_sp_conf = {
                            revealtype: "' . $revealtype . '",
                            mouseoverdelay: ' . $mouseoverdelay . ',
                            collapseprev: ' . $collapseprev . ',
                            onemustopen: ' . $onemustopen . ',
                            animatedefault: ' . $animatedefault . ',
                            animatespeed: ' . $animatespeed . '
                        };';
            $document->addScriptDeclaration($jscode);
        }
        if (($jstype == 2) && $this->includeOnce('Spoiler_Jquery')) {
            $document = & JFactory::getDocument();
            if ($jsjquery == 1) {
                $document->addScript(JURI::base() . 'plugins/content/LVSpoiler/LVSpoiler/jquery/jquery.js');
            }
            $document->addScript(JURI::base() . 'plugins/content/LVSpoiler/LVSpoiler/jquery/ddaccordion.js');
        $css = '
        .technology div{font-size: ' . $titlesize . 'px; color: #' . $titlecolor . '; font-style: ' . $titlestyle . ';border: ' . $titlebordersize . 'px #' . $titlebordercolor . ' ' . $titleborderstyle . '; font-weight: bold;
            -webkit-border-radius: ' . $titleborderradius . 'px;
            -moz-border-radius: ' . $titleborderradius . 'px;
            -khtml-border-radius: ' . $titleborderradius . 'px;
            border-radius: ' . $titleborderradius . 'px;
            width: ' . $titlewidth . ';
            background-color: #' . $titlebgcolor . '; 
        }       
	.thetextinter{font-size: ' . $spoilerfontsize . 'px; font-weight: ' . $spoilerfontweight . '; background: #' . $spoilerbg . '; border: ' . $spoilerbordersize . 'px #' . $spoilerbordercolor . ' ' . $spoilerborderstyle . '; 
	-webkit-border-radius: ' . $spoilerborderradius . 'px;
	-moz-border-radius: ' . $spoilerborderradius . 'px;
	-khtml-border-radius: ' . $spoilerborderradius . 'px;
	border-radius: ' . $spoilerborderradius . 'px;
	width: ' . $spoilerwidth . ';
	font-style: ' . $spoilerstyle . ';
	padding: ' . $spoilerpadding . 'px}	
	';
        $document->addStyleDeclaration($css);

            $jscode = '
		ddaccordion.init({
		headerclass: "technology",
		contentclass: "thelanguage",
		revealtype: "' . $revealtype . '",
		mouseoverdelay: ' . $mouseoverdelay . ',
		collapseprev: ' . $collapseprev . ',
		defaultexpanded: [],
		onemustopen: ' . $onemustopen . ',
		animatedefault: ' . $animatedefault . ',
		toggleclass: ["closedlanguage", "openlanguage"],
		togglehtml: ["' . $togglehtml . '", "' . $togglehtml1 . '", "' . $togglehtml2 . '"],
		animatespeed: "' . $animatespeed . '",
		oninit:function(expandedindices){
			//do nothing
		},
		onopenclose:function(header, index, state, isuseractivated){
			//do nothing
		}
	});';
            $document->addScriptDeclaration($jscode);
            if ($cssload == 1) {
                $document->addStyleSheet(JURI::base() . 'plugins/content/LVSpoiler/LVSpoiler/jquery/style.css');
            }
        }
    }

    private function LVSpoiler_replacer(&$matches) {
        //$jstype = LVSpoiler_params();
        $this->LVSpoiler_css();
        //нумерация каждого спойлера (если нужно)
        global $numspoilers;
        if (!$numspoilers) {
            $numspoilers = 1;
        } else {
            $numspoilers++;
        }

        $plugin = & JPluginHelper::getPlugin('content', 'LVSpoiler');
        $pluginParams = new JRegistry($plugin->params);
        $jstype = $pluginParams->get('jstype', 1);
        $load_img = $pluginParams->get('load_img', 0);
        $revealtype = $pluginParams->get('revealtype', '');
        $opened = $matches[4];

        $html = '';
        $regex1 = "#{spoiler title=([_0-9A-Za-zА-яа-яЁё](.*?))}#s";
        $regex2 = "#{/spoiler}#s";
        $spoilertext = preg_replace($regex2, '', (preg_replace($regex1, '', $matches[0])));

        //обработка изображений если установлена загрузка после открытия спойлера
        if ($load_img == 1 && $opened != 1) {
            $search = array("src=", "src =", "src  =");
            $replace = 'src="#" class="spoilerimage" data_src=';
            $spoilertext = str_replace($search, $replace, $spoilertext);
        }

        switch ($jstype) {
            case '1'; //mootools

                $fold_class = ($opened == 1) ? 'unfolded' : 'folded';

                $url = 'javascript:void(0)';

                if ($revealtype == 'clickgo') {
                    $uri = & JURI::getInstance();
                    $base = $uri->toString(array('scheme', 'host', 'port'));
                    $url = $base . $_SERVER['REQUEST_URI'] . '#spoiler_' . $numspoilers;

                    //якорь к которому будет отсылать спойлер при нажатии
                    $html .= '<a name="spoiler_' . $numspoilers . '"></a>';
                }
                $html .= '<div class="spoiler" id="' . $numspoilers . '_spoiler">
                        <input type="hidden" class="opened" value="' . $opened . '">
			<div class="sp-head ' . $fold_class . '" id="' . $numspoilers . '-sp-head">
			<div class="sp-head-click" id="' . $numspoilers . '-sp-head-click"><a href="' . $url . '">' . $matches[1] . '</a></div></div>
			<div class="sp-body" id="' . $numspoilers . '-sp-body">' . $spoilertext . '</div>
			</div>';
                break;
            case '2'; //jquery
                $html .= '<div  id="spoiler_' . $numspoilers . '" class="technology"><div>' . $matches[1] . '</div></div><div class="thelanguage"><input type="hidden" class="opened" value="' . $opened . '"><div class="thetextinter">' . $spoilertext . '</div></div>';
                break;
        }
        return $html;
    }

    private function includeOnce($name) {
        if (!defined($name)) {
            define($name, true);
            return true;
        }
        return false;
    }

}

?>