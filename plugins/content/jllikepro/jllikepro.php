<?php
/**
 * jllikepro
 *
 * @version 2.5.1
 * @author Vadim Kunicin (vadim@joomline.ru), Arkadiy (a.sedelnikov@gmail.com)
 * @copyright (C) 2010-2013 by Vadim Kunicin (http://www.joomline.ru)
 * @license GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 **/

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

require_once JPATH_ROOT.'/plugins/content/jllikepro/helper.php';

class plgContentjllikepro extends JPlugin
{
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    public function onContentPrepare($context, &$article, &$params, $page = 0)
    {
        if(JFactory::getApplication()->isAdmin())
        {
            return true;
        }

        $allowContext = array(
            'com_content.article',
            'easyblog.blog',
            'com_virtuemart.productdetails'
        );

        $allow_in_category = $this->params->get('allow_in_category', 0);

        if($allow_in_category)
        {
            $allowContext[] = 'com_content.category';
            $allowContext[] = 'com_content.featured';
        }

        if(!in_array($context, $allowContext)){
            return true;
        }

        if (strpos($article->text, '{jllikepro-off}') !== false) {
            $article->text = str_replace("{jllikepro-off}", "", $article->text);
            return true;
        }

        $autoAdd = $this->params->get('autoAdd',0);
        $sharePos = (int)$this->params->get('shares_position', 1);
        $option = JRequest::getCmd('option');
        $helper = PlgJLLikeProHelper::getInstance($this->params);

        if (strpos($article->text, '{jllikepro}') === false && !$autoAdd)
        {
            return true;
        }

        if (!isset($article->catid))
        {
            $article->catid = '';
        }

        $print = JRequest::getInt('print', 0);

        $url = 'http://' . $this->params->get('pathbase', '') . str_replace('www.', '', $_SERVER['HTTP_HOST']);

        if($this->params->get('punycode_convert',0))
        {
            $file = JPATH_ROOT.'/libraries/idna_convert/idna_convert.class.php';
            if(!JFile::exists($file))
            {
                return JText::_('PLG_JLLIKEPRO_PUNYCODDE_CONVERTOR_NOT_INSTALLED');
            }

            include_once $file;

            if($url)
            {
                if (class_exists('idna_convert'))
                {
                    $idn = new idna_convert;
                    $url = $idn->encode($url);
                }
            }
        }

        switch ($option) {
            case 'com_content':

                if(empty($article->id))
                {
                    //если категория, то завершаем
                    return true;
                }

                if($print)
                {
                    $article->text = str_replace("{jllikepro}", "", $article->text);
                    return true;
                }

                $cat = $this->params->get('categories', array());
                $exceptcat = is_array($cat) ? $cat : array($cat);

                if (in_array($article->catid, $exceptcat))
                {
                    $article->text = str_replace("{jllikepro}", "", $article->text);
                    return true;
                }


                include_once JPATH_ROOT.'/components/com_content/helpers/route.php';
                $link = $url . JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid));

                $image = '';
                if($this->params->get('content_images', 'fields') == 'fields')
                {
					If(!empty($article->images))
					{
						$images = json_decode($article->images);
	
						if(!empty($images->image_intro))
						{
							$image = $images->image_intro;
						}
						else if(!empty($images->image_fulltext))
						{
							$image = $images->image_fulltext;
						}
	
						if(!empty($image))
						{
							$image = JURI::root().$image;
						}
					}
                    
                }
                else
                {
                    $image = PlgJLLikeProHelper::extractImageFromText($article->introtext, $article->fulltext);
                }

                $shares = $helper->ShowIN($article->id, $link, $article->title, $image);

                if ($context == 'com_content.article')
                {

                    $view = JRequest::getCmd('view');
                    if ($view == 'article')
                    {
                        if ($autoAdd == 1 || strpos($article->text, '{jllikepro}') == true)
                        {
                            $helper->loadScriptAndStyle(0);

                            $text = ($this->params->get('desc_source_com_content', 'intro') == 'intro') ? $article->introtext : $article->text;

                            PlgJLLikeProHelper::addOpenGraphTags($article->title, $text, $image);

                            switch($sharePos)
                            {
                                case 0:
                                    $article->text = $shares . str_replace("{jllikepro}", "", $article->text);
                                    break;
                                default:
                                    $article->text = str_replace("{jllikepro}", "", $article->text) . $shares;
                                    break;
                            }
                        }
                    }
                }
                else if ($context == 'com_content.category')
                {
                    if ($autoAdd == 1 || strpos($article->text, '{jllikepro}') == true)
                    {
                        $helper->loadScriptAndStyle(1);
                        $article->text = str_replace("{jllikepro}", "", $article->text) . $shares;
                    }
                }
                break;
            case 'com_virtuemart':
                if ($context == 'com_virtuemart.productdetails') {
                    $VirtueShow = $this->params->get('virtcontent', 1);
                    if ($VirtueShow == 1)
                    {
                        $autoAddvm = $this->params->get('autoAddvm', 0);
                        if ($autoAddvm == 1 || strpos($article->text, '{jllikepro}') !== false)
                        {
                            $helper->loadScriptAndStyle(0);
                            $uri = JString::str_ireplace(JURI::root(), '', JURI::current());
                            $link = $url.'/'.$uri;
                            $image = PlgJLLikeProHelper::getVMImage($article->id);

                            $text = ($this->params->get('desc_source_virtuemart', 'intro') == 'intro') ? $article->product_s_desc : $article->product_desc;

                            PlgJLLikeProHelper::addOpenGraphTags($article->product_name, $text, $image);

                            $shares = $helper->ShowIN($article->id, $link, $article->product_name, $image);

                            switch($sharePos){
                                case 0:
                                    $article->text = $shares . str_replace("{jllikepro}", "", $article->text);
                                    break;
                                default:
                                    $article->text = str_replace("{jllikepro}", "", $article->text) . $shares;
                                    break;
                            }
                        }
                    }
                }
                break;
            case 'com_easyblog':
                if (($context == 'easyblog.blog') && ($this->params->get('easyblogshow', 0) == 1))
                {
					$allow_in_category = $this->params->get('allow_in_category', 0);
					$isCategory = (JRequest::getCmd('view', '') == 'entry') ? false : true;
					
					if(!$allow_in_category && $isCategory)
					{
						return true;
					}
					
                    if ($autoAdd == 1 || strpos($article->text, '{jllikepro}') == true)
                    {
                        $helper->loadScriptAndStyle(0);
                        $uri = JString::str_ireplace(JURI::root(), '', JURI::current());
                        $link = $url.'/'.$uri;

                        $image = '';
                        if($this->params->get('easyblog_images','fields') == 'fields'){
                            $images = json_decode($article->image);
                            if(isset($images->type) && $images->type == 'image')
                            {
                                $image = $images->url;
                            }
                        }
                        else
                        {
                            $image = PlgJLLikeProHelper::extractImageFromText($article->intro, $article->content);
                        }

                        if($this->params->get('easyblog_add_opengraph', 0) && !$isCategory)
                        {
                            $text = ($this->params->get('desc_source_easyblog', 'intro') == 'intro') ? $article->intro : $article->content;
                            PlgJLLikeProHelper::addOpenGraphTags($article->title, $text, $image);
                        }

                        $shares = $helper->ShowIN($article->id, $link, $article->title, $image);
                        switch($sharePos){
                            case 0:
                                $article->text = $shares . str_replace("{jllikepro}", "", $article->text);
                                break;
                            default:
                                $article->text = str_replace("{jllikepro}", "", $article->text) . $shares;
                                break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}