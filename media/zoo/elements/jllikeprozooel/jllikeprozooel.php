<?php
/**
 * jllikepro
 *
 * @version 2.4.4
 * @author Vadim Kunicin (vadim@joomline.ru)
 * @copyright (C) 2010-2013 by Vadim Kunicin (http://www.joomline.ru)
* @license GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 **/
defined('_JEXEC') or die ;

require_once JPATH_ROOT.'/plugins/content/jllikepro/helper.php';

class ElementJlLikeProZooEl extends Element implements iSubmittable {

	public function hasValue($params = array())
    {
		return (bool) $this->get('value', $this->config->get('default', 1));
	}


	public function render($params = array())
    {
		if (!$this->get('value', $this->config->get('default', 1)))
        {
            return '';
        }

        $item_route = JRoute::_($this->app->route->item($this->_item, false), true, -1);
        JPlugin::loadLanguage('plg_content_jllikepro');
        $plugin = JPluginHelper::getPlugin('content', 'jllikepro');
        $plgParams = new JRegistry($plugin->params);

        $parent_contayner = $this->config->get('parent_contayner', '');
        if(!empty($parent_contayner))
        {
            $plgParams->set('parent_contayner', $parent_contayner);
        }
        $helper = PlgJLLikeProHelper::getInstance($plgParams);
        $helper->loadScriptAndStyle(0);

        $intro = $text = $image = '';

        $field = $this->config->get('intro_field', '');
        if($field != '')
        {
            $element = $this->_item->getElement($field);
            $intro = $element->get('value');
            if(empty($intro))
            {
                $intro = $element->data();
                $intro = $intro[0]["value"];
            }
        }

        $field = $this->config->get('text_field', '');
        if($field != '')
        {
            $element = $this->_item->getElement($field);
            $text = $element->get('value');
            if(empty($text))
            {
                $text = $element->data();
                $text = $text[0]["value"];
            }
        }

        if($this->config->get('img_source', 'field') == 'field')
        {
            $field = $this->config->get('img_field', '');
            if(!empty($field)){
                $image = $this->_item->getElement($field)->get('file');
                $image = (!empty($image)) ? JURI::root() . $image : '';
            }
        }
        else{
            $image = PlgJLLikeProHelper::extractImageFromText($intro, $text);
        }

        PlgJLLikeProHelper::addOpenGraphTags($this->_item->name, $intro . $text, $image);

        $shares = $helper->ShowIN($this->_item->id, $item_route, $this->_item->name, $image);
		return $shares;
	}

	public function edit()
    {
		return $this->app->html->_('select.booleanlist', $this->getControlName('value'), '', $this->get('value', $this->config->get('default', 1)));
	}


	public function renderSubmission($params = array())
    {
        return $this->edit();
	}


	public function validateSubmission($value, $params)
    {
		return array('value' => (bool) $value->get('value'));
	}
}