<?php print $product->_tmp_var_start?>


<div class="product productitem_<?php print $product->product_id?>">
<div>
	<form class="ajax-edit" name="product_<?php print $product->product_id?>" id="form_<?php print $product->product_id?>" method="post" action="index.php?option=com_jshopping&controller=cart&task=add" enctype="multipart/form-data" autocomplete="off">
	<input type="hidden" name="to" id='to' value="cart" />
        <!--input type="hidden" name="ajax" value="1" /-->
	<input type="hidden" name="product_id" id="product_id_<?php print $product->product_id?>" value="<?php print $product->product_id?>" />
	<input type="hidden" name="category_id" id="category_id_<?php print $product->product_id?>" value="<?php print $product->category_id?>" />	
	<input type="hidden" name="action" value="<?php print SEFLink('index.php?option=com_jshopping&controller=product&task=ajax_attrib_select_and_price&product_id='.$product->product_id.'&ajax=1',1)?>" />
    <div class="image">
        <?php if ($product->image){?>
        <div class="image_block">
            <?php if ($product->label_id){?>
                <div class="product_label">
                    <?php if ($product->_label_image){?>
                        <img src="<?php print $product->_label_image?>" alt="<?php print htmlspecialchars($product->_label_name)?>" />
                    <?php }/*else{?>
                        <span class="label_name"><?php print $product->_label_name;?></span>
                    <?php }*/?>
                </div>
            <?php }?>
            <a href="<?php print $product->product_link?>">
                <img class="jshop_img" src="<?php print $product->image?>" alt="<?php print htmlspecialchars($product->name);?>" />
            </a>
        </div>
        <?php }?>

        <?php if ($this->allow_review){?>
        <div class="review_mark"><div><div><?php print showMarkStar($product->average_rating);?></div></div></div>
        <div class="count_commentar">
            <?php print sprintf(_JSHOP_X_COMENTAR, $product->reviews_count);?>
        </div>
        <?php }?>
        <?php print $product->_tmp_var_bottom_foto;?>
    </div>
    <div>
        <div class="name">
            <a href="<?php print $product->product_link?>"><?php print $product->name?></a>
            <?php if ($this->config->product_list_show_product_code){?><span class="jshop_code_prod">(<?php print _JSHOP_EAN?>: <span><?php print $product->product_ean;?></span>)</span><?php }?>
        </div>
        <div class="description">
            <?php print $product->short_description?>
        </div>
        <?php if ($product->manufacturer->name){?>
            <div class="manufacturer_name" style="padding: 0 0 6px 5px;"><?php print _JSHOP_MANUFACTURER;?>: <span><?php print $product->manufacturer->name?></span></div>
        <?php }?>
        <?php if ($product->product_old_price > 0){?>
			<div class="price-all">
					<div class="old_price"><?php if ($this->config->product_list_show_price_description) print _JSHOP_OLD_PRICE.": ";?><span><?php print formatprice($product->product_old_price)?></span></div>
				<?php }?>
				<?php if ($product->product_price_default > 0 && $this->config->product_list_show_price_default){?>
					<div class="default_price"><?php print _JSHOP_DEFAULT_PRICE.": ";?><span><?php print formatprice($product->product_price_default)?></span></div>
			</div>
        <?php }?>
        <?php if ($product->_display_price){?>
            <div class = "jshop_price">
                <?php if ($this->config->product_list_show_price_description) print _JSHOP_PRICE.": ";?>
                <?php if ($product->show_price_from) print _JSHOP_FROM." ";?>
                <span class="view_prod_price"><?=$product->product_quantity <= 0 ? _JSHOP_PRODUCT_NOT_AVAILABLE : formatprice($product->product_price)?></span>
            </div>
        <?php }?>
	
            <div style="clear:both;"></div>
                <?php if (count($this->attributes[$product->product_id])){?>
		
		<div class="jshop_prod_attributes">
			<table class="jshop">
			<?php foreach($this->attributes[$product->product_id] as $attribut){?>
			<tr>
				<td class="attributes_title">
					<span class="attributes_name"><?php print $attribut->attr_name?>:</span><span class="attributes_description"><?php print $attribut->attr_description;?></span>
				</td>
				<td>
					<span id='block_attr_sel_<?php print $attribut->attr_id?>'>
					<?php print preg_replace('#onchange="([^"]+)"#iu', '', $attribut->selects)?>
					</span>
				</td>
			</tr>
			<tr>
				<td class="prod_qty">
					<?php print _JSHOP_QUANTITY?>:&nbsp;
				</td>
				<td class="prod_qty_input">
					<input type="text" name="quantity" class="inputbox" value="1<?php //print $this->default_count_product?>" /><?php print $this->_tmp_default_count_productqty_unit;?>
				</td>   
			</td>
			<?php }?>
			</table>
		</div>
		<?php }?>
        <?php print $product->_tmp_var_bottom_price;?>
        <?php if ($this->config->show_tax_in_product && $product->tax > 0){?>
            <span class="taxinfo"><?php print productTaxInfo($product->tax);?></span>
        <?php }?>
        <?php if ($this->config->show_plus_shipping_in_product){?>
            <span class="plusshippinginfo"><?php print sprintf(_JSHOP_PLUS_SHIPPING, $this->shippinginfo);?></span>
        <?php }?>
        <?php if ($product->basic_price_info['price_show']){?>
            <div class="base_price"><?php print _JSHOP_BASIC_PRICE?>: <?php if ($product->show_price_from) print _JSHOP_FROM;?> <span><?php print formatprice($product->basic_price_info['basic_price'])?> / <?php print $product->basic_price_info['name'];?></span></div>
        <?php }?>
        <?php if ($this->config->product_list_show_weight && $product->product_weight > 0){?>
            <div class="productweight"><?php print _JSHOP_WEIGHT?>: <span><?php print formatweight($product->product_weight)?></span></div>
        <?php }?>
        <?php if ($product->delivery_time != ''){?>
            <div class="deliverytime"><?php print _JSHOP_DELIVERY_TIME?>: <span><?php print $product->delivery_time?></span></div>
        <?php }?>
        <?php if (is_array($product->extra_field)){?>
            <div class="extra_fields">
            <?php foreach($product->extra_field as $extra_field){?>
                <div><?php print $extra_field['name'];?>: <?php print $extra_field['value']; ?></div>
            <?php }?>
            </div>
        <?php }?>
        <?php if ($product->vendor){?>
            <div class="vendorinfo"><?php print _JSHOP_VENDOR?>: <a href="<?php print $product->vendor->products?>"><?php print $product->vendor->shop_name?></a></div>
        <?php }?>
        <?php if ($this->config->product_list_show_qty_stock){?>
            <div class="qty_in_stock"><?php print _JSHOP_QTY_IN_STOCK?>: <span><?php print sprintQtyInStock($product->qty_in_stock)?></span></div>
        <?php }?>
        <?php print $product->_tmp_var_top_buttons;?>
        <div class="buttons">
            <?php if ($product->buy_link){?>
			<a class="button_buy" href="javascript:void(0);" onClick="document.getElementById('form_<?php print $product->product_id?>').submit();"><?php print _JSHOP_BUY?></a> &nbsp;
            <!--<a class="button_buy" href="<?php print $product->buy_link?>"><?php print _JSHOP_BUY?></a> &nbsp;-->
            <?php }?>
            <a class="button_detail" href="<?php print $product->product_link?>"><?php print _JSHOP_DETAIL?></a>
            <?php print $product->_tmp_var_buttons;?>
        </div>
        <?php print $product->_tmp_var_bottom_buttons;?>
    </div>
	</form>
</div>
</div>
<?php print $product->_tmp_var_end?>
