<?php print $this->checkout_navigator?>
<?php print $this->small_cart?>

<div class="jshop">
<form id = "shipping_form" name = "shipping_form" action = "<?php print $this->action ?>" method = "post" onsubmit = "return validateShippingMethods()">
<?php print $this->_tmp_ext_html_shipping_start?>
<table id = "table_shippings" cellspacing="0" cellpadding="0">
<?php foreach($this->shipping_methods as $shipping){?>
  <tr>
    <td style = "padding-top:5px; padding-bottom:5px">
      <input type = "radio" name = "sh_pr_method_id" id = "shipping_method_<?php print $shipping->sh_pr_method_id?>" value="<?php print $shipping->sh_pr_method_id ?>" <?php if ($shipping->sh_pr_method_id==$this->active_shipping){ ?>checked = "checked"<?php } ?> />
      <label for = "shipping_method_<?php print $shipping->sh_pr_method_id ?>"><?php
      if ($shipping->image){
        ?><span class="shipping_image"><img src="<?php print $shipping->image?>" alt="<?php print htmlspecialchars($shipping->name)?>" /></span><?php
      }
      ?><?php print $shipping->name?> (<?php print formatprice($shipping->calculeprice); ?>)</label>
      <?php if ($this->config->show_list_price_shipping_weight && count($shipping->shipping_price)){ ?>
          <br />
          <table class="shipping_weight_to_price">
          <?php foreach($shipping->shipping_price as $price){?>
              <tr>
                <td class="weight">
                    <?php if ($price->shipping_weight_to!=0){?>
                        <?php print formatweight($price->shipping_weight_from);?> - <?php print formatweight($price->shipping_weight_to);?>
                    <?php }else{ ?>
                        <?php print _JSHOP_FROM." ".formatweight($price->shipping_weight_from);?>
                    <?php } ?>
                </td>
                <td class="price">
                    <?php print formatprice($price->shipping_price); ?>
                </td>
            </tr>
          <?php } ?>
          </table>
      <?php } ?>
      <div class="shipping_descr"><?php print $shipping->description?></div>
      <?php if ($shipping->delivery){?>
      <div class="shipping_delivery"><?php print _JSHOP_DELIVERY_TIME.": ".$shipping->delivery?></div>
      <?php }?>
      </td>
  </tr>
<?php } ?>
</table>
<br/>
<div class="button-back-jshop">
	<a href="/checkout/step3">Назад</a>
</div>
<?php print $this->_tmp_ext_html_shipping_end?>
<input type = "submit" class = "button" value = "<?php print _JSHOP_NEXT ?>" />
</form>
</div>

<!--noindex-->
	<!--googleoff: all-->
	<div id="more-info-inner" style="margin-top: 60px;">
		<div class="more-info">
			<h4>8 ПРИЧИН ПОКУПАТЬ У НАС</h4>
			<ol>
				<li>100% гарантия возврата денег, если Вам не понравился товар.</li>
				<li>Высокое качество, сертификация, правильные условия хранения продукции.</li>
				<li>Выгодные цены.</li>
				<li>Специальные предложения и акции для постоянных клиентов.</li>
				<li>Упаковка в удобную тару для пищевых продуктов.</li>
				<li>Бесплатная доставка по Киеву – при заказе на сумму от 300 грн., по Украине – от 500 грн.</li>
				<li>Офис рядом с центром города – возможность самовывоза.</li>
				<li>Большой опыт торговли – с 2004 года, интернет-торговли – с 2009 года.</li>
			</ol>
		</div>
		<div class="more-info">
			<h4>ДОСТАВКА</h4>
				<ul>
					<li>Киев:</li>
						<ul>
							<li>курьером в Ваш дом или офис:</li>
								<ul>
									<li>от 300 грн. - бесплатно;</li>
									<li>до 300 грн. - 30,00 грн.</li>
								</ul>
							<li>самовывоз из офиса (ул. Павловская, 10).</li>
						</ul>
					<li>Украина:</li>
						<ul>
							<li>службой доставки "Новая Почта":</li>
								<ul>
									<li>от 500 грн. - бесплатно;</li>
									<li>до 500 грн. - согласно тарифов "Новой Почты".</li>
								</ul>
							<li>другими курьерскими службами:</li>
								<ul>
									<li>от 500 грн. - бесплатно;</li>
									<li> до 500 грн. - согласно тарифов курьерских служб.</li>
								</ul>
						</ul>		
				</ul>
		</div>
		<div class="more-info">
			<h4>ОПЛАТА</h4>
				<ul>
					<li>Киев:</li>
						<ul>
							<li>курьеру в руки при получении товара;</li>
							<li>банковский перевод на расчетный счет (предоплата).</li>
						</ul>
					<li>Украина:</li>
						<ul>
							<li>банковский перевод на расчетный счет (предоплата);</li>
							<li>оплата наложенным платежом (при получении товара в службе доставки).</li>
						</ul>
				</ul>
		</div>
	</div>
	<!--googleon: all-->
	<!--/noindex-->