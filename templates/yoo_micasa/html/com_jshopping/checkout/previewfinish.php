<?php print $this->checkout_navigator?>
<?php print $this->small_cart?>
<div class="jshop">
<?php print $this->_tmp_ext_html_previewfinish_start?>
<table class="jshop">

<?php if ($this->count_filed_delivery){?>
  <tr>
    <td>
       <strong><?php print _JSHOP_FINISH_DELIVERY_ADRESS?></strong>: 
       <?php if ($this->delivery_info['firma_name']) print $this->delivery_info['firma_name'].", ";?> 
       <?php print $this->delivery_info['f_name'] ?> 
       <?php print $this->delivery_info['l_name'] ?>, 
       <?php if ($this->delivery_info['street']) print $this->delivery_info['street'].","?>
       <?php if ($this->delivery_info['home'] && $this->delivery_info['apartment']) print $this->delivery_info['home']."/".$this->delivery_info['apartment'].","?>
       <?php if ($this->delivery_info['home'] && !$this->delivery_info['apartment']) print $this->delivery_info['home'].","?>
       <?php if ($this->delivery_info['state']) print $this->delivery_info['state']."," ?> 
       <?php print $this->delivery_info['zip']." ".$this->delivery_info['city']." ".$this->delivery_info['country']?>
    </td>
  </tr>
<?php }?>
<?php if (!$this->config->without_shipping){?>  
  <tr>
    <td>
       <strong><?php print _JSHOP_FINISH_SHIPPING_METHOD?></strong>: <?php print $this->sh_method->name?>
       <?php if ($this->delivery_time){?>
       <div class="delivery_time"><strong><?php print _JSHOP_DELIVERY_TIME?></strong>: <?php print $this->delivery_time?></div>
       <?php }?>
    </td>
  </tr>
<?php } ?>
<?php if (!$this->config->without_payment){?>  
  <tr>
    <td>
       <strong><?php print _JSHOP_FINISH_PAYMENT_METHOD ?></strong>: <?php print $this->payment_name ?>
    </td>
  </tr>
<?php } ?> 
</table>
<br />
<br />

<form name = "form_finish" action = "<?php print $this->action ?>" method = "post">
   <table class = "jshop" align="center" style="width:auto;margin-left:auto;margin-right:auto;">
     <tr>
       <td>
		   <?php print _JSHOP_ADD_INFO ?><br />
		   <textarea class = "inputbox" id = "order_add_info" name = "order_add_info"></textarea>
       </td>       
     </tr>
     </table>
	 <table class = "jshop" align="center" style="width:100%">
     <tr>
       <td style="text-align:center;padding-top:3px;">
		<div class="button-back-jshop">
			<a href="/checkout/step4">Назад</a>
		</div>
		   <input class="button" type="submit" name="finish_registration" value="<?php print _JSHOP_ORDER_FINISH?>" onclick="return checkAGB()" />
       </td>
     </tr>
   </table>
<?php print $this->_tmp_ext_html_previewfinish_end?>
</form>
</div>

<!--noindex-->
	<!--googleoff: all-->
	<div id="more-info-inner" style="margin-top: 60px;">
		<div class="more-info">
			<h4>8 ПРИЧИН ПОКУПАТЬ У НАС</h4>
			<ol>
				<li>100% гарантия возврата денег, если Вам не понравился товар.</li>
				<li>Высокое качество, сертификация, правильные условия хранения продукции.</li>
				<li>Выгодные цены.</li>
				<li>Специальные предложения и акции для постоянных клиентов.</li>
				<li>Упаковка в удобную тару для пищевых продуктов.</li>
				<li>Бесплатная доставка по Киеву – при заказе на сумму от 300 грн., по Украине – от 500 грн.</li>
				<li>Офис рядом с центром города – возможность самовывоза.</li>
				<li>Большой опыт торговли – с 2004 года, интернет-торговли – с 2009 года.</li>
			</ol>
		</div>
		<div class="more-info">
			<h4>ДОСТАВКА</h4>
				<ul>
					<li>Киев:</li>
						<ul>
							<li>курьером в Ваш дом или офис:</li>
								<ul>
									<li>от 300 грн. - бесплатно;</li>
									<li>до 300 грн. - 30,00 грн.</li>
								</ul>
							<li>самовывоз из офиса (ул. Павловская, 10).</li>
						</ul>
					<li>Украина:</li>
						<ul>
							<li>службой доставки "Новая Почта":</li>
								<ul>
									<li>от 500 грн. - бесплатно;</li>
									<li>до 500 грн. - согласно тарифов "Новой Почты".</li>
								</ul>
							<li>другими курьерскими службами:</li>
								<ul>
									<li>от 500 грн. - бесплатно;</li>
									<li> до 500 грн. - согласно тарифов курьерских служб.</li>
								</ul>
						</ul>		
				</ul>
		</div>
		<div class="more-info">
			<h4>ОПЛАТА</h4>
				<ul>
					<li>Киев:</li>
						<ul>
							<li>курьеру в руки при получении товара;</li>
							<li>банковский перевод на расчетный счет (предоплата).</li>
						</ul>
					<li>Украина:</li>
						<ul>
							<li>банковский перевод на расчетный счет (предоплата);</li>
							<li>оплата наложенным платежом (при получении товара в службе доставки).</li>
						</ul>
				</ul>
		</div>
	</div>
	<!--googleon: all-->
	<!--/noindex-->