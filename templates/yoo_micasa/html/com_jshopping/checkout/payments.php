<?php print $this->checkout_navigator?>
<?php print $this->small_cart?>

<script type="text/javascript">
var payment_type_check = {};
<?php foreach($this->payment_methods as  $payment){?>
    payment_type_check['<?php print $payment->payment_class;?>'] = '<?php print $payment->existentcheckform;?>';
<?php }?>
</script>

<div class="jshop">
<form id = "payment_form" name = "payment_form" action = "<?php print $this->action ?>" method = "post">
<?php print $this->_tmp_ext_html_payment_start?>
<table id = "table_payments" cellspacing="0" cellpadding="0">
  <?php 
  $payment_class = "";
  foreach($this->payment_methods as  $payment){
  if ($this->active_payment==$payment->payment_id) $payment_class = $payment->payment_class;
  ?>
  <tr>
    <td style = "padding-top:5px; padding-bottom:5px">
      <input type = "radio" name = "payment_method" id = "payment_method_<?php print $payment->payment_id ?>" onclick = "showPaymentForm('<?php print $payment->payment_class ?>')" value = "<?php print $payment->payment_class ?>" <?php if ($this->active_payment==$payment->payment_id){?>checked<?php } ?> />
      <label for = "payment_method_<?php print $payment->payment_id ?>"><?php
      if ($payment->image){
        ?><span class="payment_image"><img src="<?php print $payment->image?>" alt="<?php print htmlspecialchars($payment->name)?>" /></span><?php
      }
      ?><b><?php print $payment->name;?></b> 
        <?php if ($payment->price_add_text!=''){?>
            (<?php print $payment->price_add_text?>)
        <?php }?>
      </label>
    </td>
  </tr>
  <tr id = "tr_payment_<?php print $payment->payment_class ?>" <?php if ($this->active_payment != $payment->payment_id){?>style = "display:none"<?php } ?>>
    <td class = "jshop_payment_method">
        <?php print $payment->payment_description?>
        <?php print $payment->form?>
    </td>
  </tr>
  <?php } ?>
</table>
<br />
<div class="button-back-jshop">
	<a href="/checkout">Назад</a>
</div>
<?php print $this->_tmp_ext_html_payment_end?>
<input type = "button" id = "payment_submit" class = "button" name = "payment_submit" value = "<?php print _JSHOP_NEXT ?>" onclick="checkPaymentForm();" />
</form>
</div>

<?php if ($payment_class){ ?>
<script type="text/javascript">
    showPaymentForm('<?php print $payment_class;?>');
</script>
<?php } ?>

<!--noindex-->
	<!--googleoff: all-->
	<div id="more-info-inner" style="margin-top: 60px;">
		<div class="more-info">
			<h4>8 ПРИЧИН ПОКУПАТЬ У НАС</h4>
			<ol>
				<li>100% гарантия возврата денег, если Вам не понравился товар.</li>
				<li>Высокое качество, сертификация, правильные условия хранения продукции.</li>
				<li>Выгодные цены.</li>
				<li>Специальные предложения и акции для постоянных клиентов.</li>
				<li>Упаковка в удобную тару для пищевых продуктов.</li>
				<li>Бесплатная доставка по Киеву – при заказе на сумму от 300 грн., по Украине – от 500 грн.</li>
				<li>Офис рядом с центром города – возможность самовывоза.</li>
				<li>Большой опыт торговли – с 2004 года, интернет-торговли – с 2009 года.</li>
			</ol>
		</div>
		<div class="more-info">
			<h4>ДОСТАВКА</h4>
				<ul>
					<li>Киев:</li>
						<ul>
							<li>курьером в Ваш дом или офис:</li>
								<ul>
									<li>от 300 грн. - бесплатно;</li>
									<li>до 300 грн. - 30,00 грн.</li>
								</ul>
							<li>самовывоз из офиса (ул. Павловская, 10).</li>
						</ul>
					<li>Украина:</li>
						<ul>
							<li>службой доставки "Новая Почта":</li>
								<ul>
									<li>от 500 грн. - бесплатно;</li>
									<li>до 500 грн. - согласно тарифов "Новой Почты".</li>
								</ul>
							<li>другими курьерскими службами:</li>
								<ul>
									<li>от 500 грн. - бесплатно;</li>
									<li> до 500 грн. - согласно тарифов курьерских служб.</li>
								</ul>
						</ul>		
				</ul>
		</div>
		<div class="more-info">
			<h4>ОПЛАТА</h4>
				<ul>
					<li>Киев:</li>
						<ul>
							<li>курьеру в руки при получении товара;</li>
							<li>банковский перевод на расчетный счет (предоплата).</li>
						</ul>
					<li>Украина:</li>
						<ul>
							<li>банковский перевод на расчетный счет (предоплата);</li>
							<li>оплата наложенным платежом (при получении товара в службе доставки).</li>
						</ul>
				</ul>
		</div>
	</div>
	<!--googleon: all-->
	<!--/noindex-->