<div class="jshop">


<div class="like-pro-wrapper">
    <?php

        JFactory::getLanguage()->load('plg_content_jllikepro', JPATH_ADMINISTRATOR, null, false, true);
        require_once JPATH_ROOT .'/plugins/content/jllikepro/helper.php';
        $helper = PlgJLLikeProHelper::getInstance();
        $helper->loadScriptAndStyle(0); //1-если в категории, 0-если в контенте
        echo $helper->ShowIN($id, $link, $title, $image);
    ?>
</div>
<div class="jshop_list_category">
<?php if (count($this->categories)){ ?>
<table class = "jshop list_category">
    <?php foreach($this->categories as $k=>$category){?>
        <?php if ($k%$this->count_category_to_row==0) print "<tr>"; ?>
        <div class="jshop_categ">
            
			<div class="image">
                <a href = "<?php print $category->category_link;?>"><img class="jshop_img" src="<?php print $this->image_category_path;?>/<?php if ($category->category_image) print $category->category_image; else print $this->noimage;?>" alt="<?php print htmlspecialchars($category->name)?>" title="<?php print htmlspecialchars($category->name)?>" /></a>
            </div>
			
			<div class="sub_cat_link">
               <a class = "product_link" style="text-transform: uppercase;" href = "<?php print $category->category_link?>"><?php print $category->name?></a>
               <span class = "category_short_description"><?php print $category->short_description?></span>
            </div>
			
        </div>    
        <?php if ($k%$this->count_category_to_row==$this->count_category_to_row-1) print '</tr>'; ?>
    <?php } ?>
        <?php if ($k%$this->count_category_to_row!=$this->count_category_to_row-1) print '</tr>'; ?>
</table>
<?php }?>
<div style="clear: both;"></div>
<?php include(dirname(__FILE__)."/products.php");?>
<div class="sub_cat_desc">
<?php print $this->category->description?>
</div>